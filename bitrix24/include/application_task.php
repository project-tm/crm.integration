<?php

class application_task {

    const RESPONSIBLE_ID = 1;

    static public function call() {

        $code = $_REQUEST["code"];
        $domain = $_REQUEST["domain"];
        $member_id = $_REQUEST["member_id"];

        $params = array(
            "grant_type" => "authorization_code",
            "client_id" => application_api::CLIENT_ID,
            "client_secret" => application_api::CLIENT_SECRET,
            "redirect_uri" => application_api::REDIRECT_URI,
            "scope" => application_api::SCOPE,
            "code" => $code,
        );

        $path = "/oauth/token/";
        $query_data = application_api::query("GET", application_api::getApi($path), $params, true);
//        pre($path, $query_data);
        $access_token = $query_data['access_token'];

        foreach (application_data::get() as $data) {
            pre($data);
            $lidName = 'Лид с сайта: ' . $data['Телефон'];
            $search = application_api::call("crm.lead.list", array(
                        "auth" => $access_token,
                        'filter' => array(
                            'PHONE' => $data['Телефон'],
                        ),
                        'select' => array("ID", "TITLE")
            ));
            if (empty($search['total'])) {
                $search = application_api::call("crm.lead.list", array(
                            "auth" => $access_token,
                            'filter' => array(
                                'TITLE' => $lidName
                            ),
                            'select' => array("ID", "TITLE")
                ));
            }
            pre('$search', $search);
            if (empty($search['total'])) {
                $new = application_api::call("crm.lead.add", array(
                            "auth" => $access_token,
                            'fields' => array(
                                'TITLE' => $lidName,
                                'NAME' => $data['Имя'],
                                "STATUS_ID" => "NEW",
                                "OPENED" => "Y",
                                "SOURCE_ID" => "WEB",
                                "UF_CRM_1456401752" => $data['roistat'],
                                "ASSIGNED_BY_ID" => defined('RESPONSIBLE_ID') ? RESPONSIBLE_ID : self::RESPONSIBLE_ID,
                                'PHONE' => array(
                                    array(
                                        'VALUE' => $data['Телефон'],
                                        'VALUE_TYPE' => 'WORK'
                                    )
                                ),
                                'EMAIL' => array(
                                    array(
                                        'VALUE' => $data['Email'],
                                        'VALUE_TYPE' => 'WORK'
                                    )
                                ),
                            ),
//                            'params' => array(
//                                "REGISTER_SONET_EVENT" => "Y"
//                            )
                ));
                $lidId = array(
                    'L_' . $new['result']
                );
                pre($new);
            } else {
                $lidId = array();
                foreach ($search['result'] as $value) {
                    $lidId[] = 'L_' . $value['ID'];
                }
            }

            pre('$lidId', $lidId);
//            exit;
            $DESCRIPTION = array();
            foreach ($data as $key => $value) {
                $DESCRIPTION[] = $key . ': ' . $value;
            }
            $arPost = array(
                'UF_CRM_TASK' => $lidId,
                'TITLE' => 'Заявка с сайта',
                'DESCRIPTION' => implode(PHP_EOL, $DESCRIPTION),
                'RESPONSIBLE_ID' => defined('RESPONSIBLE_ID') ? RESPONSIBLE_ID : self::RESPONSIBLE_ID,
                    //            'DEADLINE' => '2013-05-13T16:06:06+03:00'
            );
            $data = application_api::call("task.item.add", array(
                        "auth" => $access_token,
                        'fields' => $arPost
            ));
            pre('task.item.add', $data);
        }

        exit;
    }

}
