<?

require_once(__DIR__ . "/include/init.php");

if (isset($_GET['code']) and isset($_GET['domain']) and isset($_GET['member_id'])) {
    if ($_GET['domain'] !== application_api::API_URI) {
        exit;
    }

    define('RESPONSIBLE_ID', 1);
    application_task::call();
}