<?php

namespace Bitrix24;

class Roistat {

    const RESPONSIBLE_ID = 2;
    const KEY = 'MzI3Njc6MzcyNTI6ZTBiNDY4NTVhMTY5NDU2NzI3YjkzMjZhMmJhNjJjOTQ=';

    static private function change(&$arFields) {
        foreach ($arFields as &$value) {
            if (is_array($value)) {
                self::change($value);
            } else {
                $value = iconv('cp1251', 'UTF-8', $value);
            }
        }
    }

    static public function send($arFields) {
        $roistatDescription = array();
        foreach ($arFields as $key => $value) {
            $roistatDescription[] = $key . ': ' . $value;
        }
        $roistatData = array();
        $roistatData['CLIENTID'] = isset($_COOKIE['_ga']) ? $_COOKIE['_ga'] : "����������";
        $roistatData['UTM_SOURCE'] = isset($_COOKIE['UTM_SOURCE']) ? urldecode($_COOKIE['UTM_SOURCE']) : "����������";
        foreach ($roistatData as $key => $value) {
            $roistatDescription[] = $key . ': ' . $value;
        }
        $roistatDescription = implode(PHP_EOL, $roistatDescription);

        $roistatData = array(
            'roistat' => isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null,
            'key' => self::KEY,
            'title' => '��� � �����: ' . $arFields['PHONE'],
            'comment' => $roistatDescription,
            'name' => $arFields['NAME'],
            'email' => $arFields['EMAIL'],
            'phone' => $arFields['PHONE'],
            'is_need_callback' => '1',
            'fields' => array(
                "STATUS_ID" => "NEW",
                "OPENED" => "Y",
                "SOURCE_ID" => "WEB",
                'COMMENTS' => $arFields['COMMENTS'],
//                "ASSIGNED_BY_ID" => self::RESPONSIBLE_ID,
//                "UF_CRM_1461830523" => $roistatDescription,
//                'UF_CRM_1461785662' => $roistatData['CLIENTID'],
//                'UF_CRM_1461877678' => $roistatData['UTM_SOURCE'],
            ),
        );
        self::change($roistatData);

        $res = file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));
//        preExit($roistatData, $res);
    }

}
