<?php

namespace Local\Roistat;

class Task {

    const RESPONSIBLE_ID = 1;

    static public function call() {

        $code = $_REQUEST["code"];
        $domain = $_REQUEST["domain"];
        $member_id = $_REQUEST["member_id"];

        $params = array(
            "grant_type" => "authorization_code",
            "client_id" => Api::CLIENT_ID,
            "client_secret" => Api::CLIENT_SECRET,
            "redirect_uri" => Api::REDIRECT_URI,
            "scope" => Api::SCOPE,
            "code" => $code,
        );

        $path = "/oauth/token/";
        $query_data = Api::query("GET", Api::getApi($path), $params, true);
//        pre($path, $query_data);
        $access_token = $query_data['access_token'];

        foreach (Data::get() as $data) {
            pre($data);
            $lidName = 'Лид с сайта: ' . $data['Телефон'];
            $search = Api::call("crm.lead.list", array(
                        "auth" => $access_token,
                        'filter' => array(
                            'PHONE' => $data['Телефон'],
                        ),
                        'select' => array("ID", "TITLE")
            ));
            if (empty($search['total'])) {
                $search = Api::call("crm.lead.list", array(
                            "auth" => $access_token,
                            'filter' => array(
                                'TITLE' => $lidName
                            ),
                            'select' => array("ID", "TITLE")
                ));
            }
            pre('$search', $search);
            if (empty($search['total'])) {
                $new = Api::call("crm.lead.add", array(
                            "auth" => $access_token,
                            'fields' => array(
                                'TITLE' => $lidName,
                                'NAME' => empty($data['Имя']) ? $data['Телефон'] : $data['Имя'],
                                "STATUS_ID" => "NEW",
                                "OPENED" => "Y",
                                "SOURCE_ID" => "WEB",
//                                "UF_CRM_1447764634" => $data['roistat'],
                                "ASSIGNED_BY_ID" => defined('RESPONSIBLE_ID') ? RESPONSIBLE_ID : self::RESPONSIBLE_ID,
                                'PHONE' => array(
                                    array(
                                        'VALUE' => $data['Телефон'],
                                        'VALUE_TYPE' => 'WORK'
                                    )
                                ),
                                'EMAIL' => array(
                                    array(
                                        'VALUE' => $data['Email'],
                                        'VALUE_TYPE' => 'WORK'
                                    )
                                ),
                            ),
//                            'params' => array(
//                                "REGISTER_SONET_EVENT" => "Y"
//                            )
                ));
                $lidId = array(
                    'L_' . $new['result']
                );
                pre($new);
            } else {
                $lidId = array();
                foreach ($search['result'] as $value) {
                    $lidId[] = 'L_' . $value['ID'];
                }
            }

            pre('$lidId', $lidId);
//            exit;
            $DESCRIPTION = array();
            foreach ($data as $key => $value) {
                $DESCRIPTION[] = $key . ': ' . $value;
            }
            $arPost = array(
                'UF_CRM_TASK' => $lidId,
                'TITLE' => 'Заявка с сайта - ' . $data['Форма'],
                'DESCRIPTION' => implode(PHP_EOL, $DESCRIPTION),
                'RESPONSIBLE_ID' => defined('RESPONSIBLE_ID') ? RESPONSIBLE_ID : self::RESPONSIBLE_ID,
                    //            'DEADLINE' => '2013-05-13T16:06:06+03:00'
            );
            $data = Api::call("task.item.add", array(
                        "auth" => $access_token,
                        'fields' => $arPost
            ));
            pre('task.item.add', $data);
        }

        exit;
    }

}
