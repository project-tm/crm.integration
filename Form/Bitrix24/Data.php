<?php

namespace Local\Roistat;

class Data {

    static private function getPath() {
        return Api::getPath('data.txt');
    }

    static public function add($data) {
//        pre($data);
        file_put_contents(self::getPath(), PHP_EOL . base64_encode(serialize($data)), FILE_APPEND);
    }

    static public function get() {
        $data = array();
        foreach (file(self::getPath()) as $value) {
            $value = trim($value);
            if(!empty($value)) {
//                pre(base64_decode($value));
                $data[] = unserialize(base64_decode($value));
            }
        }
        file_put_contents(self::getPath(), '');
        return $data;
    }

}
