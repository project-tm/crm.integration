<?php

namespace Local\Roistat;

class Api {

    const CLIENT_ID = 'local.56bf3bb14944c6.65523529';
    const CLIENT_SECRET = 'cb20aad4194188706f96b2554ca0a6b4';
    const SCOPE = 'task,crm';
    const API_URI = 'amlight.bitrix24.ru';
    const BITRIX24_URI = 'https://www.bitrix24.net';
    const REDIRECT_URI = 'http://am-light.ru/ajax/bitrix24.php';
    const LOGIN = 'www.am-light.ru@yandex.ru';
    const PASSWORD = 'serv9731';
    const pathFile = '/data/';
    const chmod = 0755;

    static public function getPath($file) {
        return __DIR__ . self::pathFile . $file;
    }

    static public function getApi($url) {
        return 'https://' . self::API_URI . $url;
    }

    static private function initCurl($url, $refererNew = null) {
        static $referer = '';
        if ($refererNew !== null) {
            $referer = $refererNew;
        }
        file_put_contents(self::getPath('cookie.txt'), str_replace(array('#HttpOnly_', '0	PHPSESSID'), array('', (time() + 60 * 60) . '	PHPSESSID'), file_get_contents(self::getPath('cookie.txt'))));

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0');
        $header = array();
        $header[0] = "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
        $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3";
        $header[] = "Pragma: "; // browsers keep this blank.
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_COOKIESESSION, true);
        curl_setopt($curl, CURLOPT_COOKIEFILE, self::getPath('cookie.txt'));
        curl_setopt($curl, CURLOPT_COOKIEJAR, self::getPath('cookie.txt'));
        if ($referer) {
            curl_setopt($curl, CURLOPT_REFERER, $referer);
        }
        $referer = $url;
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        return $curl;
    }

    static private function getAutorizeUrl($url) {
        pre($url);
        $curl = self::initCurl($url);
        $result = curl_exec($curl);
        curl_close($curl);
        foreach (array_map('trim', explode(PHP_EOL, $result)) as $value) {
            list($key) = $value = array_map('trim', explode(':', $value));
            if ($key == 'Location') {
                unset($value[0]);
                list($url, $result) = self::getAutorizeUrl(implode(':', $value));
            }
        }
        return array($url, $result);
    }

    static public function send($data) {
//        if (!isDebug()) {
//            error_reporting(0);
//        } else {
            return Amiro::send($data);
//        }

        if (!is_dir(__DIR__ . self::pathFile)) {
            mkdir(__DIR__ . self::pathFile, self::chmod);
        }
        Data::add($data);
        list($url, $result) = self::getAutorizeUrl($apiUrl = self::getApi('/oauth/authorize/?client_id=' . self::CLIENT_ID . '&response_type=code&redirect_uri=' . urldecode(self::REDIRECT_URI)));

        if (strpos($url, self::REDIRECT_URI) === 0) {
            pre('Вы авторизованы');
            preHtml($result);
        } else {

            preg_match('~<input type="hidden" name="backurl" value="([^"]+)"~', $result, $tmp);
            $curl = self::initCurl(self::BITRIX24_URI . '/auth/');
            curl_setopt($curl, CURLOPT_POST, true);
            $post = array(
                'AUTH_FORM' => 'Y',
                'backurl' => $tmp[1],
                'TYPE' => 'AUTH',
                'USER_REMEMBER' => 'Y',
                'USER_LOGIN' => self::LOGIN,
                'USER_PASSWORD' => self::PASSWORD
            );
            $post = http_build_query($post);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Length: ' . strlen($post)));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
            $result = curl_exec($curl);
            curl_close($curl);

            pre('login', $url);
            foreach (array_map('trim', explode(PHP_EOL, $result)) as $value) {
                list($key) = $value = array_map('trim', explode(':', $value));
                if ($key == 'Location') {
                    unset($value[0]);
                    list($url, $result) = self::getAutorizeUrl(implode(':', $value));
                }
            }
            pre('END', $url);

            list($url, $result) = self::getAutorizeUrl($apiUrl);
            pre('api', $url);
            preHtml($result);
        }
        return false;
    }

    static public function query($method, $url, $data = null, $jsonDecode = false) {
        $curlOptions = array(
            CURLOPT_HEADER => false
        );
        if ($method == "POST") {
            $curlOptions[CURLOPT_POST] = true;
            $curlOptions[CURLOPT_POSTFIELDS] = http_build_query($data);
        } elseif (!empty($data)) {
            $url .= strpos($url, "?") > 0 ? "&" : "?";
            $url .= http_build_query($data);
        }
        $curl = self::initCurl($url);
        curl_setopt_array($curl, $curlOptions);
        $result = curl_exec($curl);
        pre($url, $data);
        return ($jsonDecode ? json_decode($result, 1) : $result);
    }

    static public function call($method, $params) {
        return self::query("POST", self::getApi("/rest/" . $method), $params, true);
    }

}
