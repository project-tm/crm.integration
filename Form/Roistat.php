<?php

namespace Local\Form;

class Roistat {

    const KEY = 'NzYyMzoxOTMxNzozODJlMTYxMGRmMDkzYzBmOGNjOTY3Y2EyMjI3MDJjOA==';

    static public function send($data) {
        $DESCRIPTION = array();
        foreach ($data as $key => $value) {
            $DESCRIPTION[] = $key . ': ' . $value;
        }

        $roistatData = array(
            'roistat' => isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null,
            'key' => self::KEY, // Замените SECRET_KEY на секретный ключ из пункта меню Настройки -> Интеграция со сделками в нижней части экрана и строчке Ключ для интеграций
            'title' => 'Заявка с сайта - ' . $data['Форма'],
            'comment' => implode(PHP_EOL, $DESCRIPTION),
            'name' => empty($data['Имя']) ? $data['Телефон'] : $data['Имя'],
            'email' => $data['Email'],
            'phone' => $data['Телефон'],
            'is_need_callback' => '0', // Для автоматического использования обратного звонка при отправке контакта и сделки нужно поменять 0 на 1
        );
//        pre($roistatData);

        file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));
    }

}
