<?php

namespace Local\Form;

class Lead {

    const SITE_NAME = 'dezoff.ru';

    static public function add($data) {
        set_time_limit(0);
        ignore_user_abort(true);
        if(!empty($_SERVER['HTTP_REFERER'])) {
            $data['Адрес'] = $_SERVER['HTTP_REFERER'];
        }
        Roistat::send($data);
    }

    static public function OnSaleComponentOrderOneStepComplete($ORDER_ID, $arFields, $arParams) {
        $data = array(
            'Форма' => 'Заказ',
            'Заказ' => '№' . $ORDER_ID,
        );
        $db_props = \CSaleOrderProps::GetList(
                        array("SORT" => "ASC"), array(
                    "PERSON_TYPE_ID" => $arFields["PERSON_TYPE_ID"]
                        )
        );
        while ($arProps = $db_props->Fetch()) {
            $db_vals = \CSaleOrderPropsValue::GetList(
                            array("SORT" => "ASC"), array(
                        "ORDER_ID" => $ORDER_ID,
                        "ORDER_PROPS_ID" => $arProps["ID"]
                            )
            );
            while ($arVals = $db_vals->Fetch()) {
                if ($arVals['CODE'] === 'LOCATION') {
                    $location = \CSaleLocation::GetByID($arVals['VALUE']);
                    $loc = array();
                    foreach (array('COUNTRY_NAME', 'REGION_NAME_LANG', 'CITY_NAME_LANG') as $value) {
                        if ($location[$value]) {
                            $loc[] = $location[$value];
                        }
                    }
                    if ($loc) {
                        $data[$arProps['NAME']] = implode(', ', $loc);
                    }
                } else {
                    $data[$arProps['NAME']] = $arVals['VALUE'];
                }
            }
        }
        $arOrder = \CSaleOrder::getById($ORDER_ID);
        if ($arOrder['USER_DESCRIPTION']) {
            $data['Комментарий'] = $arOrder['USER_DESCRIPTION'];
        }
        $delivery = \CSaleDelivery::GetByID($arFields['DELIVERY_ID']);
        $data['Доставка'] = $delivery['NAME'];
        $pay = \CSalePaySystem::GetByID($arFields['PAY_SYSTEM_ID']);
        $data['Оплата'] = $pay['NAME'];
        $data['Состав'] = \Local\Order::getOrderList($ORDER_ID);
        $data['Сумма'] = \SaleFormatCurrency($arFields['PRICE'], $arFields['CURRENCY']);
        self::add($data);
    }

    static public function onAfterResultAdd($WEB_FORM_ID, $RESULT_ID) {
        $result = \CFormResult::GetDataByIDForHTML($RESULT_ID, "Y");
        // получим данные по всем вопросам
        $arAnswer = \CFormResult::GetDataByID(
                        $RESULT_ID, array(), $arResult, $arAnswer2);
        $arResult = array(
            'web' => self::SITE_NAME,
        );
        foreach ($arAnswer as $answer) {
            foreach ($answer as $arQuestion) {
                $arResult[$arQuestion['TITLE']] = $result['form_' . $arQuestion['FIELD_TYPE'] . '_' . $arQuestion['ANSWER_ID']];
            }
        }

        if ($WEB_FORM_ID == 1) {
            $data = array(
                'Форма' => 'Заказать обратный звонок',
                'Телефон' => $arResult["НОМЕР ТЕЛЕФОНА"],
            );
            self::add($data);
        }
    }

}
