<?php

ini_set('display_errors',1);
ini_set("error_reporting", E_ALL);
//set_time_limit(0); 
//define("SITE_ID", "s1");
//define("BX_UTF", true);
//define("NO_KEEP_STATISTIC", true);
//define("NOT_CHECK_PERMISSIONS", true);
//define("BX_BUFFER_USED", true);
//define('NO_AGENT_CHECK', true);

$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../");
chdir($_SERVER["DOCUMENT_ROOT"]);

// windows workaround
if ( strpos($_SERVER["DOCUMENT_ROOT"], '\\') !== false ) {
    $_SERVER["DOCUMENT_ROOT"] = str_replace('\\', '/', substr( $_SERVER["DOCUMENT_ROOT"], 2 ));
    }
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
    
//var_dump($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//require("/var/www/u0178373/data/www/delightstudio.ru/bitrix/modules/main/include/prolog_before.php");

if (!class_exists('AppRegistryFile')) {include __DIR__ . '/AppRegistryFile.php';}

class BX24_API {
	const APP_ID = "local.5698bea02c6f91.94054525";
	const APP_URL = "http://delightstudio.ru/integration/bx24.php";
	const SECRET_KEY = "b9684e4aa4f9f40b47202d85cf368f79";
	const URL = "http://delightstudio.bitrix24.ru/";
	const URLS = "https://delightstudio.bitrix24.ru/";
	const SCOPE = "crm";

	private static $instance = null;
	/**
	*@return BX24_API
	*/
	public static function init() {
		if (!self::$instance) {
			self::$instance = new self();
			self::$instance->auth();
		}
		echo 'init';
		return self::$instance;
	}

	public static function codeAuth() {
		$obj = new self();
		$data = $obj->getSslPage(self::URLS . 'oauth/token/?grant_type=authorization_code&client_id='.self::APP_ID.'&client_secret='.self::SECRET_KEY.'&code='.$_REQUEST['code'].'&scope='.self::SCOPE.'&redirect_uri='.self::APP_URL);
		$data = json_decode($data, true);        
		$auth = $obj->checkAuth($data);
		if (!$auth) $this->error('auth_code');
		return $auth;
	}

	private $data = array();
	private function getPage($url, $params = false) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_REFERER, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		if ($params !== false) {
			$post = array();
			$this->http_build_query_for_curl($params, $post);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		}
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}

	private function http_build_query_for_curl( $arrays, &$new = array(), $prefix = null ) {
		if ( is_object( $arrays ) ) {
			$arrays = get_object_vars( $arrays );
		}
		foreach ( $arrays AS $key => $value ) {
			$k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
			if ( is_array( $value ) OR is_object( $value )  ) {
				$this->http_build_query_for_curl( $value, $new, $k );
			} else {
				$new[$k] = $value;
			}
		}
	}
	private function getSslPage($url, $params = false) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_REFERER, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		if ($params !== false) {
			$post = array();
			$this->http_build_query_for_curl($params, $post);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		}
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	private function getData() {
		$this->data = AppRegistryFile::getFile('data', array());
		$this->data['ex_time'] = intval($this->data['ex_time']);
	}
	private function setData($data = false) {
		if ($data !== false) $this->data = $data;
		AppRegistryFile::setFile('data', $this->data);
	}
	private function __construct() {
		$this->getData();
	}
	private function checkAuth($data) {
		$data['ex_time'] = time() + $data['expires_in'];
		
		$this->setData($data);        
		return $data['expires_in'] && $data['access_token'];
	}
	private function auth() {
		//$nowTime = time();
		$date = new DateTime(null);
		$nowTime=($date->getTimestamp() + $date->getOffset());
		
		
		$auth = isset($this->data['ex_time']);
		
		if ($auth && $this->data['ex_time'] < $nowTime){
		$data = $this->getSslPage(self::URLS . 'oauth/token/?grant_type=refresh_token&client_id='.self::APP_ID.'&client_secret='.self::SECRET_KEY.'&refresh_token='.$this->data['refresh_token'].'&scope='.self::SCOPE.'&redirect_uri='.self::APP_URL);
			$data = json_decode($data, true);            
			$auth = $this->checkAuth($data);
		}
		if (!$auth) {
			$this->error('auth_auto');
		}
	}
	public function error($code) {
		$errors = array(
			'auth_code' => 'Ошика авторизации по коду',
			'auth_auto' => 'Истек срок авторизации. Требуется авторизовать приложение по <a href="'.self::URL.'/oauth/authorize/?client_id='.self::APP_ID.'&response_type=code&redirect_uri='.self::APP_URL.'">ссылке</a>',
		);
		echo $errors[$code];
	}
	public function sendAPI($method, $params = array()) {
		$params['auth'] = $this->data['access_token'];
		$url = self::URLS.'rest/'.$method;
		echo ($url);
		$data = $this->getSslPage($url, $params);
		$data = json_decode($data, true);
		return $data;
	}

	public function sendAPIList($method, $params = array(), $start = 0) {
		$result = array();
		do {
			if ($start) $params['start'] = $start;
			$res = $this->sendAPI($method, $params);
			$result[] = $res;
			$start = $res['next'];
		} while ($res['next']);
		return $result;
	}

	public function getList() {
		$list = $this->sendAPIList('crm.product.list', array(
			'order' => array('NAME' => "ASC"),
			'select' => array('ID', 'NAME', 'PRICE')
		));
		$result = array();
		foreach ($list as $chunk) if ($chunk['result']) {
			$result = array_merge($result, $chunk['result']);
		}
		$indexed = array(
			'ART' => array(),
			'NAME' => array(),
			'ID' => array()
		);
		foreach ($result as $item) {
			$tmpName = explode(' / ', $item['NAME']);
			$item['ART'] = trim($tmpName[1]);
			$item['NAME'] = trim($tmpName[0]);
			$indexed['ID'][$item['ID']] = $item;
			$indexed['ART'][$item['ART']] = $item['ID'];
			$indexed['NAME'][ToUpper($item['NAME'])] = $item['ID'];
		}
		return $indexed;
	}

	public function getContactList($date) {
		$list = $this->sendAPIList('crm.contact.list', array(
			'order' => array('ID' => "ASC"),
			'select' => array('ID', 'PHONE', 'EMAIL', 'LAST_NAME', 'NAME', 'SECOND_NAME', 'BIRTHDATE'),
			'filter' => array('>=DATE_MODIFY' => $date)
		));
		$result = array();
		foreach ($list as $chunk) if ($chunk['result']) {
			$result = array_merge($result, $chunk['result']);
		}
		return $result;
	}
	
	public function getContactById($id) {
		$result = $this->sendAPI('crm.contact.get',array('id'=>$id));
		if(!empty($result['result'])) $result = $result['result'];
		return $result;
	}
    
    public function contactUpdate($id,$fields) {
        $result = $this->sendAPI('crm.contact.update',array('id'=>$id,'fields'=>$fields));
        //if(!empty($result['result'])) $result = $result['result'];
        return $result;
    }

	public function change($ID, $data) {
		if ($data['ART']) {
			$data['NAME'] .= ' / ' . $data['ART'];
			unset($data['ART']);
		}
		//update
		$res = $this->sendAPI('crm.product.update', array(
			'id' => $ID,
			'fields' => $data
		));
		//var_dump('UPDATE', $ID, $data, $res);
		$ID = $res['result'];
		return $ID;
	}

	public function add($data) {
		if ($data['ART']) {
			$data['NAME'] .= ' / ' . $data['ART'];
			unset($data['ART']);
		}
		//add
		$data['CURRENCY_ID'] = 'RUB';
		$data['SORT'] = 500;
		$res = $this->sendAPI('crm.product.add', array('fields' => $data));
		$ID = $res['result'];
		//var_dump('ADD', $data, $res);
		return $ID;
	}

	public function sync($localList) {
		$remoteList = $this->getList();
//		echo count($remoteList);
		$ID = 0;
		$fields = array();
		foreach ($localList as $item) {
			if ($item['ART'] && isset($remoteList['ART'][$item['ART']])) {
				$ID = $remoteList['ART'][$item['ART']];
			} elseif (isset($remoteList['NAME'][ToUpper($item['NAME'])])) {
				$ID = $remoteList['NAME'][ToUpper($item['NAME'])];
			}
			//var_dump($ID);
			if ($ID) {
				$fields = $remoteList['ID'][$ID];
				$change = $item;
				if ($change['ART'] == $fields['ART'] && $change['NAME'] == $fields['NAME']) {
					unset($change['ART']);
					unset($change['NAME']);
				}
				if ($change['PRICE'] == $fields['PRICE']) unset($change['PRICE']);
				if (count($change)) $this->change($ID, $change);
			} else {
				$this->add($item);
			}
			$ID = 0;
		}
	}

}
if (isset($_REQUEST['code'])) {
	BX24_API::codeAuth();
	die();
    }

$api = BX24_API::init();


if (isset($iList) && is_array($iList)){
$api->sync($iList)
;}
