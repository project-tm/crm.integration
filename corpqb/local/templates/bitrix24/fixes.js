$(document).ready(function(){
	initDatatable();
	$('.dateFilter form').on('submit',function(){
		var data = $(this).serialize();
		var action = $(this).attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: data,
			success: function(ajaxData){
				$('#ajaxSkudWrapper').html(ajaxData);
				initDatatable();
			}
		});
		return false;
	});
});
function initDatatable(){
	if($('#skudTable').length > 0){
		var action = $('.dateFilter form').attr('action');
		
		$('#skudTable').DataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
			},
			"iDisplayLength": 100,
			"sDom": '<"top"><"bottom"f><"clear">'
		});
		
		$('.monthSelect a').on('click',function(){
			var unixtime = $(this).attr('href');
			$.ajax({
				type: "POST",
				url: action,
				data: {'TIME': unixtime, 'AJAXCALL': 'Y'},
				success: function(ajaxData){
					history.pushState('', '', '/skud/?TIME='+unixtime);
					$('#ajaxSkudWrapper').html(ajaxData);
					initDatatable();
				}
			});
			return false;
		});
		
		$('input.realUserTime').on('change',function(){
			var t = $(this),
				parentsRow = $(t).closest('tr'),
				thisID = t.data('id'),
				periodID = t.data('period-id'),
				userID = t.data('user-id'),
				thisVal = parseInt(t.val()),
				requiredTime = $(parentsRow).find('td.required').html(),
				percent = 0;
			
			$.ajax({
				type: "POST",
				url: '/local/ajax/updateUserTime.php',
				data: {'ID': thisID, 'VALUE':thisVal, 'EDITDATA':'Y', 'USER':userID, 'PERIOD':periodID},
				success: function(ajaxData){
					var res = jQuery.parseJSON(ajaxData);
					if(res.status == 'ok'){
						$(t).addClass('ok');
						setTimeout(function(){
							$(t).removeClass('ok');
						},1000);
						
						if(parseInt(res.value)>0)
						{
							$(t).data('id',parseInt(res.value));
						}
						percent = Number((thisVal/requiredTime*100).toFixed(2));
						$(parentsRow).find('td.percent').html(percent);
					}else{
						$(t).addClass('error');
					}
					
				}
			});
			return false;
			
		});
	}
}