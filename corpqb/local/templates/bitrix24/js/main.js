$(document).ready(function(){
	$('.data-table').dateRangePicker(
	{
		showShortcuts: false,
		format: 'YYYY-MM-DD'
	}).bind('datepicker-change', function(evt, obj) {
		alert('date1: ' + obj.date1 + ' / date2: ' + obj.date2);
	});
	$('.clear-form').click(function(evt)
	{
		evt.stopPropagation();
		$('.data-table').data('dateRangePicker').clear();
		$('.data-table').data('dateRangePicker').close();
	});
	$('.date-open').click(function(evt)
	{
		evt.stopPropagation();
		$('.data-table').data('dateRangePicker').open();
	});
	$('#skudTable').DataTable(); 
});