<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//$this->setFrameMode(true);
$APPLICATION->AddHeadScript("/bitrix/js/calendar/cal-core.js", false);
$APPLICATION->AddHeadScript("/bitrix/js/calendar/cal-planner.js", false);
$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/css/main-style.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>'); 
$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.11/js/jquery.dataTables.min.js"></script>'); 

$arParams["can_delete_some"]=false;

//echo "<pre>arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>arResult: "; print_r($arResult); echo "</pre>";

//echo "<pre>"; print_r($arResult["arrFORM_FILTER"]); echo "</pre>";
?>
<?
/***********************************************
				  filter
************************************************/
/*?>
<form name="form1" method="GET" action="<?=$APPLICATION->GetCurPageParam("", array("sessid", "delete", "del_id", "action"), false)?>?" id="form_filter_<?=$arResult["filter_id"]?>" class="form-filter-<?=htmlspecialcharsbx($arResult["tf"])?>">
<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>" />
<?if ($arParams["SEF_MODE"] == "N"):?><input type="hidden" name="action" value="list" /><?endif?>
<table class="form-filter-table data-table">
	<thead>
		<tr>
			<th colspan="2">&nbsp;</td>
		</tr>
	</thead>
	<tbody>
		<?
		if (strlen($arResult["str_error"]) > 0)
		{
		?>
		<tr>
			<td class="errortext" colspan="2"><?=$arResult["str_error"]?></td>
		</tr>
		<?
		} // endif (strlen($str_error) > 0)
		?>
		<tr>
			<td><?=GetMessage("FORM_F_ID")?></td>
			<td><?=CForm::GetTextFilter("id", 45, "", "")?></td>
		</tr>
		<?
		if ($arParams["SHOW_STATUS"]=="Y")
		{
		?>
		<tr>
			<td><?=GetMessage("FORM_F_STATUS")?></td>
			<td><select name="find_status" id="find_status">
				<option value="NOT_REF"><?=GetMessage("FORM_ALL")?></option>
				<?
				foreach ($arResult["arStatuses_VIEW"] as $arStatus)
				{
				?>
				<option value="<?=$arStatus["REFERENCE_ID"]?>"<?=($arStatus["REFERENCE_ID"]==$arResult["__find"]["find_status"] ? " SELECTED=\"1\"" : "")?>><?=$arStatus["REFERENCE"]?></option>
				<?
				}
				?>
			</select></td>
		</tr>
		<tr>
			<td><?=GetMessage("FORM_F_STATUS_ID")?></td>
			<td><?echo CForm::GetTextFilter("status_id", 45, "", "");?></td>
		</tr>
		<?
		} //endif ($SHOW_STATUS=="Y");
		?>
		<tr>
			<td><?=GetMessage("FORM_F_DATE_CREATE")." (".CSite::GetDateFormat("SHORT")."):"?></td>
			<td><?=CForm::GetDateFilter("date_create", "form1", "Y", "", "")?></td>
		</tr>
		<tr>
			<td><?=GetMessage("FORM_F_TIMESTAMP")." (".CSite::GetDateFormat("SHORT")."):"?></td>
			<td><?=CForm::GetDateFilter("timestamp", "form1", "Y", "", "")?></td>
		</tr>
		<?
		if (is_array($arResult["arrFORM_FILTER"]) && count($arResult["arrFORM_FILTER"])>0)
		{
			foreach ($arResult["arrFORM_FILTER"] as $arrFILTER)
			{
				$prev_fname = "";

				foreach ($arrFILTER as $arrF)
				{
					if ($arParams["SHOW_ADDITIONAL"] == "Y" || $arrF["ADDITIONAL"] != "Y")
					{
						$i++;
						if ($arrF["SID"]!=$prev_fname)
						{
							if ($i>1)
							{
							?>
			</td>
		</tr>
							<?
							} //endif($i>1);
							?>
		<tr>
			<td>
				<?=$arrF["FILTER_TITLE"] ? $arrF['FILTER_TITLE'] : $arrF['TITLE']?>
				<?=($arrF["FILTER_TYPE"]=="date" ? " (".CSite::GetDateFormat("SHORT").")" : "")?>
			</td>
			<td>
			<?
						} //endif ($fname!=$prev_fname) ;
						?>
						<?
						switch($arrF["FILTER_TYPE"]){
							case "text":
								echo CForm::GetTextFilter($arrF["FID"]);
								break;
							case "date":
								echo CForm::GetDateFilter($arrF["FID"]);
								break;
							case "integer":
								echo CForm::GetNumberFilter($arrF["FID"]);
								break;
							case "dropdown":
								echo CForm::GetDropDownFilter($arrF["ID"], $arrF["PARAMETER_NAME"], $arrF["FID"]);
								break;
							case "exist":
							?>
								<?=CForm::GetExistFlagFilter($arrF["FID"])?>
								<?=GetMessage("FORM_F_EXISTS")?>
							<?
								break;
						} // endswitch
						?>
						<?
						if ($arrF["PARAMETER_NAME"]=="ANSWER_TEXT")
						{
						?>
				&nbsp;[<span class='form-anstext'>...</span>]
						<?
						}
						elseif ($arrF["PARAMETER_NAME"]=="ANSWER_VALUE")
						{
						?>
				&nbsp;(<span class='form-ansvalue'>...</span>)
						<?
						}
						?>
				<br />
						<?
						$prev_fname = $arrF["SID"];
					} //endif (($arrF["ADDITIONAL"]=="Y" && $SHOW_ADDITIONAL=="Y") || $arrF["ADDITIONAL"]!="Y");

				} // endwhile (list($key, $arrF) = each($arrFILTER));

			} // endwhile (list($key, $arrFILTER) = each($arrFORM_FILTER));
		} // endif(is_array($arrFORM_FILTER) && count($arrFORM_FILTER)>0);
		?></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th colspan="2">
				<input type="submit" name="set_filter" value="<?=GetMessage("FORM_F_SET_FILTER")?>" /><input type="hidden" name="set_filter" value="Y" />&nbsp;&nbsp;<input type="submit" name="del_filter" value="<?=GetMessage("FORM_F_DEL_FILTER")?>" />
			</td>
		</tr>
	</tfoot>
</table>
</form>
<br />
*/?>
        <div class="filter-wrap">
            <div class="input-block fl">
                <input type="text" placeholder="Поиск"/>
            </div>
            <?/*<div class="monthSelect fr">
                <a href="1451595600">←</a><span class="date-open">Февраль, 2016</span><a href="1456779600">→</a>
                <input class="data-table" size="60" value="">
            </div>*/?>
            <div class="cl"></div>
        </div>

<?
if ($arParams["can_delete_some"])
{
?>
<SCRIPT LANGUAGE="JavaScript">
<!--
function OnDelete_<?=$arResult["filter_id"]?>()
{
	var show_conf;
	var arCheckbox = document.forms['rform_<?=$arResult["filter_id"]?>'].elements["ARR_RESULT[]"];
	if(!arCheckbox) return;
	if(arCheckbox.length>0 || arCheckbox.value>0)
	{
		show_conf = false;
		if (arCheckbox.value>0 && arCheckbox.checked) show_conf = true;
		else
		{
			for(i=0; i<arCheckbox.length; i++)
			{
				if (arCheckbox[i].checked)
				{
					show_conf = true;
					break;
				}
			}
		}
		if (show_conf)
			return confirm("<?=GetMessage("FORM_DELETE_CONFIRMATION")?>");
		else
			alert('<?=GetMessage("FORM_SELECT_RESULTS")?>');
	}
	return false;
}

function OnSelectAll_<?=$arResult["filter_id"]?>(fl)
{
	var arCheckbox = document.forms['rform_<?=$arResult["filter_id"]?>'].elements["ARR_RESULT[]"];
	if(!arCheckbox) return;
	if(arCheckbox.length>0)
		for(i=0; i<arCheckbox.length; i++)
			arCheckbox[i].checked = fl;
	else
		arCheckbox.checked = fl;
}
//-->
</SCRIPT>
<?
} //endif($can_delete_some);
?>

<?
if (strlen($arResult["FORM_ERROR"]) > 0) ShowError($arResult["FORM_ERROR"]);
if (strlen($arResult["FORM_NOTE"]) > 0) ShowNote($arResult["FORM_NOTE"]);
?>
<form name="rform_<?=$arResult["filter_id"]?>" method="post" action="<?=POST_FORM_ACTION_URI?>#nav_start">
	<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>" />
	<?=bitrix_sessid_post()?>

	<?
	if ($arParams["can_delete_some"])
	{
	?>
	<p><input type="submit" name="delete" value="<?=GetMessage("FORM_DELETE_SELECTED")?>" onClick="return OnDelete_<?=$arResult["filter_id"]?>()"  /></p>
	<?
	} // endif($can_delete_some);
	?>

	<?
	if ($arResult["res_counter"] > 0 && $arParams["SHOW_STATUS"] == "Y" && $arParams["F_RIGHT"] >= 20 && $arParams["can_delete_some"])
	{
	?>
	<p><input type="submit" name="save" value="<?=GetMessage("FORM_SAVE")?>" /><input type="hidden" name="save" value="Y" />&nbsp;<input type="reset" value="<?=GetMessage("FORM_RESET")?>" /></p>
	<?
	} //endif(intval($res_counter)>0 && $SHOW_STATUS=="Y" && $F_RIGHT>=20);
	?>
	<p>
	<?=$arResult["pager"]?>
	</p>
    <table cellpadding='5' cellspacing='0' id='skudTable'>
		<?
		/***********************************************
					  table header
		************************************************/
		?>
		<thead>
			<tr>
				<?/*<td><?=GetMessage("FORM_TIMESTAMP")?><br /><?=SortingEx("s_timestamp")?></td>*/?>
<?
				if (is_array($arResult["arrColumns"]))
				{
					foreach ($arResult["arrColumns"] as $arrCol)
					{
                        ?><td><?=$arrCol["RESULTS_TABLE_TITLE"]?></td><?
					}
				}
?>              <td>Статус</td>
			</tr>
		</thead>
		<?
		/***********************************************
					  table body
		************************************************/
		?>
		<?
		if(count($arResult["arrResults"]) > 0)
		{
?>
			<tbody>
<?
			$j=0;
			foreach ($arResult["arrResults"] as $arRes)
			{
				$j++;
?>
				<tr>
				<?
				$cnt = 4;
				foreach ($arResult["arrColumns"] as $FIELD_ID => $arrC)
				{
				?>
				<td>
					<?
					$arrAnswer = $arResult["arrAnswers"][$arRes["ID"]][$FIELD_ID];
					if (is_array($arrAnswer))
					{
						foreach ($arrAnswer as $key => $arrA)
						{
						    $answerText="";
						?>
							<?if (strlen(trim($arrA["USER_TEXT"])) > 0) {$answerText=$arrA["USER_TEXT"];}?>
							<?if (strlen(trim($arrA["ANSWER_TEXT"])) > 0) {$answerText=$arrA["ANSWER_TEXT"];}?>
							<?if ($answerText) {
							    if ($arrA["SID"]=="PRICE") {$answerText=number_format($answerText,0,"."," ");}
							    echo $answerText;
							    
							}?>
							<?if (strlen(trim($arrA["ANSWER_VALUE"])) > 0 && $arParams["SHOW_ANSWER_VALUE"]=="Y") {?>(<span class='form-ansvalue'><?=$arrA["ANSWER_VALUE"]?></span>)<?}?>
									<?
									if (intval($arrA["USER_FILE_ID"])>0)
									{
										if ($arrA["USER_FILE_IS_IMAGE"]=="Y")
										{
										?>
											<?=$arrA["USER_FILE_IMAGE_CODE"]?>
										<?
										}
										else
										{
										?>
										<a title="<?=GetMessage("FORM_VIEW_FILE")?>" target="_blank" href="/bitrix/tools/form_show_file.php?rid=<?=$arRes["ID"]?>&hash=<?=$arrA["USER_FILE_HASH"]?>&lang=<?=LANGUAGE_ID?>"><?=$arrA["USER_FILE_NAME"]?></a><br />
										(<?=$arrA["USER_FILE_SIZE_TEXT"]?>)<br />
										[&nbsp;<a title="<?=str_replace("#FILE_NAME#", $arrA["USER_FILE_NAME"], GetMessage("FORM_DOWNLOAD_FILE"))?>" href="/bitrix/tools/form_show_file.php?rid=<?=$arRes["ID"]?>&hash=<?=$arrA["USER_FILE_HASH"]?>&lang=<?=LANGUAGE_ID?>&action=download"><?=GetMessage("FORM_DOWNLOAD")?></a>&nbsp;]
										<?
										}
									}
									?>
						<?
						} //foreach
					} // endif (is_array($arrAnswer));
					?>
				</td>
				<?
				} //foreach
				?>
				<td><?=$arRes["STATUS_TITLE"]?></td>
			</tr>
			<?
			} //foreach
			?>
			</tbody>
		<?
		}?>
	</table>

	<p><?=$arResult["pager"]?></p>
	<?
	if (intval($arResult["res_counter"])>0 && $arParams["SHOW_STATUS"]=="Y" && $arParams["F_RIGHT"] >= 20 && $arParams["can_delete_some"])
	{
	?>
	<p>
	<input type="submit" name="save" value="<?=GetMessage("FORM_SAVE")?>" /><input type="hidden" name="save" value="Y" />&nbsp;<input type="reset" value="<?=GetMessage("FORM_RESET")?>" />
	</p>
	<?
	} //endif (intval($res_counter)>0 && $SHOW_STATUS=="Y" && $F_RIGHT>=20);
	?>

	<?
	if ($arParams["can_delete_some"])
	{
	?>
	<p><input type="submit" name="delete" value="<?=GetMessage("FORM_DELETE_SELECTED")?>" onClick="return OnDelete_<?=$arResult["filter_id"]?>()" /></p>
	<?
	} //endif ($can_delete_some);
	?>
</form>