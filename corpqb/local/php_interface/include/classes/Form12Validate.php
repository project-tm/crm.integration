<?php

class Form12Validate {

    public function __construct() {
        if (!CModule::IncludeModule("form"))
            die('no form');
    }

    //Проверка
    public function checkFileAdd(&$arResult) {
        $orderType = isset($arResult['arAnswers']['ORDER_TYPE']) ? $arResult['arAnswers']['ORDER_TYPE'] : false;

        if($orderType){
            foreach ($orderType as $item){
                if($item['FIELD_PARAM'] == 'CASH'){
                    $cashAnswerId = $item['ID'];
                }
                if($item['FIELD_PARAM'] == 'BANK'){
                    $bankAnswerId = $item['ID'];
                }
            }

            if(isset($arResult["FORM_ERRORS"]['ORDER_FILE']) && $arResult["arrVALUES"]['form_' . $orderType[0]['FIELD_TYPE'] . '_ORDER_TYPE'] != $bankAnswerId){
                unset($arResult["FORM_ERRORS"]['ORDER_FILE']);
            }
        }
    }

    //Проверка
    public function checkFileUpdate($RESULT_ID, &$arResult) {

        $arAnswerM = CFormResult::GetDataByID($RESULT_ID, array());

        $orderType = isset($arAnswerM['ORDER_TYPE']) ? $arAnswerM['ORDER_TYPE'][0] : false;
        $orderContr = isset($arAnswerM['ORDER_CONTR']) ? $arAnswerM['ORDER_CONTR'][0] : false;
        $orderFile = isset($arAnswerM['ORDER_FILE']) ? $arAnswerM['ORDER_FILE'][0] : false;

        if($orderType && $orderContr){
            foreach ($arResult['arAnswers']['ORDER_TYPE'] as $item){
                if($item['FIELD_PARAM'] == 'CASH'){
                    $cashAnswerId = $item['ID'];
                }
                if($item['FIELD_PARAM'] == 'BANK'){
                    $bankAnswerId = $item['ID'];
                }
            }
            if((isset($arResult["FORM_ERRORS"]['ORDER_FILE']) && $arResult["arrVALUES"]['form_' . $orderType['FIELD_TYPE'] . '_ORDER_TYPE'] != $bankAnswerId) || isset($arAnswerM['ORDER_FILE'])){
                unset($arResult["FORM_ERRORS"]['ORDER_FILE']);
            }
            elseif((!isset($_FILES['form_' . $arResult['arAnswers']['ORDER_FILE'][0]['FIELD_TYPE'] . '_' . $arResult['arAnswers']['ORDER_FILE'][0]['ID']]['name'])
                || empty($_FILES['form_' . $arResult['arAnswers']['ORDER_FILE'][0]['FIELD_TYPE'] . '_' . $arResult['arAnswers']['ORDER_FILE'][0]['ID']]['name']))
                && $arResult["arrVALUES"]['form_' . $orderType['FIELD_TYPE'] . '_' . $orderType['SID']] == $bankAnswerId
                && ($arAnswerM['ORDER_TYPE'][0]['ANSWER_ID'] != $bankAnswerId || !isset($arAnswerM['ORDER_FILE']))){
                $arResult["FORM_ERRORS"]['ORDER_FILE'] = GetMessage("FORM_EMPTY_REQUIRED_FIELDS") . ' ' . $arResult['arQuestions']['ORDER_FILE']['TITLE'];
            }
        }

    }
}
