<?php

namespace helpers;

use Bitrix\Main\Loader;

class FormHelper extends \Singleton
{
    protected $cacheFormFields = array();
    protected $cacheFormParameters = array();
    protected $cacheFormStatus = array();

    public function __construct()
    {
        if (!Loader::includeModule('form')) {
            throw new \Exception('Module "form" disabled!');
        }
    }

    /**
     * Алиас для getAnswerFieldInfo. Для полей, у которых всего 1 ответ.
     * @param $formId
     * @param $fieldCode
     * @return array|mixed
     */
    public function getAnswerFirstFieldInfo($formId, $fieldCode)
    {
        $info = $this->getAnswerFieldInfo($formId, $fieldCode);
        if (is_array($info)) {
            return reset($info);
        }

        return array();
    }

    /**
     * Получает кешированную информацию об ответе формы. Во избежании хардкода.
     * @param $formCode
     * @param $fieldCode
     * @param $answerCode
     * @param bool $cache
     * @return mixed
     */
    public function getAnswerFieldInfo($formCode, $fieldCode, $answerCode = null, $cache = true)
    {
        if(is_int($formCode)){
            $formId = $formCode;
        } else {
            $formId = $this->getFormParameters($formCode, $cache)['ID'];
        }

        if (!isset($this->cacheFormFields[$formId][$fieldCode]) OR !$cache) {
            $by = 's_id';
            $order = 'asc';
            /**
             * Наверное, правильнее выбрать сразу все поля, чем по 10 раз повторять запрос
             */
//            $filter = array('SID' => $fieldCode);
            $filter = array();
            $resField = \CFormField::GetList($formId, 'N', $by, $order, $filter);
            while ($rowField = $resField->Fetch()) {
                /*$filter = array(
                    'MESSAGE' => $answerTitle,
                    'MESSAGE_EXACT_MATCH' => 'Y'
                );*/
                $resAnswer = \CFormAnswer::GetList($rowField['ID'], $by, $order, $filter);
                while ($rowAnswer = $resAnswer->Fetch()) {
                    $rowAnswer['FORM_CODE'] = 'form_' . $rowAnswer['FIELD_TYPE'] . '_' . $rowAnswer['ID'];
                    $rowAnswer['PARAMETERS'] = $this->getAnswerParams($rowAnswer);

                    if (isset($rowAnswer['PARAMETERS']['CODE']) && $answerCode) {
                        $code = $rowAnswer['PARAMETERS']['CODE'][1];
                        $this->cacheFormFields[$formId][$rowField['SID']][$code] = $rowAnswer;
                    } else {
                        $this->cacheFormFields[$formId][$rowField['SID']][] = $rowAnswer;
                    }
                }
            }
        }

        if (is_null($answerCode)) {
            return $this->cacheFormFields[$formId][$fieldCode];
        } else {
            return $this->cacheFormFields[$formId][$fieldCode][$answerCode];
        }
    }

    /**
     * Возвращает параметры формы, в частности - ID по коду
     * @param $code
     * @param bool $cache
     * @return mixed
     */
    public function getFormParameters($code, $cache = true)
    {
        if (!isset($this->cacheFormParameters[$code]) OR !$cache) {
            $by = 's_sort';
            $order = 'asc';
            $filter = array(
                'SID' => $code,
                'SID_EXACT_MATCH' => 'Y'
            );
            $res = \CForm::GetList($by, $order, $filter);
            if ($row = $res->Fetch()) {
                $this->cacheFormParameters[$code] = $row;
            }
        }

        return $this->cacheFormParameters[$code];
    }

    /**
     * Получаем параметры ответа на вопрос
     * @param array $rowAnswer
     * @return array
     */
    protected function getAnswerParams(array $rowAnswer)
    {
        $answerParams = array();
        $params = explode(" ", $rowAnswer['FIELD_PARAM']);
        if ($params) {
            foreach ($params as $param) {
                $paramParts = explode("=", $param);
                if (is_array($paramParts) AND count($paramParts) == 2) {
                    $key = strtoupper($paramParts[0]);
                    $value = str_replace('"', '', $paramParts);
                    $value = str_replace('\'', '', $value);
                    $answerParams[$key] = $value;
                }
            }
        }

        return $answerParams;
    }

    /**
     * Получает информацию о статусе формы
     * @param $formCode
     * @param $statusCode "CSS класс для отображения заголовка статуса". Если не указать, вернёт весь массив статусов формы
     * @param bool $cache
     * @return mixed
     */
    public function getFormStatus($formCode, $statusCode = null, $cache = true)
    {
        if(is_int($formCode)){
            $formId = $formCode;
        } else {
            $formId = $this->getFormParameters($formCode, $cache)['ID'];
        }

        if (empty($this->cacheFormStatus[$formId]) OR !isset($this->cacheFormStatus[$formId][$statusCode]) OR !$cache) {
            $by = 's_sort';
            $order = 'asc';
            $res = \CFormStatus::GetList($formId, $by, $order);
            while ($row = $res->Fetch()) {
                $code = $row['CSS'];
                $row['CODE'] = $code;
                $this->cacheFormStatus[$formId][$code] = $row;
            }
        }

        if (is_null($statusCode) AND !empty($this->cacheFormStatus[$formId])) {
            return $this->cacheFormStatus[$formId];
        } else {
            return $this->cacheFormStatus[$formId][$statusCode];
        }
    }
}