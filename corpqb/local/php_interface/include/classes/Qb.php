<?php
class Qb
{
    public function __construct()
    {
        if (!CModule::IncludeModule("iblock")) die('no iblock');
    }

    //Проверка количества остатка отпуска пользователя
    public function checkUserVacation($dateStart, $dateEnd)
    {
        $dateTimeStart = strtotime($dateStart);
        $dateTimeEnd = strtotime($dateEnd);

        global $USER;
        $userID = $USER->GetID();
        $rsUser = CUser::GetByID($userID);
        $arUser = $rsUser->Fetch();
        $allVacationDays = $arUser['UF_VACATIONDAYS'];

        //Выходные и праздничные дни
        $year_holidays = explode(',', COption::GetOptionString('calendar','year_holidays'));
        $week_holidays = explode('|', COption::GetOptionString('calendar','week_holidays'));

        $unavailableDays = 0;

        //Проверим график отсутствий
        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","DATE_ACTIVE_TO", "PROPERTY_USER", "PROPERTY_ABSENCE_TYPE");
        $arFilter = Array("IBLOCK_ID"=> ABSENCE_IBLOCK_ID, "PROPERTY_USER"=> $userID, "ACTIVE"=> "Y", "PROPERTY_ABSENCE_TYPE" => ABSENCE_PROPERTY_ENUM_ID, 'DATE_ACTIVE_FROM'=> '1.01.'.date('Y'), 'DATE_ACTIVE_TO'=> '31.12.'.date('Y'));
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while($arFields = $res->GetNext())
        {
            for($time = strtotime($arFields['DATE_ACTIVE_FROM']); $time <= strtotime($arFields['DATE_ACTIVE_TO']); $time += 86400)
            {
                $dayCodeBitrix = strtoupper(substr(date('D', $time),0,2)); //Обрезали до 2х символов, сделали буквы большими

                //Если выходной день - пропускаем
                if(!in_array($dayCodeBitrix, $week_holidays) && !in_array(date('d.m.Y', $time), $year_holidays))
                {
                    $unavailableDays++;
                }
            }
        }

        $freeDays = $allVacationDays - $unavailableDays;
        $checkedDays = ($dateTimeEnd - $dateTimeStart) / 86400;

        if($freeDays >= $checkedDays)
        {
            return 'ok';
        }
        else
        {
            return $freeDays;
        }
    }

    public function checkValidDates($dateStart, $dateEnd){
        $dateTimeStart = strtotime($dateStart);
        $dateTimeEnd = strtotime($dateEnd);

        //Дата окончания раньше, чем дата начала
        if($dateTimeEnd < $dateTimeStart)
        {
            return false;
        }
        //Если указана одна и таже дата
        elseif(($dateTimeEnd == $dateTimeStart))
        {
            $rerurnArray=Array(
                'TYPE' => 'ONEDAY',
                'TIMES' => Array(
                    date('d.m.Y 00:00:00', $dateTimeStart),
                    date('d.m.Y 23:59:59', $dateTimeEnd)
                )
            );
        }
        //В рамках одного дня (отпросился на несколько часов)
        elseif(date('d.m.Y', $dateTimeStart) == date('d.m.Y', $dateTimeEnd))
        {
            $rerurnArray=Array(
                'TYPE' => 'HOURS',
                'TIMES' => Array(
                    date('d.m.Y H:i:s', $dateTimeStart),
                    date('d.m.Y H:i:s', $dateTimeEnd)
                )
            );
        }
        else
        {
            $rerurnArray=Array(
                'TYPE' => 'PERIOD',
                'TIMES' => Array(
                    date('d.m.Y 00:00:00', $dateTimeStart),
                    date('d.m.Y 23:59:59', $dateTimeEnd)
                )
            );
        }
        return $rerurnArray;
    }
}