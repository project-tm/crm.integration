<?php

abstract class Singleton
{
    private static $instances;

    protected function __construct()
    {
    }
    private function __clone()
    {
    }
    private function __wakeup()
    {
    }

    final public static function getInstance()
    {
        $className = get_called_class();

        if (isset(self::$instances[$className]) == false) {
            self::$instances[$className] = new static();
        }

        return self::$instances[$className];
    }

}