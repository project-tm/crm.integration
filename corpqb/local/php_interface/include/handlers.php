<?
function my_onAfterResultAdd($WEB_FORM_ID, $RESULT_ID)
{
    $text="";
    switch ($WEB_FORM_ID) {
        case 3:
            $text="Заказан новый пропуск для посетителя.";
        break;
        
        case 4:
            $text="Заказан новый комплект визитных карточек.";
        break;
        
        case 6:
            $text="Заказана курьерская доставка.";
        break;
    }
    
    if ($text) {
        $text.=' <a href="http://b.qb.digital/bitrix/admin/form_result_edit.php?lang=ru&WEB_FORM_ID='.$WEB_FORM_ID.'&RESULT_ID='.$RESULT_ID.'" target="_blank">Перейти к заявке</a>';
        if (CModule::IncludeModule('im'))
        {
            $order = array('sort' => 'asc');
            $tmp = 'sort';
            $filter=array("GROUPS_ID"=>array("18"));
            $rsUsers = CUser::GetList($order, $tmp,$filter);
            while ($arUser=$rsUsers->GetNext()) {
                $arFields = array(
                 
                "MESSAGE_TYPE" => "P",
                "TO_USER_ID" => $arUser["ID"],
                "FROM_USER_ID" => 1,
                "MESSAGE" => $text,
                "AUTHOR_ID" => 1,
                );
                CIMMessenger::Add($arFields);                
            }
        }        
    }
}

function my_onAfterResultUpdate($WEB_FORM_ID, $RESULT_ID)
{
    $text="";
    CModule::IncludeModule("form");
    $dbForm=CFormResult::GetByID($RESULT_ID);
    if ($arForm=$dbForm->GetNext()) {
        print_r($arForm);
        $text='Ваша заявка "'.$arForm["NAME"].'" от '.$arForm["DATE_CREATE"].' переведена в статус "'.$arForm["STATUS_TITLE"].'".';
    }
    if ($text) {
        if (CModule::IncludeModule('im'))
        {
            $arFields = array(
             
            "MESSAGE_TYPE" => "P",
            "TO_USER_ID" => $arForm["USER_ID"],
            "FROM_USER_ID" => 1,
            "MESSAGE" => $text,
            "AUTHOR_ID" => 1,
            );
            CIMMessenger::Add($arFields);                
        }        
    }
}


// зарегистрируем функцию как обработчик двух событий
AddEventHandler('form', 'onAfterResultAdd', 'my_onAfterResultAdd');
AddEventHandler('form', 'onAfterResultUpdate', 'my_onAfterResultUpdate');
