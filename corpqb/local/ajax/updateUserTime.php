<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(
	isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
	!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
	strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' &&
	CModule::IncludeModule("iblock")
)
{
	global $APPLICATION;
	$APPLICATION->RestartBuffer();
	
	if($_POST['EDITDATA']=='Y' && isset($_POST['VALUE']) && strlen($_POST['VALUE'])>0)
	{
		$resultArray = Array(
			'status'=> 'error',
			'value' => false,
		);
		
		if(intval($_POST['ID'])>0)
		{
			CIBlockElement::SetPropertyValuesEx(intval($_POST['ID']), SKUD_IBLOCK_ID, array(120 => intval($_POST['VALUE'])));
			$resultArray['status']='ok';
		}
		elseif(intval($_POST['USER'])>0 && intval($_POST['PERIOD'])>0)
		{
			$el = new CIBlockElement;

			$PROP = array(
				118 => $_POST['USER'],
				119 => $_POST['PERIOD'],
				120 => $_POST['VALUE'],
			);

			$arLoadPeriodArray = Array(
				"MODIFIED_BY"    => $USER->GetID(),
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID"      => SKUD_IBLOCK_ID,
				"PROPERTY_VALUES"=> $PROP,
				"NAME"           => 'USER_ID: '.$_POST['USER'].' PERIOD_ID:'.$_POST['PERIOD'],
				"ACTIVE"         => "Y",
			);

			if($PERIOD_ID = $el->Add($arLoadPeriodArray))
			{
				$resultArray['status']='ok';
				$resultArray['value']=$PERIOD_ID;
			}
			else
			{
				//echo "Error: ".$el->LAST_ERROR;
			}
		}
	}
	echo json_encode($resultArray);
	die();
}