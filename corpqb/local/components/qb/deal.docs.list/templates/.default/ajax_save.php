<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('form');


if (isset($_POST['WEB_FORM_ID']) && (strlen($_REQUEST["web_form_apply"])>0))
{
    $data = $_POST;

    if(isset($_POST['DEAL_ID'])){
        $FIELD_SID = "DOCS_DEAL_ID"; // символьный идентификатор вопроса или поля веб-формы
        $rsField = CFormField::GetBySID($FIELD_SID);
        $arField = $rsField->Fetch();
        $data['form_text_ADDITIONAL_' . $arField['ID']] = $_POST['DEAL_ID'];
    }

    // check errors
    $formErrors = CForm::Check($_POST["WEB_FORM_ID"], $data, false, "Y", "Y");

    if(isset($_POST['RESULT_ID'])){
        unset($formErrors['DOCS_FILE']);
    }

    if (count($formErrors) <= 0)
    {
        // check user session
        if (check_bitrix_sessid())
        {
            $return = false;
            // add result
            if(isset($_POST['RESULT_ID'])){
                if(CFormResult::Update($_POST['RESULT_ID'], $data, 'Y'))
                {

                    // send email notifications
                    CFormCRM::onResultAdded($_POST["WEB_FORM_ID"], $_POST['RESULT_ID']);
                    CFormResult::SetEvent($_POST['RESULT_ID']);
                    CFormResult::Mail($_POST['RESULT_ID']);

                    echo json_encode(['result' => 'ok']);

                }
                else
                {
                    echo json_encode(['result' => 'fail', 'errors' => $GLOBALS["strError"]]);
                }
            }else{
                if($RESULT_ID = CFormResult::Add($_POST["WEB_FORM_ID"], $data))
                {

                    CFormResult::Update($RESULT_ID, $data, 'Y');

                    //$arResult["FORM_NOTE"] = GetMessage("FORM_DATA_SAVED1").$RESULT_ID.GetMessage("FORM_DATA_SAVED2");
                    $arResult["FORM_RESULT"] = 'addok';

                    // send email notifications
                    CFormCRM::onResultAdded($_POST["WEB_FORM_ID"], $RESULT_ID);
                    CFormResult::SetEvent($RESULT_ID);
                    CFormResult::Mail($RESULT_ID);

                    // choose type of user redirect and do it

                    echo json_encode(['result' => 'ok']);

                }
                else
                {
                    echo json_encode(['result' => 'fail', 'errors' => $GLOBALS["strError"]]);
                }
            }
        }
    }else{
        echo json_encode(['result' => 'fail', 'errors' => $formErrors]);
    }
}elseif(isset($_GET['WEB_FORM_ID']) && (isset($_GET['del_id']) || isset($_GET['del_ids']))){
    // deleting single form result
    $del_id = intval($_REQUEST["del_id"]);

    if ($del_id > 0 /* && check_bitrix_sessid()*/) {
        $GLOBALS['strError'] = '';
        CFormResult::Delete($del_id);

        if (strlen($GLOBALS['strError']) <= 0) {
            LocalRedirect($APPLICATION->GetCurPageParam("", array("del_id", "sessid", 'formresult'), false));
            exit();
        }
    }

    // deleting single form result
    $del_ids = $_GET["del_ids"];

    if (count($del_ids) > 0 && check_bitrix_sessid()) {
        foreach ($del_ids as $del_id) {
            CFormResult::Delete($del_id);
        }
    }
}
exit();