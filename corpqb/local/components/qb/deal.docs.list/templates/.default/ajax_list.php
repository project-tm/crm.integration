<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->IncludeComponent("qb:deal.docs.list", ".default", Array(
    "WEB_FORM_ID" => $_REQUEST["WEB_FORM_ID"],	// ID веб-формы
    "SEF_MODE" => "N",	// Включить поддержку ЧПУ
    "SHOW_ADDITIONAL" => "N",	// Показать дополнительные поля веб-формы
    "SHOW_ANSWER_VALUE" => "N",	// Показать значение параметра ANSWER_VALUE
    "SHOW_STATUS" => "Y",	// Показать текущий статус результата
    "NOT_SHOW_FILTER" => "",	// Коды полей которые нельзя показывать в фильтре (через запятую)
    "NOT_SHOW_TABLE" => "",	// Коды полей которые нельзя показывать в таблице
    "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
    "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
    "DEAL_ID" => $_REQUEST["DEAL_ID"],	// Ссылка на дополнительном пункте в навигационной цепочке
),
    false
);