<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
//$this->setFrameMode(true);
//$APPLICATION->AddHeadScript("/bitrix/js/calendar/cal-core.js", false);
//$APPLICATION->AddHeadScript("/bitrix/js/calendar/cal-planner.js", false);
$APPLICATION->AddHeadString('<link href="' . SITE_TEMPLATE_PATH . '/css/main-style.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="' . $this->GetFolder() . '/css/style.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="/bitrix/js/crm/css/crm.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>');
$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.11/js/jquery.dataTables.min.js"></script>');

$isAdmin = false;
$moreColumnCount = 2;

if (CSite::InGroup(array(1))) {
    $moreColumnCount += 1;
    $isAdmin = true;
}
?>
<div id="deal_docs">
    <div class="crm-list-top-bar" id="deal_actions_grid_crm_activity_grid_editor_toolbar">
        <a class="crm-menu-bar-btn btn-new crm-activity-command-add-event"
           href="<?= $this->GetFolder() ?>/ajax_form.php?WEB_FORM_ID=<?= $arParams["WEB_FORM_ID"] ?>&DEAL_ID=<?= $arParams["DEAL_ID"] ?>"
           title="Добавить">
            <span class="crm-toolbar-btn-icon"></span><span>Добавить</span>
        </a>
    </div>
    <br/>
    <form method="post" id="docsEditableForm">
        <table cellpadding='5' cellspacing='0' id='docsTable' class="display" width="100%">
            <thead>
            <tr>
                <th style="padding-left: 10px"><input type="checkbox" class="check_all"/></th>
                <th width="10px"></th>
                <th>Добавил</th>
                <? if (is_array($arResult["arrColumns"])) {
                    foreach ($arResult["arrColumns"] as $arrCol) {
                        ?>
                        <th><?= $arrCol["RESULTS_TABLE_TITLE"] ?></th><?
                    }
                } ?>
                <th>Дата</th>
                <th></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th style="text-align: right"></th>
                <?
                if (is_array($arResult["arrColumns"])) {
                    foreach ($arResult["arrColumns"] as $arrCol) {
                        ?>
                        <th style="<?= $arrCol['SID'] == 'PRICE' ? 'text-align: right;' : '' ?>"></th>
                        <?
                    }
                }
                ?>
                <th></th>
                <th></th>
            </tr>
            </tfoot>
            <? if (count($arResult["arrResults"]) > 0) { ?>
                <tbody>
                <? foreach ($arResult["arrResults"] as $arRes) { ?>
                    <tr>
                        <td><input type="checkbox" name="check_order[]" class="chBox" value="<?= $arRes["ID"] ?>">
                        </td>
                        <td style="text-align: right"></td>
                        <td><?= $arRes['USER_FIRST_NAME'] . ' ' . $arRes['USER_SECOND_NAME'] . ' ' . $arRes['USER_LAST_NAME'] ?></td>
                        <? foreach ($arResult["arrColumns"] as $FIELD_ID => $arrC) { ?>
                            <?php
                            $firstElem = current($arResult["arrAnswers"][$arRes["ID"]][$FIELD_ID]);
                            ?>
                            <td>
                                <?
                                $entityInfo = [];
                                $arrAnswer = $arResult["arrAnswers"][$arRes["ID"]][$FIELD_ID];
                                if (is_array($arrAnswer)) {
                                    foreach ($arrAnswer as $key => $arrA) {
                                        $answerText = "";
                                        if (strlen(trim($arrA["USER_TEXT"])) > 0) {
                                            $answerText = $arrA["USER_TEXT"];
                                        }
                                        if (strlen(trim($arrA["ANSWER_TEXT"])) > 0) {
                                            $answerText = $arrA["ANSWER_TEXT"];
                                        }

                                        if ($arrA['FIELD_TYPE'] == 'file' && $arFile = CFormResult::GetFileByAnswerID($arRes["ID"], $arrA['ANSWER_ID'])) {
                                            echo "<a class=\"tablebodylink\" href=\"/bitrix/tools/form_show_file.php?rid=" . $arRes['ID'] . "&hash=" . $arFile["USER_FILE_HASH"] . "&lang=" . LANGUAGE_ID . "&action=download\">" . $answerText . "</a> (" . CFile::FormatSize($arFile["USER_FILE_SIZE"]) . ")";
                                        } else {
                                            echo $answerText;
                                        }
                                        //}
                                    } //foreach
                                } // endif (is_array($arrAnswer));
                                ?>
                            </td>
                            <?
                        } //foreach
                        ?>
                        <td width="150px"><?= $arRes['DATE_CREATE'] ?></td>
                        <td width="50px">
                            <a href="<?= $this->GetFolder() ?>/ajax_form.php?WEB_FORM_ID=<?= $arParams["WEB_FORM_ID"] ?>&RESULT_ID=<?= $arRes['ID'] ?>&DEAL_ID=<?= $arParams["DEAL_ID"] ?>"
                               title="Редактировать" class="action_btn_link">
                                <span class="feed-destination-edit"></span>
                            </a>
                            <a href="<?= $this->GetFolder() ?>/ajax_save.php?WEB_FORM_ID=<?= $arParams["WEB_FORM_ID"] ?>&del_id=<?= $arRes['ID'] ?>"
                               title="Удалить" class="action_btn_link">
                                <span class="feed-destination-delete"></span>
                            </a>
                        </td>
                    </tr>
                    <?
                } //foreach
                ?>
                </tbody>
                <?
            } ?>
        </table>

        <div class="control_panel">
            <div class="btn btn-del">Удалить</div>
        </div>
    </form>
</div>
<? CUtil::InitJSCore(array('window')); ?>
<script>
    var Dialog;
    var ppd = false;
    $(document).ready(function () {
        $('#deal_docs').on('click', '.feed-destination-edit, .crm-activity-command-add-event', function (e) {
            e.preventDefault();
            var aAAA = $(this).closest('a');
            var form = $.get($(aAAA).attr('href'), function (data) {
                Dialog = new BX.CDialog({
                    content: data,
                    icon: 'head-block',

                    resizable: true,
                    draggable: true,
                    height: '100%',
                    width: '100%',
                    buttons: false
                });
                Dialog.Show();
            });
        });

        $('body').on('click', '#cancel', function () {
            Dialog.Close();
        });

        $('body').on('submit', 'form[name=AJAX_DOCS]', function (event) {
            event.preventDefault();
            var self = $(this);
            var url = self.attr('action');
            var formData = new FormData($(this)[0]);
            if (!ppd) {
                ppd = true;
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: formData,
                    //async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: function (returndata) {
                        if (returndata['errors'] == undefined) {
                            self.find('.deal_form_result').html('<span style="color:green">Изменения сохранены</span>');

                            setTimeout(function () {
                                Dialog.Close();
                            }, 700);
                            $.get('<?=$this->GetFolder() ?>/ajax_list.php', {
                                WEB_FORM_ID: <?=$arParams['WEB_FORM_ID']?>,
                                DEAL_ID: <?=$arParams['DEAL_ID']?>}, function (data) {
                                $('#deal_docs').replaceWith(data);
                                ppd = false;
                            });
                        } else {
                            var res = '<ul>';
                            for (var key in returndata.errors) {
                                res += '<li>' + returndata.errors[key] + '</li>';
                            }
                            res += '</ul>';
                            self.find('.deal_form_result').html('<span style="color:red">' + res + '</span>');
                            ppd = false;
                        }
                    },
                    error: function () {
                        ppd = false;
                    }
                });
            }
            return false;
        });

        $('body').on('submit', 'form[name=DEAL_DOCS]', function (event) {
            event.preventDefault();
            var formData = new FormData($(this)[0]);
            var self = $(this);
            var url = self.attr('action');
            console.log(ppd);
            if (!ppd) {
                ppd = true;
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: formData,
                    //async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: function (returndata) {
                        if (returndata.errors == undefined) {
                            self.find('.deal_form_result').html('<span style="color:green">Добавлено!</span>');
                            self.find('input[type=text]').each(function (indx, elem) {
                                $(elem).val('');
                            });

                            setTimeout(function () {
                                Dialog.Close();
                            }, 700);
                            $.get('<?=$this->GetFolder() ?>/ajax_list.php', {
                                WEB_FORM_ID: <?=$arParams['WEB_FORM_ID']?>,
                                DEAL_ID: <?=$arParams['DEAL_ID']?>}, function (data) {
                                $('#deal_docs').replaceWith(data);
                                ppd = false;
                            });
                        } else {
                            var res = '<ul>';
                            for (var key in returndata.errors) {
                                res += '<li>' + returndata.errors[key] + '</li>';
                            }
                            res += '</ul>';
                            self.find('.deal_form_result').html('<span style="color:red">' + res + '</span>');
                            ppd = false;
                        }
                    },
                    error: function () {
                        ppd = false;
                    }
                });
            }
            return false;
        });


        $('#docsEditableForm').on('click', '.btn-del', function (e) {
            var trEditableArr = $('#docsTable').find('input[type=checkbox]:checked').closest('tr');
            var data = {};
            var ids = [];
            data['sessid'] = '<?=bitrix_sessid()?>';

            trEditableArr.each(function (indx, element) {
                ids.push($(element).find('input.chBox').val());
            });

            data['del_ids'] = ids;
            $.ajax({
                type: 'GET',
                data: data,
                success: function () {
                    trEditableArr.each(function (indx, elementTr) {
                        elementTr.remove();
                        $('#docsEditableForm input.check_all').prop("checked", false);
                    });

                    $.get('<?=$this->GetFolder() ?>/ajax_list.php', {
                        WEB_FORM_ID: <?=$arParams['WEB_FORM_ID']?>,
                        DEAL_ID: <?=$arParams['DEAL_ID']?>}, function (data) {
                        $('#deal_docs').replaceWith(data);
                    });
                }
            });
        });

        $('#docsEditableForm').on('click', '.feed-destination-delete', function (e) {
            e.preventDefault();
            var self = $(this);

            if (!ppd) {
                ppd = true;
                $.ajax({
                    type: 'GET',
                    url: self.parent().attr('href'),
                    success: function () {
                        self.closest('tr').remove();
                        ppd = false;

                        $.get('<?=$this->GetFolder() ?>/ajax_list.php', {
                            WEB_FORM_ID: <?=$arParams['WEB_FORM_ID']?>,
                            DEAL_ID: <?=$arParams['DEAL_ID']?>}, function (data) {
                            $('#deal_docs').replaceWith(data);
                        });
                    }
                });
            }
        });

        $('#docsEditableForm').on('click', 'input.check_all', function (e) {
            $('#docsTable').find('input[type=checkbox]').prop("checked", $(this).prop("checked"));
        });

        var t = $('#docsTable').DataTable({
            "columnDefs": [
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                },
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 1
                }

            ],
            "info": false,
            "paging": false,
            "order": [[2, 'asc']],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Russian.json"
            },
        });

        t.on('order.dt search.dt', function () {
            t.column(1, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

    });
</script>