<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if(isset($_REQUEST["RESULT_ID"])){
    $APPLICATION->IncludeComponent("qb:form.result.edit", "deal_docs", array(
        "RESULT_ID" => $_REQUEST["RESULT_ID"],
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "Y",
        "SEF_MODE" => "N",
        "SEF_FOLDER" => "",
        "EDIT_ADDITIONAL" => "N",
        "EDIT_STATUS" => "N",
        "LIST_URL" => $_SERVER['HTTP_REFERER'],
        "VIEW_URL" => "",
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => "",
        "AJAX_OPTION_STYLE" => "Y",
        "DEAL_ID" => $_REQUEST["DEAL_ID"]
    ),
        false
    );
}else{
    $APPLICATION->IncludeComponent("qb:deal.docs.new", ".default", array(
        "SEF_MODE" => "N",	// Включить поддержку ЧПУ
        "WEB_FORM_ID" => $_REQUEST["WEB_FORM_ID"],	// ID веб-формы
        "IGNORE_CUSTOM_TEMPLATE" => "Y",	// Игнорировать свой шаблон
        "USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
        "CACHE_TYPE" => "A",	// Тип кеширования
        "CACHE_TIME" => "3600",	// Время кеширования (сек.)
        "DEAL_ID" => $_REQUEST["DEAL_ID"]
    ),
        false
    );
}