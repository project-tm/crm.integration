<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->AddHeadString('<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>');
$APPLICATION->AddHeadString('<link href="/bitrix/js/crm/css/crm.css" type="text/css" rel="stylesheet">');
?>

<?php

$orderType = isset($arResult['arAnswers']['ORDER_TYPE']) ? $arResult['arAnswers']['ORDER_TYPE'] : false;

if($orderType){
    foreach ($orderType as $item){
        if($item['FIELD_PARAM'] == 'CASH'){
            $cashAnswerId = $item['ID'];
        }
        if($item['FIELD_PARAM'] == 'BANK'){
            $bankAnswerId = $item['ID'];
        }
    }

    $orderFieldId = $arResult['arAnswers']['ORDER_FILE'][0]['ID'];
}

CModule::IncludeModule('crm');

if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y")
{
?>
<?=$arResult["FORM_HEADER"]?>

<table>
<?
if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y")
{
?>
	<tr>
		<td><?
/***********************************************************************************
					form header
***********************************************************************************/
if ($arResult["isFormTitle"])
{
?>
	<h3><?=$arResult["FORM_TITLE"]?></h3>
<?
} //endif ;

	if ($arResult["isFormImage"] == "Y")
	{
	?>
	<a href="<?=$arResult["FORM_IMAGE"]["URL"]?>" target="_blank" alt="<?=GetMessage("FORM_ENLARGE")?>"><img src="<?=$arResult["FORM_IMAGE"]["URL"]?>" <?if($arResult["FORM_IMAGE"]["WIDTH"] > 300):?>width="300"<?elseif($arResult["FORM_IMAGE"]["HEIGHT"] > 200):?>height="200"<?else:?><?=$arResult["FORM_IMAGE"]["ATTR"]?><?endif;?> hspace="3" vscape="3" border="0" /></a>
	<?//=$arResult["FORM_IMAGE"]["HTML_CODE"]?>
	<?
	} //endif
	?>

			<p><?=$arResult["FORM_DESCRIPTION"]?></p>
		</td>
	</tr>
	<?
} // endif
	?>
</table>
<br />
<?
/***********************************************************************************
						form questions
***********************************************************************************/
?>
<table class="form-table data-table">
	<tbody>
	<?
	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
	{
		if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
		{
			echo $arQuestion["HTML_CODE"];
		}
		else
		{
	?>
            <tr class="<?= ($FIELD_SID == 'ORDER_FILE' ? 'order_file hidden' : '') ?> <?= (isset($_REQUEST['form_radio_ORDER_TYPE']) && $_REQUEST['form_radio_ORDER_TYPE'] != 72) ? 'hide' : '' ?>">
            <? if($FIELD_SID == 'ORDER_TYPE') : ?>
                <input type="hidden" value="<?=$arResult['arrVALUES']['form_radio_ORDER_TYPE']?>" name="form_radio_ORDER_TYPE" />
            <? else :?>
			<td>
				<?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
				<span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
				<?endif;?>
				<?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?>
				<?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />".$arQuestion["IMAGE"]["HTML_CODE"] : ""?>
			</td>
            <td><?=$arQuestion["HTML_CODE"]?></td>
            <? endif; ?>
		</tr>
	<?
		}
	} //endwhile
	?>
<?
if($arResult["isUseCaptcha"] == "Y")
{
?>
		<tr>
			<th colspan="2"><b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b></th>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" /></td>
		</tr>
		<tr>
			<td><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?></td>
			<td><input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" /></td>
		</tr>
<?
} // isUseCaptcha
?>
	</tbody>
	<tfoot>
		<tr>
			<th colspan="2">
				<input <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="Добавить" />
<!--				--><?//if ($arResult["F_RIGHT"] >= 15):?>
<!--				&nbsp;<input type="hidden" name="web_form_apply" value="Y" /><input type="submit" name="web_form_apply" value="--><?//=GetMessage("FORM_APPLY")?><!--" />-->
<!--				--><?//endif;?>
<!--				&nbsp;<input type="reset" value="--><?//=GetMessage("FORM_RESET");?><!--" />-->
			</th>
		</tr>
	</tfoot>
</table>
<p>
<?=$arResult["REQUIRED_SIGN"];?> - <?=GetMessage("FORM_REQUIRED_FIELDS")?>
</p>
<?=$arResult["FORM_FOOTER"]?>
<?
} //endif (isFormNote)
?>
<script>
	$(document).ready(function() {

		var fileee = $('input[name=form_file_<?=$orderFieldId?>]');
		var elem = $('.order_file').prev('tr').find('input');
		var $preval = $(elem).val();

		setInterval(function(){
			var curVal = $(elem).val();
			if ($preval !== curVal) {
				elem.change();
			}
		}, 100);

		$(elem).on('change',function(){
			if($('.crm-block-cont-block-company').css('display')=='block' || $('#crm-payform_CHANGE_BTN_form_text_75_payform_ENTITY_SELECTOR_form_text_75-block-search').hasClass('crm-block-cont-block-company')){
				$('input[name=form_radio_ORDER_TYPE]').val('<?=$bankAnswerId?>');
				$('tr.order_file').show();
				$('tr.order_file #form_file_<?=$orderFieldId?>_del').prop('checked', true);
				fileee.insertBefore('.bx-input-file-desc');
			}else{
				$('input[name=form_radio_ORDER_TYPE]').val('<?=$cashAnswerId?>');
				$('tr.order_file').hide();
				$('tr.order_file #form_file_<?=$orderFieldId?>_del').prop('checked', true);
				fileee.detach();
			}
		})
    });

</script>
