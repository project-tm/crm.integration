<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
    <style>
        .input_file_del {
            display: none;
        }
    </style>
<?= sprintf( // form header (<form> tag and hidden inputs)
    "<form name=\"%s\" action=\"%s\" method=\"%s\" enctype=\"multipart/form-data\">",
    'AJAX_DOCS', '/local/components/qb/deal.docs.list/templates/.default/ajax_save.php', "POST"
) ?>
<?= bitrix_sessid_post() ?>

<? if ($arResult["FORM_SIMPLE"] == "N" && $arResult["isResultStatusChangeAccess"] == "Y" && $arParams["EDIT_STATUS"] == "Y") {
    ?>
    <p>
        <b><?= GetMessage("FORM_CURRENT_STATUS") ?></b>
        [<?= $arResult["RESULT_STATUS"] ?>]
        <?= GetMessage("FORM_CHANGE_TO") ?>
        <?= $arResult["RESULT_STATUS_FORM"] ?>
    </p>
    <?
}
?>
<div class="deal_form_result"></div>
<? if ($arResult["FORM_NOTE"]): ?><?= $arResult["FORM_NOTE"] ?><? endif ?>
<? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>
    <input type="hidden" name="WEB_FORM_ID" value="<?= $arParams['WEB_FORM_ID'] ?>">
    <input type="hidden" name="RESULT_ID" value="<?= $arParams['RESULT_ID'] ?>">
    <input type="hidden" name="DEAL_ID" value="<?= $arParams['DEAL_ID'] ?>">
    <input type="hidden" name="web_form_apply" value="1">
    <table class="form-table my-data-table" id="deal_docs_table">
        <thead>
        <tr>
            <th colspan="2">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <?
        foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
            ?>
            <tr>
                <td>
                    <? if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
                        <span class="error-fld" title="<?= $arResult["FORM_ERRORS"][$FIELD_SID] ?>"></span>
                    <? endif; ?>
                    <?= $arQuestion["CAPTION"] ?><?= $arResult["arQuestions"][$FIELD_SID]["REQUIRED"] == "Y" ? $arResult["REQUIRED_SIGN"] : "" ?>
                    <?= $arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />" . $arQuestion["IMAGE"]["HTML_CODE"] : "" ?>
                </td>
                <td><?= $arQuestion["HTML_CODE"] ?></td>
            </tr>
            <?
        } //endwhile
        ?>
        </tbody>
        <tfoot>
        <tr>
            <th colspan="2">
                <br/>
                <br/>
                <input type="submit" name="web_form_apply" value="Сохранить"/>
                <input type="button" name="cancel" value="Закрыть" id="cancel">
            </th>
        </tr>
        </tfoot>
    </table>
<?= $arResult["FORM_FOOTER"] ?>