<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("QB_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("QB_COMPONENT_DESC"),
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "qb",
		"CHILD" => array(
			"ID" => "skud",
			"NAME" => GetMessage("QB_COMPONENT_NAME")
		)
	),
);

?>