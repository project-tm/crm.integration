<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
$APPLICATION->AddHeadScript("/bitrix/js/calendar/cal-core.js", false);
$APPLICATION->AddHeadScript("/bitrix/js/calendar/cal-planner.js", false);
$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/css/main-style.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>'); 
$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.11/js/jquery.dataTables.min.js"></script>'); 

?>
<div class="skudWrapper">
	<div class="dateFilter">
		<form  action="" method="get">
			<input type='hidden' name="AJAXCALL" value="Y" />
			<div>Укажите интервал:</div>
			<div>
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.calendar",
					"",
					Array(
						"COMPONENT_TEMPLATE" => ".default",
						"FORM_NAME" => "",
						"HIDE_TIMEBAR" => "N",
						"INPUT_NAME" => "DATE_FILTER_FROM",
						"INPUT_NAME_FINISH" => "DATE_FILTER_TO",
						"INPUT_VALUE" => $_REQUEST['DATE_FILTER_FROM'],
						"INPUT_VALUE_FINISH" => $_REQUEST['DATE_FILTER_TO'],
						"SHOW_INPUT" => "Y",
						"SHOW_TIME" => "N"
					),false,
					Array('HIDE_ICONS'=>'Y')
				);
				?>
			</div>
			<button type="submit" class="filterBlock" id="skudFilter">Фильтровать</div>
		</form>
		
	</div>
	<div id="ajaxSkudWrapper">
		<?/*if($_REQUEST['AJAXCALL'] == 'Y')
		{
			global $APPLICATION;
			$APPLICATION->RestartBuffer();
		}
		?>
		<?if(isset($arResult['SELECTTIME'])):?>
			<div class="monthSelect">
				<a href='<?=$arResult['SELECTTIME']['PREV'];?>'>←</a><span><?=$arResult['SELECTTIME']['MONTH'];?></span><a href='<?=$arResult['SELECTTIME']['NEXT'];?>'>→</a>
			</div>
		<?endif;*/?>
		<?/*<div class="filter-wrap">
			<div class="input-block fl">
				<input type="text" placeholder="Поиск по сотруднику"/>
			</div>
			<div class="monthSelect fr">
				<a href="1451595600">←</a><span class="date-open">Февраль, 2016</span><a href="1456779600">→</a>
				<input class="data-table" size="60" value="">
			</div>
			<div class="cl"></div>
		</div>*/?>
		<table cellpadding='5' cellspacing='0' id='skudTable'>
			<thead>
				<tr>
					<td>ФИО</td>
					<?/*<td>Должен</td>
					<td>Отработал</td>
					<td>%</td>*/?>
					<td>Отпуск</td>
					<?/*<td>Командировка</td>*/?>
					<td>Больничный</td>
					<td>За свой счет</td>
					<?/*<td>К вычету</td>*/?>
				</tr>
			</thead>
			<tbody>
				<?foreach($arResult['EMPLOYEE'] as $user):
				    if ($user["ACTIVE"]!="Y") continue;?>
					<tr>
						<td class="emplName"><?=$user['NAME'];?></td>
						<?/*<td class="required"><?=$user['TIME']['REQUIRED'];?></td>
						<?if(isset($arResult['SELECTTIME']) && $arResult['PERIOD_ID'][0]):?>
							<td><input class="realUserTime" data-id="<?=$user['TIME']['REAL_ID'];?>" data-period-id="<?=$arResult['PERIOD_ID'][0];?>" data-user-id='<?=$user['USERID'];?>' value="<?=$user['TIME']['REAL'];?>" type="number"/></td>
						<?else:?>
							<td><?=$user['TIME']['REAL'];?></td>
						<?endif;?>
						<td class="percent"><?=$user['TIME']['PERCENT'];?></td>*/?>
						<td><?=($user['ABSENCE']['334'])?intval($user['ABSENCE']['334']):'—';?></td>
						<?/*<td><?=($user['ABSENCE']['9'])?$user['ABSENCE']['9']:'—';?></td>*/?>
						<td><?=($user['ABSENCE']['10'])?intval($user['ABSENCE']['10']):'—';?></td>
						<td><?=($user['ABSENCE']['8'])?intval($user['ABSENCE']['8']):'—';?></td>
						<?/*<td>-</td>*/?>
					</tr>
				<?endforeach;?>
			</tbody>
		</table>
		<?
		/*echo '<pre>';
		print_r($arResult);
		echo '</pre>';*/
		?>
		<?if($_REQUEST['AJAXCALL'] == 'Y')
		{
			die();
		}
		?>
	</div>
	<div class="cl"></div>
</div>