<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

if(!CModule::IncludeModule("iblock")) die();

global $USER;
$arGroups = $USER->GetUserGroup($USER->GetId());
if(!in_array(BOSSES_GROUP_ID, $arGroups)){
	//LocalRedirect(SITE_DIR);
	ShowMessage("Ошибка! Недостаточно прав для просмотра данного раздела!!!");
}
elseif($this->StartResultCache())
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
	}
	
	$filterDateFrom = strtotime($_REQUEST['DATE_FILTER_FROM'].' 00:00:00');
	$filterDateTo = strtotime($_REQUEST['DATE_FILTER_TO'].' 23:59:59');
	
	$periodCount = 1;
	$monthTime = ($_REQUEST['TIME'])?$_REQUEST['TIME']:time();

	if(isset($_REQUEST['DATE_FILTER_FROM']) || isset($_REQUEST['DATE_FILTER_TO']))
	{
		$intervalFilter = array(
			"LOGIC" => "OR",
			array(
				"><DATE_ACTIVE_FROM" => array($_REQUEST['DATE_FILTER_FROM'], $_REQUEST['DATE_FILTER_TO']),
				">DATE_ACTIVE_TO" => $_REQUEST['DATE_FILTER_TO'] 
			), 
			array( 
				"><DATE_ACTIVE_TO" => array($_REQUEST['DATE_FILTER_FROM'], $_REQUEST['DATE_FILTER_TO']), 
				"<DATE_ACTIVE_FROM"=>$_REQUEST['DATE_FILTER_FROM'] 
			), 
			array( 
				"<DATE_ACTIVE_FROM" => $_REQUEST['DATE_FILTER_FROM'],
				">DATE_ACTIVE_TO" => $_REQUEST['DATE_FILTER_TO']
			),
			array( 
				"><DATE_ACTIVE_FROM" => array($_REQUEST['DATE_FILTER_FROM'], $_REQUEST['DATE_FILTER_TO']),
				"><DATE_ACTIVE_TO" => array($_REQUEST['DATE_FILTER_FROM'], $_REQUEST['DATE_FILTER_TO']) 
			)
		);
		$periodCount = 1000;
	}
	else
	{
		$arResult['SELECTTIME'] = Array(
			'TIME' => $monthTime,
			'MONTH' => FormatDate("f", $monthTime).', '.date('Y', $monthTime),
			'NEXT' => mktime(0, 0, 0, date('m',$monthTime)+1, 1, date('Y',$monthTime)),
			'PREV' => mktime(0, 0, 0, date('m',$monthTime)-1, 1, date('Y',$monthTime))
		);
		
		$intervalFilter = array(
			"LOGIC" => "AND",
			array(
				">=DATE_ACTIVE_FROM" => date('1.m.Y', $monthTime),
				"<=DATE_ACTIVE_TO" => date('t.m.Y', $monthTime)
			)
		);
	}
	
	//Получим имеющиеся интервалы попадающие под фильтр
	$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","DATE_ACTIVE_TO","PROPERTY_HOURS");
	$arFilter = Array("IBLOCK_ID"=> PERIOD_IBLOCK_ID, "ACTIVE"=>"Y");
	$arFilter[]= $intervalFilter;

	$res = CIBlockElement::GetList(Array('DATE_ACTIVE_FROM'=>'DESC'), $arFilter, false, Array("nPageSize"=> $periodCount), $arSelect);
	while($arFields = $res->GetNext())
	{
		$arResult['PERIODS'][$arFields['ID']] = Array(
			"ID" => $arFields['ID'],
			"HOURS" => intval($arFields['PROPERTY_HOURS_VALUE']),
			"ACTIVE_FROM" => $arFields['DATE_ACTIVE_FROM'],
			"ACTIVE_TO" => $arFields['DATE_ACTIVE_TO'],
		);
		$arResult['PERIOD_ID'][] = $arFields['ID'];
	}
	
	if(!$arResult['PERIOD_ID'])
	{
		$arResult['PERIOD_ID'] = 0;
	}
	
	//Получим список пользователей
	$rsUsers = CUser::GetList($by = "", $order = "", $filter, array("SELECT" => array("UF_*")));
	$is_filtered = $rsUsers->is_filtered;
	while($arUser = $rsUsers->GetNext())
	{
		if((!empty($arUser['UF_DATE_START']) && $monthTime >= strtotime($arUser['UF_DATE_START'])) &&
		((!empty($arUser['UF_DATE_END']) && $monthTime <= strtotime($arUser['UF_DATE_END'])) || empty($arUser['UF_DATE_END']))
		){
			$arResult['EMPLOYEE'][$arUser['ID']]= Array(
				"USERID" => $arUser['ID'],
				"NAME"=>false,
				"ACTIVE"=>$arUser["ACTIVE"],
				"TIME"=>Array(),
			);
		}
	}
	//Получим данные СКУД
	$arSelect = Array("ID", "NAME","PROPERTY_HOURS","PROPERTY_EMPLOYEE","PROPERTY_PERIOD");
	$arFilter = Array("IBLOCK_ID"=>SKUD_IBLOCK_ID, "ACTIVE"=>"Y","PROPERTY_PERIOD" => $arResult['PERIOD_ID']);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($arFields = $res->GetNext())
	{
		//Посчитаем суммарное(фактическое время) время за отчетный период
		if(isset($arResult['EMPLOYEE'][$arFields['PROPERTY_EMPLOYEE_VALUE']]))
		{
			$arResult['EMPLOYEE'][$arFields['PROPERTY_EMPLOYEE_VALUE']]['TIME']['REAL'] += intval($arFields['PROPERTY_HOURS_VALUE']);
			$arResult['EMPLOYEE'][$arFields['PROPERTY_EMPLOYEE_VALUE']]['TIME']['REAL_ID'] = intval($arFields['ID']);			
			
			//И время, которое должен был отработать сотрудник
			if(isset($arResult['PERIODS'][$arFields['PROPERTY_PERIOD_VALUE']]))
			{
				$arResult['EMPLOYEE'][$arFields['PROPERTY_EMPLOYEE_VALUE']]['TIME']['REQUIRED'] += intval($arResult['PERIODS'][$arFields['PROPERTY_PERIOD_VALUE']]['HOURS']);
			}
		}
	}
	
	
	//Выходные и праздничные дни
	$year_holidays = explode(',', COption::GetOptionString('calendar','year_holidays'));
	$week_holidays = explode('|', COption::GetOptionString('calendar','week_holidays'));
	
	//Рабочее время
	$work_time_start = COption::GetOptionString('calendar','work_time_start');
	$work_time_end = COption::GetOptionString('calendar','work_time_end');
	
	
	//Проверим график отсутствий
	$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","DATE_ACTIVE_TO", "PROPERTY_USER", "PROPERTY_ABSENCE_TYPE");
	$arFilter = Array("IBLOCK_ID"=>ABSENCE_IBLOCK_ID, "ACTIVE"=>"Y");
	$arFilter[]= $intervalFilter;
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($arFields = $res->GetNext())
	{
		$requiredTime = 0;
		$workTime = 0;
		$work_time_diff = $work_time_end - $work_time_start;
		
		//Если информация о пользователе отсутствующем в интервале - пропускаем
		if(!isset($arResult['EMPLOYEE'][$arFields['PROPERTY_USER_VALUE']])) continue;
			
		$unixTimeFrom = strtotime($arFields['DATE_ACTIVE_FROM']);//unixtime начала
		$unixTimeTo = strtotime($arFields['DATE_ACTIVE_TO']);//unixtime конца
		
		if($unixTimeFrom == $unixTimeTo){
			$unixTimeTo = strtotime($arFields['DATE_ACTIVE_TO'].' 23:59:59');
		}
		if(($unixTimeTo - $unixTimeFrom)/3600 > 8){
			$work_time_diff = 8;
		}
		
		$unixTimeStartDay = strtotime(date('d.m.Y '.$work_time_start.':00:00', $unixTimeFrom));
		$unixTimeEndDay = strtotime(date('d.m.Y '.$work_time_end.':00:00', $unixTimeTo));
		$unixTimeStartLastDay = strtotime(date('d.m.Y '.$work_time_start.':00:00', $unixTimeTo));
		
		
		if($unixTimeFrom >= $unixTimeStartDay && $unixTimeFrom <= $unixTimeEndDay)
		{
			$workTime += $unixTimeFrom - $unixTimeStartDay;
		}
		
		if($unixTimeTo >= $unixTimeStartLastDay && $unixTimeTo <= $unixTimeEndDay)
		{
			$workTime += $unixTimeEndDay - $unixTimeTo;
		}
		
		
		for($time = $unixTimeStartDay; $time < $unixTimeEndDay; $time += 86400)
		{
			$setFilter = false;
			//Если установлен фильтр
			if(($filterDateFrom && $time < $filterDateFrom) || ($filterDateTo && $time > $filterDateTo)){
				$setFilter = true;
			}

			$dayCodeBitrix = strtoupper(substr(date('D', $time),0,2)); //Обрезали до 2х символов, сделали буквы большими
			//Если выходной день - пропускаем
			if(!in_array($dayCodeBitrix, $week_holidays) && !in_array(date('d.m.Y', $time), $year_holidays) && $setFilter == false)
			{
				$requiredTime += $work_time_diff * 3600;
			}
		}

		$arResult['EMPLOYEE'][$arFields['PROPERTY_USER_VALUE']]['ABSENCE'][$arFields['PROPERTY_ABSENCE_TYPE_ENUM_ID']] += round(($requiredTime - $workTime) / 3600, 2);
	}
	
	function print_pre($arr){
		echo '<pre>';
		print_r($arr);
		echo '</pre>';
	}

	//Теперь пройдемся по сотрудникам и рассчитаем данные для каждого
	foreach($arResult['EMPLOYEE'] as &$user)
	{
		$rsUser = CUser::GetByID($user['USERID']);
		$arUser = $rsUser->Fetch();
		$user['NAME']=$arUser['LAST_NAME'].' '.$arUser['NAME'];
		
		$percent = $user['TIME']['REAL'] / $user['TIME']['REQUIRED'] * 100;
		$user['TIME']['PERCENT'] = round($percent, 2);
		
		if(!isset($user['TIME']['REQUIRED']))
		{
			if(isset($arResult['PERIODS'][current($arResult['PERIOD_ID'])]))
			{
				$user['TIME']['REQUIRED'] = $arResult['PERIODS'][current($arResult['PERIOD_ID'])]['HOURS'];
			}
			else
			{
				$user['TIME']['REQUIRED'] = 'н/д';
			}
		}
		
		if(isset($arResult['ABSENCE'][$user['USERID']]))
		{
			$user['ABSENCE'] = $arResult['ABSENCE'][$user['USERID']];
			unset($arResult['ABSENCE'][$user['USERID']]);
		}
	}
	$this->IncludeComponentTemplate();
}
?>