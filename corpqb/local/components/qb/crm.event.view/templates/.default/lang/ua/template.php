<?
$MESS["CRM_EVENT_TABLE_EMPTY"] = "— Немає даних —";
$MESS["CRM_EVENT_TABLE_FILES"] = "Файли";
$MESS["CRM_EVENT_ENTITY_TYPE_DEAL"] = "Угода";
$MESS["CRM_EVENT_ENTITY_TYPE_COMPANY"] = "Компанія";
$MESS["CRM_EVENT_ENTITY_TYPE_CONTACT"] = "Контакт";
$MESS["CRM_EVENT_ENTITY_TYPE_LEAD"] = "Лід";
$MESS["CRM_EVENT_DELETE"] = "Видалити";
$MESS["CRM_EVENT_DELETE_TITLE"] = "Видалити подію";
$MESS["CRM_EVENT_DELETE_CONFIRM"] = "Ви впевнені, що хочете видалити?";
$MESS["CRM_ALL"] = "Усього";
$MESS["CRM_IMPORT_EVENT"] = "Якщо e-mail контрагента зазначений в картці CRM, то ви можете автоматично зберігати вашу переписку з ним електронною поштою як Подію. Для цього перешліть отриманий лист за адресою <b>%EMAIL%</b> і система автоматично додасть текст та прикріплені файли в якості Події до цього контрагенту.";
$MESS["CRM_EVENT_VIEW_ADD_SHORT"] = "Подія";
$MESS["CRM_EVENT_VIEW_ADD"] = "Додати подію";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER_SHORT"] = "Фільтр";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER"] = "Показати / приховати форму фільтра";
$MESS["CRM_EVENT_ENTITY_TYPE_QUOTE"] = "Пропозиція";
$MESS["CRM_SHOW_ROW_COUNT"] = "Показати кількість";
?>