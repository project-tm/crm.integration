<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

ob_start();

if (CModule::IncludeModule("form")) {
    $rs = CForm::GetBySID('DEAL_BILLING');
    $FORM = $rs->Fetch();
}
$rsUsers = CUser::GetList(
    $order,
    $tmp,
    $filter,
    array("SELECT"=>array('ID', 'NAME', 'LAST_NAME'))
);
while ($state = $rsUsers->GetNext()) {
    $arResult['USERS'][] = array(
        'ID'   => $state['ID'],
        'NAME' => $state['NAME'] . ' '. $state['LAST_NAME']
    );
};

// Функция для изменения значений гет-параметров в урле, для сохранения фильтров в выгрузке

function sgp(
    $url,
    $varname,
    $value = NULL, // если NULL - убираем переменную совсем
    $clean = TRUE // превращать ли ?one=&two=
) {              // в ?one&two (так адрес красивее)

    // Версия функции "substitue get parameter" без регулярных выражений

    if (is_array($varname)) {
        foreach ($varname as $i => $n) {
            $v = (is_array($value))
                ? ( isset($value[$i]) ? $value[$i] : NULL )
                : $value;
            $url = sgp($url, $n, $v, $clean);
        }
        return $url;
    }

    $urlinfo = parse_url($url);

    $get = (isset($urlinfo['query']))
        ? $urlinfo['query']
        : '';

    parse_str($get, $vars);

    if (!is_null($value))        // одновременно переписываем переменную
        $vars[$varname] = $value; // либо добавляем новую
    else
        unset($vars[$varname]); // убираем переменную совсем

    $new_get = http_build_query($vars);

    if ($clean)
        $new_get = preg_replace( // str_replace() выигрывает
            '/=(?=&|\z)/',     // в данном случае
            '',                // всего на 20%
            $new_get
        );

    $result_url =   (isset($urlinfo['scheme']) ? "$urlinfo[scheme]://" : '')
        . (isset($urlinfo['host']) ? "$urlinfo[host]" : '')
        . (isset($urlinfo['path']) ? "$urlinfo[path]" : '')
        . ( ($new_get) ? "?$new_get" : '')
        . (isset($urlinfo['fragment']) ? "#$urlinfo[fragment]" : '')
    ;
    return $result_url;
}


$arParams['WEB_FORM_ID'] = intval($FORM['ID']);
$arParams['RESULT_ID'] = intval($arParams['RESULT_ID']);
if (!$arParams['RESULT_ID']) $arParams['RESULT_ID'] = '';

$arParams['NAME_TEMPLATE'] = empty($arParams['NAME_TEMPLATE'])
    ? (method_exists('CSite', 'GetNameFormat') ? CSite::GetNameFormat() : "#NAME# #LAST_NAME#")
    : $arParams["NAME_TEMPLATE"];

if (!function_exists("__FormResultListCheckFilter")) {
    function __FormResultListCheckFilter(&$str_error, &$arrFORM_FILTER) // check of filter values
    {
        global $strError, $_GET;
        global $find_date_create_1, $find_date_create_2;
        $str = "";

        CheckFilterDates($find_date_create_1, $find_date_create_2, $date1_wrong, $date2_wrong, $date2_less);
        if ($date1_wrong == "Y") $str .= GetMessage("FORM_WRONG_DATE_CREATE_FROM") . "<br />";
        if ($date2_wrong == "Y") $str .= GetMessage("FORM_WRONG_DATE_CREATE_TO") . "<br />";
        if ($date2_less == "Y") $str .= GetMessage("FORM_FROM_TILL_DATE_CREATE") . "<br />";

        if (is_array($arrFORM_FILTER)) {
            reset($arrFORM_FILTER);
            foreach ($arrFORM_FILTER as $arrF) {
                if (is_array($arrF)) {
                    foreach ($arrF as $arr) {
                        $title = ($arr["TITLE_TYPE"] == "html") ? strip_tags(htmlspecialcharsback($arr["TITLE"])) : $arr["TITLE"];
                        if ($arr["FILTER_TYPE"] == "date") {
                            $date1 = $_GET["find_" . $arr["FID"] . "_1"];
                            $date2 = $_GET["find_" . $arr["FID"] . "_2"];

                            CheckFilterDates($date1, $date2, $date1_wrong, $date2_wrong, $date2_less);

                            if ($date1_wrong == "Y")
                                $str .= str_replace("#TITLE#", $title, GetMessage("FORM_WRONG_DATE1")) . "<br />";
                            if ($date2_wrong == "Y")
                                $str .= str_replace("#TITLE#", $title, GetMessage("FORM_WRONG_DATE2")) . "<br />";
                            if ($date2_less == "Y")
                                $str .= str_replace("#TITLE#", $title, GetMessage("FORM_DATE2_LESS")) . "<br />";
                        }
                        if ($arr["FILTER_TYPE"] == "integer") {
                            $int1 = intval($_GET["find_" . $arr["FID"] . "_1"]);
                            $int2 = intval($_GET["find_" . $arr["FID"] . "_2"]);
                            if ($int1 > 0 && $int2 > 0 && $int2 < $int1) {
                                $str .= str_replace("#TITLE#", $title, GetMessage("FORM_INT2_LESS")) . "<br />";
                            }
                        }
                    }
                }
            }
        }

        $strError .= $str;
        $str_error .= $str;

        return strlen($str) <= 0;
    }
}

if (CModule::IncludeModule("form")) {
    //  insert chain item
    if (strlen($arParams["CHAIN_ITEM_TEXT"]) > 0) {
        $APPLICATION->AddChainItem($arParams["CHAIN_ITEM_TEXT"], $arParams["CHAIN_ITEM_LINK"]);
    }

    // preparing additional parameters
    $arResult["FORM_ERROR"] = $_REQUEST["strError"];
    //$arResult["FORM_NOTE"] = $_REQUEST["strFormNote"];
    if (!empty($_REQUEST["formresult"]) && $_SERVER['REQUEST_METHOD'] != 'POST') {
        $formResult = strtoupper($_REQUEST['formresult']);
        switch ($formResult) {
            case 'ADDOK':
                $arResult['FORM_NOTE'] = str_replace("#RESULT_ID#", $arParams["RESULT_ID"], GetMessage('FORM_NOTE_ADDOK'));
                break;
            default:
                $arResult['FORM_NOTE'] = str_replace("#RESULT_ID#", $arParams["RESULT_ID"], GetMessage('FORM_NOTE_EDITOK'));
        }
    }

    $arParams["F_RIGHT"] = CForm::GetPermission($arParams["WEB_FORM_ID"]);

    if ($arParams["F_RIGHT"] < 15) $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

    $arParams["isStatisticIncluded"] = CModule::IncludeModule("statistic");

    if (is_array($arParams["NOT_SHOW_FILTER"])) {
        $arParams["arrNOT_SHOW_FILTER"] = $arParams["NOT_SHOW_FILTER"];
    } else {
        $arParams["arrNOT_SHOW_FILTER"] = explode(",", $arParams["NOT_SHOW_FILTER"]);
    }

    if (is_array($arParams["arrNOT_SHOW_FILTER"])) //array_walk($arParams["arrNOT_SHOW_FILTER"], create_function("&\$item", "\$item=trim(\$item);"));
        TrimArr($arParams["arrNOT_SHOW_FILTER"]);

    else $arParams["arrNOT_SHOW_FILTER"] = array();

    if (is_array($arParams["NOT_SHOW_TABLE"])) {
        $arParams["arrNOT_SHOW_TABLE"] = $arParams["NOT_SHOW_TABLE"];
    } else {
        $arParams["arrNOT_SHOW_TABLE"] = explode(",", $arParams["NOT_SHOW_TABLE"]);
    }
    if (is_array($arParams["arrNOT_SHOW_TABLE"])) //array_walk($arParams["arrNOT_SHOW_TABLE"], create_function("&\$item", "\$item=trim(\$item);"));
        TrimArr($arParams["arrNOT_SHOW_TABLE"]);

    else $arParams["arrNOT_SHOW_TABLE"] = array();

    // deleting single form result
    $del_id = intval($_REQUEST["del_id"]);

    if ($del_id > 0 /* && check_bitrix_sessid()*/) {
        $GLOBALS['strError'] = '';
        CFormResult::Delete($del_id);

        if (strlen($GLOBALS['strError']) <= 0) {
            LocalRedirect($APPLICATION->GetCurPageParam("", array("del_id", "sessid", 'formresult'), false));
            exit();
        }
    }

    // deleting single form result
    $del_ids = $_GET["del_ids"];

    if (count($del_ids) > 0 && check_bitrix_sessid()) {
        foreach ($del_ids as $del_id) {
            CFormResult::Delete($del_id);
        }
    }

    // deleting multiple form results
    if ($_REQUEST["delete"] && check_bitrix_sessid()) {
        $ARR_RESULT = $_REQUEST["ARR_RESULT"];
        if (is_array($ARR_RESULT) && count($ARR_RESULT) > 0 && check_bitrix_sessid()) {
            $GLOBALS['strError'] = '';
            foreach ($ARR_RESULT as $del_id) {
                $del_id = intval($del_id);
                if ($del_id > 0) CFormResult::Delete($del_id); // rights check inside
            }

            if (strlen($GLOBALS['strError']) <= 0) {
                LocalRedirect($APPLICATION->GetCurPageParam("", array("delete", "sessid", 'formresult')));
                exit();
            }
        }
    }

    if (strlen($GLOBALS['strError']) > 0)
        $arResult["FORM_ERROR"] .= $GLOBALS['strError'];

    if (intval($arParams["WEB_FORM_ID"]) > 0)
        $dbres = CForm::GetByID($arParams["WEB_FORM_ID"]);
    else
        $dbres = CForm::GetBySID($arParams["WEB_FORM_NAME"]);

    // get form info
    if ($arParams["arFormInfo"] = $dbres->Fetch()) {
        $GLOBALS["WEB_FORM_ID"] = $arParams["WEB_FORM_ID"] = $arParams["arFormInfo"]["ID"];
        $GLOBALS["WEB_FORM_NAME"] = $arParams["WEB_FORM_NAME"] = $arParams["arFormInfo"]["SID"];

        // check form params
        $arParams["USER_ID"] = $USER->GetID();

        // prepare filter
        $FilterArr = Array(
            "find_id",
            "find_id_exact_match",
            "find_status",
            "find_status_id",
            "find_status_id_exact_match",
            "find_timestamp_1",
            "find_timestamp_2",
            "find_date_create_2",
            "find_date_create_1",
            "find_date_create_2",
            "find_registered",
            "find_user_auth",
            "find_user_id",
            "find_user_id_exact_match",
            "find_guest_id",
            "find_guest_id_exact_match",
            "find_session_id",
            "find_session_id_exact_match"
        );

        $arResult["arrFORM_FILTER"] = array();

        $arListFilter = array("ACTIVE" => "Y");
        if (count($arParams["arrNOT_SHOW_FILTER"]) > 0) {
            $arListFilter["FIELD_SID"] = "~'" . implode("' & ~'", $arParams["arrNOT_SHOW_FILTER"]) . "'";
        }

        $z = CFormField::GetFilterList($arParams["WEB_FORM_ID"], $arListFilter);
        while ($zr = $z->Fetch()) {
            $FID = $arParams["WEB_FORM_NAME"] . "_" . $zr["SID"] . "_" . $zr["PARAMETER_NAME"] . "_" . $zr["FILTER_TYPE"];
            $zr["FID"] = $FID;
            if (!is_set($arResult["arrFORM_FILTER"][$zr["SID"]])) $arResult["arrFORM_FILTER"][$zr["SID"]] = array();
            $arResult["arrFORM_FILTER"][$zr["SID"]][] = $zr;
            $fname = "find_" . $FID;

            if ($zr["FILTER_TYPE"] == "date" || $zr["FILTER_TYPE"] == "integer") {
                $FilterArr[] = $fname . "_1";
                $FilterArr[] = $fname . "_2";
                $FilterArr[] = $fname . "_0";
            } elseif ($zr["FILTER_TYPE"] == "text") {
                $FilterArr[] = $fname;
                $FilterArr[] = $fname . "_exact_match";
            } else $FilterArr[] = $fname;
        }

        //fix minor bug with CFormField::GetFilterList and filter list logic - without it "exist" checkbox will be before main search field for date fields in filter
        foreach ($arResult["arrFORM_FILTER"] as $q_sid => $arFilterFields) {
            $cntFF = count($arFilterFields);
            if (is_array($arFilterFields) && $cntFF > 0) {
                $change = false;
                for ($i = 0; $i < $cntFF; $i++) {
                    if ($arFilterFields[$i]["FILTER_TYPE"] == "date") {
                        $tmp = $arFilterFields[$i];
                        $arFilterFields[$i] = $arFilterFields[$i - 1];
                        $arFilterFields[$i - 1] = $tmp;
                        $change = true;
                    }
                }

                if ($change) $arResult["arrFORM_FILTER"][$q_sid] = $arFilterFields;
            }
        }

        $arParams["sess_filter"] = "FORM_RESULT_LIST_" . $arParams["WEB_FORM_NAME"];
        if (strlen($_REQUEST["set_filter"]) > 0)
            InitFilterEx($FilterArr, $arParams["sess_filter"], "set");
        else
            InitFilterEx($FilterArr, $arParams["sess_filter"], "get");

        if (strlen($_REQUEST["del_filter"]) > 0) {
            DelFilterEx($FilterArr, $arParams["sess_filter"]);
        } else {
            InitBVar($GLOBALS["find_id_exact_match"]);
            InitBVar($GLOBALS["find_status_id_exact_match"]);
            InitBVar($GLOBALS["find_user_id_exact_match"]);
            InitBVar($GLOBALS["find_guest_id_exact_match"]);
            InitBVar($GLOBALS["find_session_id_exact_match"]);

            $arResult["ERROR_MESSAGE"] = "";
            if (__FormResultListCheckFilter($arResult["ERROR_MESSAGE"], $arResult["arrFORM_FILTER"])) {
                $arFilter = Array(
                    "ID" => $GLOBALS["find_id"],
                    "ID_EXACT_MATCH" => $GLOBALS["find_id_exact_match"],
                    "STATUS_ID" => $GLOBALS["find_status_id"],
                    "STATUS_ID_EXACT_MATCH" => $GLOBALS["find_status_id_exact_match"],
                    "TIMESTAMP_1" => $GLOBALS["find_timestamp_1"],
                    "TIMESTAMP_2" => $GLOBALS["find_timestamp_2"],
                    "DATE_CREATE_1" => $GLOBALS["find_date_create_1"],
                    "DATE_CREATE_2" => $GLOBALS["find_date_create_2"],
                    "REGISTERED" => $GLOBALS["find_registered"],
                    "USER_AUTH" => $GLOBALS["find_user_auth"],
                    "USER_ID" => CSite::InGroup(array(1)) ? $GLOBALS["find_user_id"] : $USER->GetID(),
                    "USER_ID_EXACT_MATCH" => $GLOBALS["find_user_id_exact_match"],
                    "GUEST_ID" => $GLOBALS["find_guest_id"],
                    "GUEST_ID_EXACT_MATCH" => $GLOBALS["find_guest_id_exact_match"],
                    "SESSION_ID" => $GLOBALS["find_session_id"],
                    "SESSION_ID_EXACT_MATCH" => $GLOBALS["find_session_id_exact_match"]
                );
                if (is_array($arResult["arrFORM_FILTER"])) {
                    foreach ($arResult["arrFORM_FILTER"] as $arrF) {
                        foreach ($arrF as $arr) {
                            if ($arr["FILTER_TYPE"] == "date" || $arr["FILTER_TYPE"] == "integer") {
                                $arFilter[$arr["FID"] . "_1"] = $GLOBALS["find_" . $arr["FID"] . "_1"];
                                $arFilter[$arr["FID"] . "_2"] = $GLOBALS["find_" . $arr["FID"] . "_2"];
                                $arFilter[$arr["FID"] . "_0"] = $GLOBALS["find_" . $arr["FID"] . "_0"];
                            } elseif ($arr["FILTER_TYPE"] == "text") {
                                $arFilter[$arr["FID"]] = $GLOBALS["find_" . $arr["FID"]];
                                $exact_match = ($GLOBALS["find_" . $arr["FID"] . "_exact_match"] == "Y") ? "Y" : "N";
                                $arFilter[$arr["FID"] . "_exact_match"] = $exact_match;
                            } else $arFilter[$arr["FID"]] = $GLOBALS["find_" . $arr["FID"]];
                        }
                    }
                }
                if (is_array($arResult["arrFORM_FILTER"])) {
                    foreach ($arResult["arrFORM_FILTER"] as $arrF) {
                        foreach ($arrF as $arr) {
                            if ($arr["FILTER_TYPE"] == "date" || $arr["FILTER_TYPE"] == "integer") {
                                $arFilter[$arr["FID"] . "_1"] = $GLOBALS["find_" . $arr["FID"] . "_1"];
                                $arFilter[$arr["FID"] . "_2"] = $GLOBALS["find_" . $arr["FID"] . "_2"];
                                $arFilter[$arr["FID"] . "_0"] = $GLOBALS["find_" . $arr["FID"] . "_0"];
                            } elseif ($arr["FILTER_TYPE"] == "text") {
                                $arFilter[$arr["FID"]] = $GLOBALS["find_" . $arr["FID"]];
                                $exact_match = ($GLOBALS["find_" . $arr["FID"] . "_exact_match"] == "Y") ? "Y" : "N";
                                $arFilter[$arr["FID"] . "_exact_match"] = $exact_match;
                            } else $arFilter[$arr["FID"]] = $GLOBALS["find_" . $arr["FID"]];
                        }
                    }
                }

                if(isset($arParams['DEAL_ID'])){
                    $arFields[] = array(
                        "SID"              => "DEAL_BILLING_DEAL_ID",      // код поля по которому фильтруем
                        "VALUE"             => $arParams['DEAL_ID'],
                    );

                    $arFilter["FIELDS"] = $arFields;
                }
            }
        }

        $arFilter["FIELDS"] = $arFields;

        if (strlen($_POST['save']) > 0 && $_SERVER['REQUEST_METHOD'] == "POST" && check_bitrix_sessid()) {
            // update results
            if (isset($_POST["RESULT_ID"]) && is_array($_POST["RESULT_ID"])) {
                $RESULT_ID = $_POST["RESULT_ID"];
                foreach ($RESULT_ID as $rid) {
                    $rid = intval($rid);
                    $var_STATUS_PREV = "STATUS_PREV_" . $rid;
                    $var_STATUS = "STATUS_" . $rid;
                    if (intval($_REQUEST[$var_STATUS]) > 0 && $_REQUEST[$var_STATUS_PREV] != $_REQUEST[$var_STATUS]) {
                        CFormResult::SetStatus($rid, $_REQUEST[$var_STATUS]); // rights and status check inside
                    }
                }
            }
        }
        if ($_POST['EDIT'] && $_SERVER['REQUEST_METHOD'] == "POST" && check_bitrix_sessid()) {// check errors
            $arResult["FORM_ERRORS"] = CForm::Check($arParams["WEB_FORM_ID"], $arResult["arrVALUES"], $arParams["RESULT_ID"], "Y", $arParams['USE_EXTENDED_ERRORS']);
            $APPLICATION->RestartBuffer();
            foreach ($_POST['EDIT'] as $id => $data) {
                $arResult["FORM_ERRORS"] = CForm::Check($arParams["WEB_FORM_ID"], $data, $id, "Y", $arParams['USE_EXTENDED_ERRORS']);

                if (
                    $arParams['USE_EXTENDED_ERRORS'] == 'Y' && (!is_array($arResult["FORM_ERRORS"]) || count($arResult["FORM_ERRORS"]) <= 0)
                    ||
                    $arParams['USE_EXTENDED_ERRORS'] != 'Y' && strlen($arResult["FORM_ERRORS"]) <= 0
                ) {
                    echo CFormResult::Update($id, $data, "Y");
                }

            }
            exit();
        }

        if ($_POST['CHANGE_STATUS_IDS'] && $_POST['CHANGE_STATUS'] && $_SERVER['REQUEST_METHOD'] == "POST" && check_bitrix_sessid()) {
            foreach ($_POST['CHANGE_STATUS_IDS'] as $rid) {
                CFormResult::SetStatus($rid, $_REQUEST['CHANGE_STATUS']); // rights and status check inside
            }
            exit();
        }

        // get results list
        $arParams["by"] = $_REQUEST["by"];
        $arParams["order"] = $_REQUEST["order"];
        $arResult["is_filtered"] = false;
        $arResult["arRID"] = array();
        $arResults = array();



        $rsResults = CFormResult::GetList($arParams["WEB_FORM_ID"], $arParams["by"], $arParams["order"], $arFilter, $arResult["is_filtered"]);

        $arResult["res_counter"] = 0;
        $arParams["can_delete_some"] = false;

        while ($arR = $rsResults->Fetch()) {


            $arResult["res_counter"]++;
            $arResults[] = $arR;
            $arResult["arRID"][] = $arR["ID"]; // array of IDs of all results

            if (!$arParams["can_delete_some"]) {
                if ($arParams["F_RIGHT"] >= 20 || ($arParams["F_RIGHT"] >= 15 && $arParams["USER_ID"] == $arR["USER_ID"])) {
                    $arrRESULT_PERMISSION = CFormResult::GetPermissions($arR["ID"], $v);
                    if (in_array("DELETE", $arrRESULT_PERMISSION)) $arParams["can_delete_some"] = true;
                }
            }
        }


        $rsResults = new CDBResult;
        $rsResults->InitFromArray($arResults);

        $page_split = 100;
        //$rsResults->NavStart($page_split);
        $arResult["pager"] = $rsResults->GetNavPrint(GetMessage("FORM_PAGES"), false, 'text', false, array('formresult', 'RESULT_ID'));

        if (!$rsResults->NavShowAll) {
            $pagen_from = (intval($rsResults->NavPageNomer) - 1) * intval($rsResults->NavPageSize);
            $arRID_tmp = array();
            if (is_array($arResult["arRID"]) && count($arResult["arRID"]) > 0) {
                $i = 0;
                foreach ($arResult["arRID"] as $rid) {
                    if ($i >= $pagen_from && $i < $pagen_from + $page_split) {
                        $arRID_tmp[] = $rid; // array of IDs of results for the page
                    }
                    $i++;
                }
            }
            $arResult["arRID"] = $arRID_tmp;
        }

        $arResult["arrResults"] = array();
        $arrUsers = array();
        while ($arRes = $rsResults->NavNext(false)) {
            $arRes["arrRESULT_PERMISSION"] = CFormResult::GetPermissions($arRes["ID"], $v);

            $arRes["can_view"] = false;
            $arRes["can_edit"] = false;
            $arRes["can_delete"] = false;

            if ($arParams["F_RIGHT"] >= 20 || ($arParams["F_RIGHT"] >= 15 && $arParams["USER_ID"] == $arRes["USER_ID"])) {
                if (in_array("VIEW", $arRes["arrRESULT_PERMISSION"])) $arRes["can_view"] = true;
                if (in_array("EDIT", $arRes["arrRESULT_PERMISSION"])) $arRes["can_edit"] = true;
                if (in_array("DELETE", $arRes["arrRESULT_PERMISSION"])) $arRes["can_delete"] = true;
            }

            $arr = explode(" ", $arRes["TIMESTAMP_X"]);
            $arRes["TSX_0"] = $arr[0];
            $arRes["TSX_1"] = $arr[1];

            if ($arRes["USER_ID"] > 0) {
                if (!in_array($arRes["USER_ID"], array_keys($arrUsers))) {
                    $rsU = CUser::GetByID($arRes["USER_ID"]);
                    $arU = $rsU->Fetch();
                    $arRes["LOGIN"] = $arU["LOGIN"];
                    $arRes["USER_FIRST_NAME"] = $arU["NAME"];
                    $arRes["USER_LAST_NAME"] = $arU["LAST_NAME"];
                    $arRes["USER_SECOND_NAME"] = $arU["SECOND_NAME"];
                    $arrUsers[$arRes["USER_ID"]]["USER_FIRST_NAME"] = $arRes["USER_FIRST_NAME"];
                    $arrUsers[$arRes["USER_ID"]]["USER_LAST_NAME"] = $arRes["USER_LAST_NAME"];
                    $arrUsers[$arRes["USER_ID"]]["USER_SECOND_NAME"] = $arRes["USER_SECOND_NAME"];
                    $arrUsers[$arRes["USER_ID"]]["LOGIN"] = $arRes["LOGIN"];
                } else {
                    $arRes["USER_FIRST_NAME"] = $arrUsers[$arRes["USER_ID"]]["USER_FIRST_NAME"];
                    $arRes["USER_LAST_NAME"] = $arrUsers[$arRes["USER_ID"]]["USER_LAST_NAME"];
                    $arRes["USER_SECOND_NAME"] = $arrUsers[$arRes["USER_ID"]]["USER_SECOND_NAME"];
                    $arRes["LOGIN"] = $arrUsers[$arRes["USER_ID"]]["LOGIN"];
                }
            }

            $arResult["arrResults"][] = $arRes;
        }

        // get columns titles
        if ($arResult["res_counter"] > 0) {
            $arFilter = array(
                // "IN_RESULTS_TABLE" => "Y",
                "RESULT_ID" => implode(" | ", $arResult["arRID"])
            );
            CForm::GetResultAnswerArray($arParams["WEB_FORM_ID"], $arResult["arrColumns"], $arResult["arrAnswers"], $arResult["arrAnswersSID"], $arFilter);
        } else {
            $arFilter = array("IN_RESULTS_TABLE" => "Y");

            $rsFields = CFormField::GetList($arParams["WEB_FORM_ID"], "ALL", ($v1 = "s_c_sort"), ($v2 = "asc"), $arFilter, $v3);

            while ($arField = $rsFields->Fetch()) {
                $arResult["arrColumns"][$arField["ID"]] = $arField;
            }
        }


        //$arResult = array();

        //$final = array();

        if (count($arResult["arrResults"]) > 0) {

            $result = 0;      // Обнуляем общий результат для вывода в неделю

            $arResult["arrResults"] = array_reverse($arResult["arrResults"]);
            foreach ($arResult["arrResults"] as $arRes){
                $expense = 0;     // Обнуляем расход для вывода в неделю
                $incoming = 0;    // Обнуляем приход для вывода в неделю



                // Получаем первый день недели для вывода

                $test = strtotime($arResult["arrAnswers"][$arRes["ID"]][164][164]['USER_TEXT']);
                $week_number = date("W", $test);
                $year = date("Y", $test);
                $first_day = date('d/m/Y', $week_number * 7 * 86400 + strtotime('1/1/' . $year) - date('w', strtotime('1/1/' . $year)) * 86400 + 86400);

                // Получаем данные о сделке, к которой принадлежит событие

                $deals = CCrmDeal::GetByID($arResult["arrAnswers"][$arRes["ID"]][158][0]["USER_TEXT"]);


                if (CModule::IncludeModule("highloadblock")) {
                    $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getList(['filter' => ['NAME' => 'PaymentTypeCommission']])->fetch();
                    $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
                    $entity_data_class = $entity->getDataClass();
                    $aResult1 = $entity_data_class::getList(['filter' => ['UF_PAYMENT_IDN' => $arResult["arrAnswers"][$arRes["ID"]][163][163]["USER_TEXT"]]])->fetch();
                }
                if($aResult1['UF_PAYMENT_IDN']=='IN'){
                    $sub_incoming = $arResult["arrAnswers"][$arRes["ID"]][162][162]['USER_TEXT'];
                    $sub_expense = 0;

                    $result = $result+$arResult["arrAnswers"][$arRes["ID"]][162][162]['USER_TEXT'];
                }else{
                    $sub_incoming = 0;
                    $sub_expense = $arResult["arrAnswers"][$arRes["ID"]][162][162]['USER_TEXT'];
                    $result = $result-$arResult["arrAnswers"][$arRes["ID"]][162][162]['USER_TEXT'];
                }

                $element = array(
                    'ID'            => $arRes['ID'],                                                  // ID события
                    'DEAL_ID'       => $deals['ID'],                                                  // ID сделки
                    'DEAL_TITLE'    => $deals['TITLE'],                                               // Название сделки
                    'USE'           => $arResult["arrAnswers"][$arRes["ID"]][161][161]['USER_TEXT'],  // Назначение
                    'INCOMING'      => $sub_incoming,                                                 // Приход
                    'EXPENSE'       => $sub_expense,                                                  // Расход
                    'RESULT'        => $result,                                                       // Итог

                );



                if(!in_array($deals['COMPANY_ID'], $clArr)){
                    $cl = CCrmCompany::GetByID($deals['COMPANY_ID']);
                    $clients[] = array(
                        'ID' => $cl['ID'],
                        'TITLE' => $cl['TITLE']
                    );
                    $clArr[]=$deals['COMPANY_ID'];
                }


               // $usl = true;
                if($_GET['startdate'] || $_GET['enddate'] || $_GET['client'] || $_GET['user1'] || $_GET['debt']){
                    // проверка по дате
                    $date = str_replace('/', '-', $first_day);
                    if(strlen($_GET['enddate'])!=10 && strlen($_GET['startdate'])!=10 || (strlen($_GET['enddate'])!=10 && strlen($_GET['startdate'])==10 && strtotime($_GET['startdate'])<strtotime($date)) || (strlen($_GET['enddate'])==10 && strlen($_GET['startdate'])==10 && strtotime($_GET['startdate'])<strtotime($date) && strtotime($_GET['enddate'])>strtotime($date)) || (strlen($_GET['enddate'])==10 && strlen($_GET['startdate'])!=10 && strtotime($_GET['enddate'])>strtotime($date))){
                        $usl = true;
                    }else{
                        $usl = false;
                    }

                    if(intval($_GET['client'])>1){
                        if((intval($_GET['client'])==intval($deals['COMPANY_ID'])) && $usl){
                            $usl = true;
                        }else{
                            $usl = false;
                        }

                    }
                    if(intval($_GET['user1'])>1){
                        if((intval($_GET['user1'])==intval($deals['ASSIGNED_BY'])) && $usl){
                            $usl = true;
                        }else{
                            $usl = false;
                        }

                    }


                    if(strlen($_GET['debt'])>1){
                        //echo $deals['OPPORTUNITY'] - $summInAll.'|';
                        if($_GET['debt']=='on' && (intval($deals['OPPORTUNITY']) - intval($summInAll))>0 && $usl){
                            $usl = true;
                        }else{
                            $usl = false;
                        }
                    }

                    if(!$_GET['startdate'] && !$_GET['enddate'] && !$_GET['user1'] && !$_GET['client'] && !$_GET['debt']){
                        $usl = true;
                    }
                    if($usl){
                        $incoming = $incoming + $sub_incoming;
                        $expense = $expense + $sub_expense;
                        $all_filter[] = $element['ID'];
                        $final['ITEMS'][$first_day][] = $element;
                        $final['ITEMS'][$first_day]['INCOMING'] =  intval($final['ITEMS'][$first_day]['INCOMING']) + intval($sub_incoming);
                        $final['ITEMS'][$first_day]['EXPENSE'] = intval($final['ITEMS'][$first_day]['EXPENSE']) + intval($sub_expense);
                        $final['ITEMS'][$first_day]['RESULT'] = $result;
                    }
                }else{

                    $incoming = $incoming + $sub_incoming;
                    $expense = $expense + $sub_expense;
                    $all_filter[] = $element['ID'];
                    $final['ITEMS'][$first_day][] = $element;
                    $final['ITEMS'][$first_day]['INCOMING'] =  intval($final['ITEMS'][$first_day]['INCOMING']) + intval($sub_incoming);
                    $final['ITEMS'][$first_day]['EXPENSE'] = intval($final['ITEMS'][$first_day]['EXPENSE']) + intval($sub_expense);
                    $final['ITEMS'][$first_day]['RESULT'] = $result;
                }
                $FINAL['INCOMING'] = $FINAL['INCOMING'] + $incoming;
                $FINAL['EXPENSE'] = $FINAL['EXPENSE'] + $expense;
                $FINAL['FINAL'] = $FINAL['INCOMING'] - $FINAL['EXPENSE'];

            }
            $arResult['FINAL'] = array(
                'INCOMING'  => number_format($FINAL['INCOMING'], 2, '.', ' '),  // Общий Бюджет
                'EXPENSE'   => number_format($FINAL['EXPENSE'], 2, '.', ' '),   // Общий Долг
                'FINAL'     => number_format($FINAL['FINAL'], 2, '.', ' '),     // Общий Расход
            );
        }


        // Получаем клиентов для фильтров
        $arFilterDeals = array(
            'COMPANY_ID' => implode(" | ", $clArr)
        );

        $dea = CCrmDeal::GetList($arOrder = Array('DATE_CREATE' => 'DESC'), $arFilterDeals, $arSelect = Array());
        while ($d = $dea->GetNext()) {

            $summInAll = 0;
            $summOutInAll = 0;
            $summOutOutAll = 0;
            if (count($arResult["arrResults"]) > 0) {
                foreach ($arResult["arrResults"] as $arRes) {
                    $sum = 0;
                    $isIn = false;
                    $isOut = false;
                    $isOutIn = false;
                    $arrColumns = [];
                    $arrAnswers = [];
                    $arrAnswersVarname = [];
                    CForm::GetResultAnswerArray($arParams['WEB_FORM_ID'],
                        $arrColumns,
                        $arrAnswers,
                        $arrAnswersVarname,
                        array("RESULT_ID" => $arRes["ID"]));

                    $ans = current($arrAnswersVarname);
                    if ($ans['DEAL_BILLING_OUT_TYPE'][0]['USER_TEXT'] == 'IN') {
                        $isOutIn = true;
                    }
                    foreach ($arResult["arrColumns"] as $FIELD_ID => $arrC) {

                        $arrAnswer = $arResult["arrAnswers"][$arRes["ID"]][$FIELD_ID];

                        if (is_array($arrAnswer)) {
                            foreach ($arrAnswer as $key => $arrA) {
                                if ($arrA['SID'] == 'DEAL_BILLING_TYPE' && $arrA['USER_TEXT'] == 'IN') {
                                    $isIn = true;
                                } elseif ($arrA['SID'] == 'DEAL_BILLING_TYPE' && $arrA['USER_TEXT'] != 'IN') {
                                    $isOut = true;
                                }

                                if ($arrA['SID'] == 'DEAL_BILLING_SUM') {
                                    if(array_search($arrA['RESULT_ID'], $all_filter)){
                                        $sum = intval($arrA['USER_TEXT']);
                                    }else{
                                        $sum = 0;
                                    }

                                }
                            } //foreach
                        }
                    } //foreach
                    if ($isIn) {
                        $summInAll += $sum;
                    }
                    if ($isOut) {
                        if ($isOutIn) {
                            $summOutInAll += $sum;
                        } else {

                                $summOutOutAll += $sum;

                        }
                    }
                }
            }
        }
        $arResult['FINAL']['OUT'] = number_format($summOutOutAll, 2, '.', ' ');  // Внешний Расход


        $arResult['ITEMS'] = $final;

        $arResult['CLIENTS']=$clients; // Массив клиентов

        if(!$_GET['EXPORT']) {
            $url = $_SERVER['REQUEST_URI'];

            $arResult['EXPORT_UPL'] = sgp($url, 'EXPORT', 'TRUE');
            $this->IncludeComponentTemplate();
        }else{

            // Fix::
            ob_end_clean();

            // Подключаем класс

            include_once dirname(__FILE__) . "/PHPExcel.php";
            include_once dirname(__FILE__) . '/PHPExcel/IOFactory.php';

            // Берем чистый файл
            $pExcel = PHPExcel_IOFactory::load(dirname(__FILE__) . "/test.xlsx");

            $pExcel->setActiveSheetIndex(0);
            $aSheet = $pExcel->getActiveSheet();

            $aSheet->setTitle('Медиафакт');

            $aSheet->setCellValue('A1','ID');
            $aSheet->setCellValue('B1','Сделка');
            $aSheet->setCellValue('C1','Назначение');
            $aSheet->setCellValue('D1','Приход');
            $aSheet->setCellValue('E1','Расход');
            $aSheet->setCellValue('F1','Остаток');
            $i = 1;
            foreach ($arResult['ITEMS'] as $key => $item){
                foreach ($item as $subkey => $subitem){
                    foreach ($subitem as $subsubkey => $subsubitem){
                        if($subsubitem['ID']){
                            $i++;
                            $aSheet->setCellValue('A'.$i, $subsubitem['ID']);
                            $aSheet->setCellValue('B'.$i, $subsubitem['DEAL_TITLE']);
                            $aSheet->setCellValue('C'.$i, $subsubitem['USE']);
                            $aSheet->setCellValue('D'.$i, $subsubitem['INCOMING']);
                            $aSheet->setCellValue('E'.$i, $subsubitem['EXPENSE']);
                            $aSheet->setCellValue('F'.$i, $subsubitem['RESULT']);
                        }
                    }
                }
            }

            $validLocale = PHPExcel_Settings::setLocale('ru');
            include(dirname(__FILE__) . "/PHPExcel/Writer/Excel5.php");
            $objWriter = new PHPExcel_Writer_Excel5($pExcel);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="mediafact.xls"');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
        }
    } else {
        echo ShowError(GetMessage("FORM_INCORRECT_FORM_ID"));
    }
} else {
    echo ShowError(GetMessage("FORM_MODULE_NOT_INSTALLED"));
}
?>