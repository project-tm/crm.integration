<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
//$this->setFrameMode(true);
//$APPLICATION->AddHeadScript("/bitrix/js/calendar/cal-core.js", false);
//$APPLICATION->AddHeadScript("/bitrix/js/calendar/cal-planner.js", false);
$APPLICATION->AddHeadString('<link href="' . SITE_TEMPLATE_PATH . '/css/main-style.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="' . $this->GetFolder() . '/css/style.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="/bitrix/js/crm/css/crm.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>');
$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.11/js/jquery.dataTables.min.js"></script>');

$APPLICATION->setTitle('Отчет приход-расход');
?>
<style>
    .row-fluid .span9 {
        width: 74.39024390243902%;
    }
    .dealings-summa .summa {
        float: left;
        color: #000;
        margin-right: 20px;
        margin-bottom: 10px;
        font-weight: bold;
    }
    .dealings-summa .summa .result {
        font-size: 19px;
        font-weight: normal;
    }
    .dealings-summa .summa .result.blue {
        color: #0088cc;
    }
    .dealings-summa .summa .result.red {
        color: #a6342f;
    }
    .dealings-summa .summa .result.yellow {
        color: #a9ac32;
    }
    .dealings-summa .summa .result.green {
        color: #59ad31;
    }
    .deal_billing_stat table thead tr th,
    .deal_billing_stat table tbody tr td
    {
        border-bottom: 1px solid #d4d4d4;
        line-height: 35px;
    }
    .deal_billing_stat table table {
        border-collapse: collapse;
    }
    .b-pagintaion__item{
        list-style: none;
        float: left;
        line-height: 14px;
        padding-right: 10px;
    }
    .paginator{
        float:right;
    }
    .paginator .active a{
        color: #08C!important;
    }
    .row-fluid .span3 {
        width: 23.170731707317074%;
    }
    .well {
        min-height: 20px;
        padding: 19px;
        margin-bottom: 20px;
        background-color: #f5f5f5;
        border: 1px solid #e3e3e3;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
    }
    .well .h, .popup .h {
        font-weight: bold;
        color: #999999;
        margin-bottom: 10px;
    }
    .withcaret {
        position: relative;
    }
    .row-fluid [class*="span"]:first-child {
        margin-left: 0;
    }
    .withcaret .clearfilter {
        position: absolute;
        right: -3px;
        top: 7px;
        opacity: 0.8;
        cursor: pointer;
    }
    .filter{
        float: left;
        width: 73%;
        margin-right: 5%;
        border-radius: 5px;
        background: rgb(238, 242, 244);
        padding-bottom: 10px;
        width: 669px;
    }
    .crm-search-inp{
        background: #fff;
        border: 1px solid #c6cdd3;
        border-radius: 2px;
        color: #313232;
        display: inline-block;
        font-size: 13px;
        height: 36px;
        margin: 0;
        outline: 0;
        vertical-align: middle;
        padding: 0 8px;
        padding-top: 0px;
        padding-right: 8px;
        padding-bottom: 0px;
        padding-left: 8px;
        width: 213px;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        margin-top: 9px;
    }
    p.des{
        color: #535C69;
        float: left;
        margin-bottom: 10px;
        padding-left: 10px;
        padding-top: 4px;
        width: 126px;
    }
    .line{
        float: left;
        width: 100%;
        padding: 4px;
        padding-left: 7px;
        padding-top: 0px;
    }
    select#soflow, select#soflow-color {
        background: transparent;
        border: 1px solid #c6cdd3;
        border-radius: 0;
        -webkit-box-shadow: none;
        box-shadow: none;
        color: #535c69;
        display: inline-block;
        font-size: 14px;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        line-height: 15px;
        height: 36px;
        text-shadow: 0 1px #fff;
        outline: 0;
        overflow: hidden;
        padding: 0 0 0 5px;
        min-width: 100px;
        vertical-align: top;
        -webkit-appearance: none;
        position: relative;
        width: 250px;
        z-index: 1;
    }
    .bx-select-wrap{
        background-color: #fff;
        border-radius: 2px;
        display: inline-block;
        vertical-align: top;
        position: relative;
        margin-top: 7px;
    }
    .bx-select-wrap:after {
        background: url("/bitrix/components/bitrix/crm.interface.filter/templates/flat/bitrix/main.interface.filter/new/images/filter/filter-sprite.png") no-repeat -9px -641px;
        content: "";
        height: 10px;
        margin-top: -5px;
        right: 8px;
        position: absolute;
        top: 50%;
        width: 5px;
        z-index: 0;
    }
    .bx-filter-bottom-separate {
        background-color: #d8dde0;
        height: 1px;
        margin: 0 18px 13px;
        float: left;
        width: 635px;
        margin-left: 15px;
    }
    .sub{
        background: transparent;
        border-radius: 2px;
        border: 1px solid #a1a6ac;
        box-shadow: none;
        color: #535c69;
        cursor: pointer;
        display: inline-block;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-weight: bold;
        font-size: 12px;
        height: 40px;
        line-height: 38px;
        margin-left: 12px;
        min-width: 55px;
        text-transform: uppercase;
        text-decoration: none;
        padding: 0 15px;
        position: relative;
        vertical-align: middle;
        -webkit-font-smoothing: antialiased;
        -webkit-transition: all .25s linear;
        transition: all .25s linear;
    }
    .sub:hover{
        text-decoration: none;
        background: #cfd4d8!important;
    }
    .acc{
        width:100%;
        margin-top: 50px;
        float: left;
        border-top: 1px solid #ccc;
    }
    .acc .acc__button{
        float:left;
        width:100%;
        background: #f3f3f3;
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
    }
    .acc .acc__content{
        display:none;
    }
    .acc__button div{
        float:left;
        line-height: 30px;
        color: #636363;
    }
    .acc__week{
        width:67%;
        padding-left:3%;
    }
    .acc__inc{
        width:10%;
    }
    .acc__exp{
        width:10%;
    }
    .acc__res{
        width:10%;
    }
    .acc .acc__week,
    .acc .acc__inc,
    .acc .acc__exp,
    .acc .acc__res
    {
        line-height: 30px;
        float:left;
    }
    table tr td{
        border-top: 1px solid #dddddd;
    }
</style>

<div class="filter">
<form action="" type="get">
    <div class="line">
        <p class="des">По дате:</p>
        <input class="crm-search-inp" name='startdate' value="<?= $_GET['startdate'] ?>" type="date">
        <span style="padding-left: 8px; padding-right: 13px;">...</span><input class="crm-search-inp" name='enddate' value="<?= $_GET['enddate'] ?>" type="date">
    </div>
    <div class="line">
        <p class="des">Клиент:</p>
			<span class="bx-select-wrap">
				<select name="client" id="soflow" value="<?= $_GET['client'] ?>">
                    <option value="">Клиент</option>
                    <? foreach ($arResult['CLIENTS'] as $client){ $i++; ?>
                        <option <?= $client['ID']==$_GET['client'] ? 'selected' : '' ?> value="<?=$client['ID']?>"><?=$client['TITLE']?></option>
                    <? } ?>
                </select>
			</span>
    </div>
    <div class="line">
        <p class="des">Менеджер:</p>
			<span class="bx-select-wrap">
				<select name="user1" id="soflow">
                    <option value="">Менеджер</option>
                    <? foreach ($arResult['USERS'] as $user){ $i++; ?>
                        <option <?= $user['ID']==$_GET['user1'] ? 'selected' : '' ?> value="<?=$user['ID']?>"><?=$user['NAME']?></option>
                    <? } ?>
                </select>
			</span>
    </div>
    <div class="line">
        <p class="des">С долгом клиента</p>
        <input type="checkbox" style="width: 28px; height: 49px;" name="debt" <?= $_GET['debt'] ? 'checked' : '' ?>>
    </div>
    <div class="bx-filter-bottom-separate" style=""></div>
    <input type="submit" class="sub">
</form>
</div>
<div class="dealings-summa">
    <div class="summa"> Приход: <br> <span class="result ng-binding"><?=$arResult['FINAL']['INCOMING']?></span> </div>
    <div class="summa"> Расход: <br> <span class="result red ng-binding"><?=$arResult['FINAL']['EXPENSE']?></span> </div>
    <div class="summa"> Внешний расход: <br> <span class="result red ng-binding"><?=$arResult['FINAL']['OUT']?></span> </div>
    <div class="summa"> Прибыль: <br> <span class="result green ng-binding"><?=$arResult['FINAL']['FINAL']?></span> </div>
    <a href="<?=$arResult['EXPORT_UPL']?>">Выгрузить данные</a>
</div>
<div id="deal_billing"> </div>
    <div id="chart_div" style="float:left;     width: 100%;
    height: 500px;"></div>
	<div class="acc" id="accordion">
        <div class="acc__week">Неделя</div>
        <div class="acc__inc">Приход</div>
        <div class="acc__exp">Расход</div>
        <div class="acc__res">Остаток</div>
        <?  ?>
        <?foreach ($arResult['ITEMS'] as $key => $item){?>

            <?foreach ($item as $subkey => $subitem){?>
                <a href="#" class="acc__button">
                    <div class="acc__week"><?=$subkey?></div>
                    <div class="acc__inc"><?=$subitem['INCOMING']?></div>
                    <div class="acc__exp"><?=$subitem['EXPENSE']?></div>
                    <div class="acc__res"><?=$subitem['RESULT']?></div>
                </a>
                <div class="acc__content">
                    <table style="width:100%;">
                        <thead>
                        <tr>
                            <th style="width: 5%;">ID</th>
                            <th style="width: 30%;">Сделка</th>
                            <th style="width: 35%;">Назначение</th>
                            <th style="width: 10%;"></th>
                            <th style="width: 10%;"></th>
                            <th style="width: 10%;"></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?foreach ($subitem as $subsubkey => $subsubitem){
                                if($subsubitem['ID']){?>
                                    <tr style="line-height:30px;">
                                        <td><?=$subsubitem['ID']?></td>
                                        <td><a href="/crm/deal/show/<?=$subsubitem['DEAL_ID']?>/"><?=$subsubitem['DEAL_TITLE']?></a></td>
                                        <td><?=$subsubitem['USE']?></td>
                                        <td><?=$subsubitem['INCOMING']?></td>
                                        <td><?=$subsubitem['EXPENSE']?></td>
                                        <td><?=$subsubitem['RESULT']?></td>
                                    </tr>
                            <?}}?>
                        </tbody>
                    </table>
                </div>
            <?}?>
        <?}?>
	</div>

<?
    foreach ($arResult['ITEMS'] as $key => $item) {
        foreach ($item as $subkey => $subitem) {
            $arrD[]['DATE']=$subkey;
            $arr[] = array(
                'DATE'      => $subkey,
                'INCOMING'  => $subitem['INCOMING'],
                'EXPENSE'   => $subitem['EXPENSE'],
                'RESULT'    => $subitem['RESULT'],
            );
        }
    }

function sorta($a, $b) {
    if ($a['DATE'] > $b['DATE'])
        return 1;
}

usort($arr, 'sorta');

?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>
	$(document).ready(function(){
		$('#accordion a').click(function(e) {
			$(this).next().slideToggle();
			return false;
		});
	});
    </script>
 <?if(count($arr)){?>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart'], 'language': 'ru'});

    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Дата', 'Приход', 'Расход', 'Остаток'],
            <?foreach ($arr as $item) {?>
                [<?
                $access_date = $item['DATE'];

// функция  explode()разбивает  строку другой строкой. В данном случае
// $access_date разбит на символе /

$date_elements  = explode("/",$access_date);

// здесь
// $date_elements[0] = 29
// $date_elements[1] = 5
// $date_elements[2] = 2016
echo 'new Date('.$date_elements[2].','.($date_elements[1]-1).','.$date_elements[0].')';?>, <?=$item['INCOMING']?>, <?=$item['EXPENSE']?>, <?=$item['RESULT']?>],
            <?}?>
        ]);

        var options = {
            title: '',
            curveType: 'function',
            legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

        chart.draw(data, options);
    }
</script>
<?}else{?>
    <br>
<h2 style="color:#a6342f">Данные отсутствуют!</h2>
<?}?>