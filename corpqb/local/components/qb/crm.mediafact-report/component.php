<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$isAdmin = false;
$moreColumnCount = 2;

// Функция для изменения значений гет-параметров в урле, для сохранения фильтров в выгрузке

function sgp(
    $url,
    $varname,
    $value = NULL, // если NULL - убираем переменную совсем
    $clean = TRUE // превращать ли ?one=&two=
) {              // в ?one&two (так адрес красивее)

    // Версия функции "substitue get parameter" без регулярных выражений

    if (is_array($varname)) {
        foreach ($varname as $i => $n) {
            $v = (is_array($value))
                ? ( isset($value[$i]) ? $value[$i] : NULL )
                : $value;
            $url = sgp($url, $n, $v, $clean);
        }
        return $url;
    }

    $urlinfo = parse_url($url);

    $get = (isset($urlinfo['query']))
        ? $urlinfo['query']
        : '';

    parse_str($get, $vars);

    if (!is_null($value))        // одновременно переписываем переменную
        $vars[$varname] = $value; // либо добавляем новую
    else
        unset($vars[$varname]); // убираем переменную совсем

    $new_get = http_build_query($vars);

    if ($clean)
        $new_get = preg_replace( // str_replace() выигрывает
            '/=(?=&|\z)/',     // в данном случае
            '',                // всего на 20%
            $new_get
        );

    $result_url =   (isset($urlinfo['scheme']) ? "$urlinfo[scheme]://" : '')
        . (isset($urlinfo['host']) ? "$urlinfo[host]" : '')
        . (isset($urlinfo['path']) ? "$urlinfo[path]" : '')
        . ( ($new_get) ? "?$new_get" : '')
        . (isset($urlinfo['fragment']) ? "#$urlinfo[fragment]" : '')
    ;
    return $result_url;
}

if (CSite::InGroup(array(1))) {
	$moreColumnCount += 1;
	$isAdmin = true;
}

// Получаем сотрудников для фильтров

$rsUsers = CUser::GetList(
    $order,
    $tmp,
    $filter,
    array("SELECT"=>array('ID', 'NAME', 'LAST_NAME'))
);
while ($state = $rsUsers->GetNext()) {
    $arResult['USERS'][] = array(
        'ID'   => $state['ID'],
        'NAME' => $state['NAME'] . ' '. $state['LAST_NAME']
    );
};



$d = CCrmDeal::GetList();
while ($c = $d->GetNext()) {
    if (!in_array($c['COMPANY_ID'], $clArr, true)) {
        $cl = CCrmCompany::GetByID($c['COMPANY_ID']);
            if($cl['ID'] && $cl['TITLE']){
                $clients[] = array(
                    'ID' => $cl['ID'],
                    'TITLE' => $cl['TITLE']
                );
            }
    }
    $clArr[] = $c['COMPANY_ID'];
}
$arResult['CLIENTS']=$clients; // Массив клиентов

// Устанвливаем фильтры, полученные из get-параметров

//По клиенту
if($_GET['client']){
    $arFilterDeals["COMPANY_ID"] = $_GET['client'];
}
// По продажнику
if($_GET['user1']){
    $arFilterDeals["UF_CRM_DEAL_SELLER"] = $_GET['user1'];
}
// По ответственному
if($_GET['user2']){
    $arFilterDeals["ASSIGNED_BY_ID"] = $_GET['user2'];
}

$deals = CCrmDeal::GetList($arOrder = Array('DATE_CREATE' => 'DESC'), $arFilterDeals, $arSelect = Array());

while ($d = $deals->GetNext()){
	$summInAll = 0;
	$summOutInAll = 0;
	$summOutOutAll = 0;


	if (count($arResult["arrResults"]) > 0) {
		foreach ($arResult["arrResults"] as $arRes) {
			$sum = 0;
			$isIn = false;
			$isOut = false;
			$isOutIn = false;
			$arrColumns = [];
			$arrAnswers = [];
			$arrAnswersVarname = [];
			CForm::GetResultAnswerArray($arParams['WEB_FORM_ID'],
					$arrColumns,
					$arrAnswers,
					$arrAnswersVarname,
					array("RESULT_ID" => $arRes["ID"]));

			$ans = current($arrAnswersVarname);
			if($ans['DEAL_BILLING_OUT_TYPE'][0]['USER_TEXT'] == 'IN'){
				$isOutIn = true;
			}
			foreach ($arResult["arrColumns"] as $FIELD_ID => $arrC) {

				$arrAnswer = $arResult["arrAnswers"][$arRes["ID"]][$FIELD_ID];
				if (is_array($arrAnswer)) {
					foreach ($arrAnswer as $key => $arrA) {
						if($arrA['SID'] == 'DEAL_BILLING_TYPE' && $arrA['USER_TEXT'] == 'IN'){
							$isIn = true;
						}elseif($arrA['SID'] == 'DEAL_BILLING_TYPE' && $arrA['USER_TEXT'] != 'IN'){
							$isOut = true;
						}
						if($arrA['SID'] == 'DEAL_BILLING_SUM'){
							$sum = intval($arrA['USER_TEXT']);
						}
					} //foreach
				}
			} //foreach
			if($isIn){
				$summInAll += $sum;
			}
			if($isOut){
				if($isOutIn){
					$summOutInAll += $sum;
				}else{
					$summOutOutAll += $sum;
				}
			}
		}
	}
	if($d['UF_CRM_DMF_SOL_DATE']){ // Проверка на принадлежность к медиафактам

        // Конвертация даты

		$format = "DD.MM.YYYY HH:MI:SS";
		$new_format = CSite::GetDateFormat("SHORT");
		$DEADLINE = $DB->FormatDate($d['CLOSEDATE'], $format, $new_format);


        $element = array(
            'ID'           => $d['ID'],                                                     // id для ссылки
            'TITLE'        => $d['TITLE'],                                                  // Название
            'OPPORTUNITY'  => number_format($d['OPPORTUNITY'], 2, '.', ' '),                // Бюджет
            'DEBT'         => number_format(($d['OPPORTUNITY'] - $summInAll), 2, '.', ' '), // Долг
            'DEADLINE'     => $DEADLINE,                                                    // План срок
            'CLOSE'     => $d['UF_CRM_DMF_SOL_DATE'],                                       // Факт срок
            'INVOICE'      => '',                                                           // Счет
            'REPORT'       => $d['UF_CRM_DMF_SCR']                                          // Отчет


        );

        // Фильтры после получения элементов

        if($_GET['startdate'] || $_GET['enddate'] || $_GET['debt']){
                // проверка по дате
                if((strlen($_GET['enddate'])!=10 && strlen($_GET['startdate'])==10 && strtotime($_GET['startdate'])<=strtotime($d['UF_CRM_DMF_SOL_DATE'])) || (strlen($_GET['enddate'])==10 && strlen($_GET['startdate'])==10 && strtotime($_GET['startdate'])<=strtotime($d['UF_CRM_DMF_SOL_DATE']) && strtotime($_GET['enddate'])>=strtotime($d['UF_CRM_DMF_SOL_DATE'])) || (strlen($_GET['enddate'])==10 && strlen($_GET['startdate'])!=10 && strtotime($_GET['enddate'])>=strtotime($d['UF_CRM_DMF_SOL_DATE']))){
                    $usl = true;
                }else{
                    $usl = false;
                }
            // проверка долгов
            if(strlen($_GET['debt'])>1){
                if($_GET['debt']=='on' && ($d['OPPORTUNITY'] - $summInAll)>0 && $usl){
                    $usl = true;
                }else{
                    $usl = false;
                }
            }

            if($usl){
                $arResult['ITEMS'][] = $element;
            }
        }else{
            $arResult['ITEMS'][] = $element;
        }


        // Массив сумм данных

		$global_opp = $global_opp + $d['OPPORTUNITY'];                               // Общий Бюджет
		$global_dolg = $global_dolg+($d['OPPORTUNITY'] - $summInAll);                // Общий Долг
		$global_rash = $global_rash+($summOutOutAll+$summOutInAll);                  // Общий Расход
		$global_pol = $global_pol+$summInAll;                                        // Всего Получено
		$global_prib = $global_prib+($summInAll - $summOutOutAll - $summOutInAll);   // Всего Прибыль

		$i++; // Количество обработанных элементов
	}

}
if(!$_GET['EXPORT']){

// Пагинация

// Текущая страница, при отсутствии гет параметра ровна 1

    $page = 1;

    if ($_GET['PAGE']) {
        $page = intval($_GET['PAGE']);
    }

// Возвращаем в шаблон номер текущей страницы

    $arResult['PAGINATION']["PAGE"] = $page; // PAGINATION - массив, содержащий все данные для пагинации

// Функция для формирования пагинации

    function paganation($display_array, $page, $show_per_page)
    {
        $page = $page < 1 ? 1 : $page;
        $start = ($page - 1) * $show_per_page;
        $offset = $show_per_page;
        $outArray = array_slice($display_array, $start, $offset);
        return $outArray;
    }

    $arResult['PAGINATION']['ALL'] = count($arResult['ITEMS']);                                                                    // Общее количество элементов
    $arResult['PAGINATION']['PAGES'] = ceil($arResult['PAGINATION']['ALL'] / $arParams["PERPAGE"]);                                // Количество страниц
    $arResult['ITEMS'] = paganation($arResult['ITEMS'], $arResult['PAGINATION']["PAGE"], $arParams["PERPAGE"]);                    // Обработанный массив элементов
    $arResult['PAGINATION']['OFFSET'] = (($arResult['PAGINATION']["PAGE"] - 1) * $arParams["PERPAGE"])+count($arResult['ITEMS']);  // Количество отображенных элементов

    $arResult['TOTAL'] = array(
        'OPPORTUNITY'  => number_format($global_opp, 2, '.', ' '),      // Общий Бюджет
        'DEBT'         => number_format($global_dolg, 2, '.', ' '),     // Общий Долг
        'CONSUMPTION'  => number_format($global_rash, 2, '.', ' '),     // Общий Расход
        'RECEIVED'     => number_format($global_pol,  2, '.', ' '),     // Всего Получено
        'PROFIT'       => number_format($global_prib, 2, '.', ' '),     // Всего Прибыль
    );
    $url = $_SERVER['REQUEST_URI'];

    $arResult['EXPORT_UPL'] = sgp($url, 'EXPORT', 'TRUE');
    $this->IncludeComponentTemplate();
}else{

    // Fix::
    ob_end_clean();

    // Подключаем класс

    include_once dirname(__FILE__) . "/PHPExcel.php";
    include_once dirname(__FILE__) . '/PHPExcel/IOFactory.php';

    // Берем чистый файл
    $pExcel = PHPExcel_IOFactory::load(dirname(__FILE__) . "/test.xlsx");

    $pExcel->setActiveSheetIndex(0);
    $aSheet = $pExcel->getActiveSheet();

    $aSheet->setTitle('Медиафакт');

    $aSheet->setCellValue('A1','Сделка');
    $aSheet->setCellValue('B1','Бюджет');
    $aSheet->setCellValue('C1','Долг');
    $aSheet->setCellValue('D1','План срок');
    $aSheet->setCellValue('E1','Факт срок');
    $aSheet->setCellValue('F1','Счет');
    $aSheet->setCellValue('G1','Отчет');
    $i = 1;
    foreach ($arResult['ITEMS'] as $item){
        $i++;
        $aSheet->setCellValue('A'.$i, $item['TITLE'] ? $item['TITLE'] : ' ');
        $aSheet->setCellValue('B'.$i, $item['OPPORTUNITY'] ? $item['OPPORTUNITY'] : ' ');
        $aSheet->setCellValue('C'.$i, $item['DEBT'] ? $item['DEBT'] : ' ');
        $aSheet->setCellValue('D'.$i, $item['DEADLINE'] ? $item['DEADLINE'] : ' ');
        $aSheet->setCellValue('E'.$i, $item['CLOSE'] ? $item['CLOSE'] : ' ');
        $aSheet->setCellValue('F'.$i, $item['INVOICE'] ? $item['INVOICE'] : ' ');
        $aSheet->setCellValue('G'.$i, $item['REPORT'] ? $item['REPORT'] : ' ');
    }

    $validLocale = PHPExcel_Settings::setLocale('ru');
    include(dirname(__FILE__) . "/PHPExcel/Writer/Excel5.php");
    $objWriter = new PHPExcel_Writer_Excel5($pExcel);
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="mediafact.xls"');
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output');

}


?>