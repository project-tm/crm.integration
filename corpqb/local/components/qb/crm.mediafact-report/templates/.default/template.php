<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
?>

<style>
	.row-fluid .span9 {
	width: 74.39024390243902%;
}
	.dealings-summa .summa {
	float: left;
	color: #000;
	margin-right: 40px;
		margin-bottom: 10px;
		font-weight: bold;
	}
	.dealings-summa .summa .result {
	font-size: 19px;
		font-weight: normal;
	}
	.dealings-summa .summa .result.blue {
	color: #0088cc;
}
	.dealings-summa .summa .result.red {
	color: #a6342f;
}
	.dealings-summa .summa .result.yellow {
	color: #a9ac32;
}
	.dealings-summa .summa .result.green {
	color: #59ad31;
}
	.deal_billing_stat table thead tr th,
	.deal_billing_stat table tbody tr td
	{
		border-bottom: 1px solid #d4d4d4;
		line-height: 35px;
	}
	.deal_billing_stat table table {
	border-collapse: collapse;
	}
	.b-pagintaion__item{
		list-style: none;
		float: left;
		line-height: 14px;
		padding-right: 10px;
	}
	.paginator{
		float:right;
	}
	.paginator .active a{
		color: #08C!important;
	}
	.row-fluid .span3 {
		width: 23.170731707317074%;
	}
	.well {
		min-height: 20px;
		padding: 19px;
		margin-bottom: 20px;
		background-color: #f5f5f5;
		border: 1px solid #e3e3e3;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	}
	.well .h, .popup .h {
		font-weight: bold;
		color: #999999;
		margin-bottom: 10px;
	}
	.withcaret {
		position: relative;
	}
	.row-fluid [class*="span"]:first-child {
		margin-left: 0;
	}
	.withcaret .clearfilter {
		position: absolute;
		right: -3px;
		top: 7px;
		opacity: 0.8;
		cursor: pointer;
	}
	.filter{
		float: left;
		width: 73%;
		margin-right: 5%;
		border-radius: 5px;
		background: rgb(238, 242, 244);
		padding-bottom: 10px;
		width: 669px;
	}
	.crm-search-inp{
		background: #fff;
		border: 1px solid #c6cdd3;
		border-radius: 2px;
		color: #313232;
		display: inline-block;
		font-size: 13px;
		height: 36px;
		margin: 0;
		outline: 0;
		vertical-align: middle;
		padding: 0 8px;
		padding-top: 0px;
		padding-right: 8px;
		padding-bottom: 0px;
		padding-left: 8px;
		width: 213px;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		box-sizing: border-box;
		margin-top: 9px;
	}
	p.des{
		color: #535C69;
		float: left;
		margin-bottom: 10px;
		padding-left: 10px;
		padding-top: 4px;
		width: 126px;
	}
	.line{
		float: left;
		width: 100%;
		padding: 4px;
		padding-left: 7px;
		padding-top: 0px;
	}
	select#soflow, select#soflow-color {
		background: transparent;
		border: 1px solid #c6cdd3;
		border-radius: 0;
		-webkit-box-shadow: none;
		box-shadow: none;
		color: #535c69;
		display: inline-block;
		font-size: 14px;
		font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
		line-height: 15px;
		height: 36px;
		text-shadow: 0 1px #fff;
		outline: 0;
		overflow: hidden;
		padding: 0 0 0 5px;
		min-width: 100px;
		vertical-align: top;
		-webkit-appearance: none;
		position: relative;
		width: 250px;
		z-index: 1;
	}
	.bx-select-wrap{
		background-color: #fff;
		border-radius: 2px;
		display: inline-block;
		vertical-align: top;
		position: relative;
		 margin-top: 7px;
	}
	.bx-select-wrap:after {
		background: url("/bitrix/components/bitrix/crm.interface.filter/templates/flat/bitrix/main.interface.filter/new/images/filter/filter-sprite.png") no-repeat -9px -641px;
		content: "";
		height: 10px;
		margin-top: -5px;
		right: 8px;
		position: absolute;
		top: 50%;
		width: 5px;
		z-index: 0;
	}
	.bx-filter-bottom-separate {
		background-color: #d8dde0;
		height: 1px;
		margin: 0 18px 13px;
		float: left;
		width: 635px;
		margin-left: 15px;
	}
	.sub{
		background: transparent;
		border-radius: 2px;
		border: 1px solid #a1a6ac;
		box-shadow: none;
		color: #535c69;
		cursor: pointer;
		display: inline-block;
		font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
		font-weight: bold;
		font-size: 12px;
		height: 40px;
		line-height: 38px;
		margin-left: 12px;
		min-width: 55px;
		text-transform: uppercase;
		text-decoration: none;
		padding: 0 15px;
		position: relative;
		vertical-align: middle;
		-webkit-font-smoothing: antialiased;
		-webkit-transition: all .25s linear;
		transition: all .25s linear;
	}
	.sub:hover{
		text-decoration: none;
		background: #cfd4d8!important;
	}
	 th {
		padding: 5px!important;
		border-right: 1px solid #ebebeb;
		border-bottom: 1px solid #cdcdcd;
		border-top: 0;
		border-left: none;
		color: black;
		font-weight: bold;
		background-color: #d9d9d9;
		background-image: url(/bitrix/components/bitrix/crm.interface.grid/templates/.default/bitrix/main.interface.grid/.default/images/listhead.png);
		background-repeat: repeat-x;
		background-position: top;
		cursor: default;
		 border: 0;
		 padding: 0 10px!important;
		 font: 12px/40px "Helvetica Neue",Helvetica,Arial,sans-serif;
		 vertical-align: middle;
		 white-space: nowrap;
		 padding: 6px;
		 border-right: 1px solid #ededed;
		 vertical-align: middle;
		 border-top: 0;
		 border-left: 0;
		 border-right: 0;
	}
	th {
		background: #e4e4e4 url(/local/templates/bitrix24/images/interface/grid/bx-grid-head.png) repeat-x left top!important;
	}
	table{
		border-spacing: 0px;
		margin-top: 30px;
		float: left;
	}
	.deal_billing_stat table tbody tr td {
		border-bottom: 1px solid #d4d4d4;
		line-height: 35px;
		border-right: 1px solid #EDEDED;
	}
</style>

<div class="filter">
	<form action="" type="get">
		<div class="line">
			<p class="des">По дате:</p>
			<input class="crm-search-inp" name='startdate' value="<?= $_GET['startdate'] ?>" type="date">
			<span style="padding-left: 8px; padding-right: 13px;">...</span><input class="crm-search-inp" name='enddate' value="<?= $_GET['enddate'] ?>" type="date">
		</div>
		<div class="line">
			<p class="des">Клиент:</p>
			<span class="bx-select-wrap">
				<select name="client" id="soflow" value="<?= $_GET['client'] ?>">
					<option value="">Клиент</option>
					<? foreach ($arResult['CLIENTS'] as $client){ $i++; ?>
						<option <?= $client['ID']==$_GET['client'] ? 'selected' : '' ?> value="<?=$client['ID']?>"><?=$client['TITLE']?></option>
					<? } ?>
				</select>
			</span>
		</div>
		<div class="line">
			<p class="des">Продажник:</p>
			<span class="bx-select-wrap">
				<select name="user1" id="soflow">
					<option value="">Продажник</option>
					<? foreach ($arResult['USERS'] as $user){ $i++; ?>
						<option <?= $user['ID']==$_GET['user1'] ? 'selected' : '' ?> value="<?=$user['ID']?>"><?=$user['NAME']?></option>
					<? } ?>
				</select>
			</span>
		</div>
		<div class="line">
			<p class="des">Продюссер:</p>
			<span class="bx-select-wrap">
				<select name="user2" id="soflow">
					<option value="">Продюссер</option>
					<? foreach ($arResult['USERS'] as $user){ $i++; ?>
						<option <?= $user['ID']==$_GET['user2'] ? 'selected' : '' ?> value="<?=$user['ID']?>"><?=$user['NAME']?></option>
					<? } ?>
				</select>
			</span>
		</div>
		<div class="line">
			<p class="des">С долгом клиента</p>
			<input type="checkbox" style="width: 28px; height: 49px;" name="debt" <?= $_GET['debt'] ? 'checked' : '' ?>>
		</div>
		<div class="bx-filter-bottom-separate" style=""></div>
		<input type="submit" class="sub">
	</form>
</div>
<div class="dealings-summa">
	<div class="summa"> Бюджет: <br> <span class="result ng-binding"><?= $arResult['TOTAL']['OPPORTUNITY']?></span> </div>
<div class="summa"> Клиент должен: <br> <span class="result blue ng-binding"><?= $arResult['TOTAL']['DEBT']?></span> </div>
<div class="summa"> Расход: <br> <span class="result red ng-binding"><?= $arResult['TOTAL']['CONSUMPTION']?></span> </div>
<div class="summa"> Получено: <br> <span class="result yellow ng-binding"><?= $arResult['TOTAL']['RECEIVED']?></span> </div>
<div class="summa"> Прибыль: <br> <span class="result green ng-binding"><?= $arResult['TOTAL']['PROFIT']?></span> </div>

<div class="summa-all"><span style="font-size: 12px;" class="ng-binding">Сделки: <?=$arResult['PAGINATION']['OFFSET']?> из <?=$arResult['PAGINATION']['ALL']?></span> <br> <a targets="_blank" href="<?=$arResult['EXPORT_UPL']?>">Экспорт в excel</a> </div>
</div>
<div class="deal_billing_stat">
	<table width="100%" id="CRM_ACTIVITY_LIST_MY_ACTIVITIES" class="bx-interface-grid">
		<thead>
		<tr style="color: #636363; border-bottom: 1px solid #d4d4d4;">
			<th>Сделка:</th>
			<th>Бюджет:</th>
			<th>Долг:</th>
			<th>План срок:</th>
			<th>Факт срок:</th>
			<th>Счет:</th>
			<th>Отчет:</th>
		</tr>
		</thead>
		<tbody>
		<? foreach ($arResult['ITEMS'] as $item){ $i++; ?>
				<tr>
					<td><a href="/crm/deal/show/<?= $item['ID'] ?>/"><?= $item['TITLE'] ?></a></td>
					<td><?= $item['OPPORTUNITY'] ?></td>
					<td><?= $item['DEBT'] ?></td>
					<td><?= $item['DEADLINE'] ?></td>
					<td><?= $item['CLOSE'] ?></td>
					<td><?= $item['INVOICE'] ?></td>
					<td><a href="<?= $item['REPORT'] ?>"><?= $item['REPORT'] ?></a></td>
				</tr>
		<? } ?>
		<tr class="bx-grid-footer">
			<td colspan="8">
				<table cellpadding="0" cellspacing="0" border="0" style="margin-top:0px;" class="bx-grid-footer">
					<tbody><tr>
						<td id="crm_activity_list_my_activities_row_count_wrapper">
							Всего элементов: <?=intval($arResult['PAGINATION']['ALL'])?>
						</td>
						<td>
							<div class="modern-page-navigation">
									Страницы:
								<ul class="paginator">
									<? if(intval($arResult['PAGINATION']["PAGE"])>1){ ?>
										<li class="b-pagintaion__item"><a class="pag_link" href="?PAGE=<?=intval($arResult['PAGINATION']["PAGE"])-1?>"><</a></li>
									<? } ?>

									<?for ($x=0; $x++<$arResult['PAGINATION']['PAGES']; ){?>
										<? if($x == intval($arResult['PAGINATION']["PAGE"])){ ?>
											<li class="b-pagintaion__item active"><a style="cursor:pointer" href="?PAGE=<?=$x?>"><?=$x?></a></li>
										<? }else{ ?>
											<li class="b-pagintaion__item"><a style="cursor:pointer" class="pag_link" href="?PAGE=<?=$x?>"><?=$x?></a></li>
										<? } ?>

									<?}?>

									<? if(intval($arResult['PAGINATION']["PAGE"])<intval($arResult['PAGINATION']['PAGES'])){ ?>
										<li class="b-pagintaion__item"><a style="cursor:pointer" class="pag_link" href="?PAGE=<?=intval($arResult['PAGINATION']["PAGE"])+1?>">></a></li>
									<? } ?>
								</ul>
							</div
						</td>
						<td class="bx-right">&nbsp;</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
</div>