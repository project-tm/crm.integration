<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('form');
CModule::IncludeModule('crm');

$crmEvent = new CCrmEvent();
$currentUserID = CCrmSecurityHelper::GetCurrentUserID();
$eventDate = ConvertTimeStamp(time() + CTimeZone::GetOffset(), 'FULL', 's1');

if (isset($_POST['WEB_FORM_ID']) && (strlen($_REQUEST["web_form_apply"]) > 0)) {
    $data = $_POST;

    if (isset($_POST['DEAL_ID'])) {
        $FIELD_SID = "DEAL_BILLING_DEAL_ID"; // символьный идентификатор вопроса или поля веб-формы
        $rsField = CFormField::GetBySID($FIELD_SID);
        $arField = $rsField->Fetch();
        $data['form_text_ADDITIONAL_' . $arField['ID']] = $_POST['DEAL_ID'];

        $FIELD_SID = "DEAL_BILLING_OUT_TYPE"; // символьный идентификатор вопроса или поля веб-формы
        $rsField = CFormField::GetBySID($FIELD_SID);
        $arField = $rsField->Fetch();
        $data['form_text_ADDITIONAL_' . $arField['ID']] = 'OUT';
    }

    // check errors
    $formErrors = CForm::Check($_POST["WEB_FORM_ID"], $data, false, "Y", "Y");

    if (count($formErrors) <= 0) {
        // check user session
        if (check_bitrix_sessid()) {
            $return = false;
            // add result
            if (isset($_POST['RESULT_ID'])) {
                if (CFormResult::Update($_POST['RESULT_ID'], $data, 'Y')) {
                    // send email notifications
                    CFormCRM::onResultAdded($_POST["WEB_FORM_ID"], $_POST['RESULT_ID']);
                    CFormResult::SetEvent($_POST['RESULT_ID']);
                    CFormResult::Mail($_POST['RESULT_ID']);

                    echo json_encode(['result' => 'ok']);

                } else {
                    echo json_encode(['result' => 'fail', 'errors' => $GLOBALS["strError"]]);
                }
            } else {
                if (CModule::IncludeModule("highloadblock")) {
                    $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getList(['filter' => ['NAME' => 'PaymentTypeCommission']])->fetch();
                    $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
                    $entity_data_class = $entity->getDataClass();

                    $formHelper = \helpers\FormHelper::getInstance();
                    $fieldType = $formHelper->getAnswerFirstFieldInfo('DEAL_BILLING', 'DEAL_BILLING_TYPE');
                    $fieldSum = $formHelper->getAnswerFirstFieldInfo('DEAL_BILLING', 'DEAL_BILLING_SUM');
                    $aResult = $entity_data_class::getList(['filter' => ['UF_PAYMENT_IDN' => $data['form_' . $fieldType['FIELD_TYPE'] . '_' . $fieldType['ID']]]])->fetch();
                    if ($aResult['UF_PAYMENT_COMM'] > 0) {
                        $data['form_' . $fieldSum['FIELD_TYPE'] . '_' . $fieldSum['ID']] = intval($aResult['UF_PAYMENT_COMM']) / 100 * $data['form_' . $fieldSum['FIELD_TYPE'] . '_' . $fieldSum['ID']] + $data['form_' . $fieldSum['FIELD_TYPE'] . '_' . $fieldSum['ID']];
                    }
                }

                if ($RESULT_ID = CFormResult::Add($_POST["WEB_FORM_ID"], $data)) {

                    $eventMess = $aResult['UF_PAYMENT_TYPE'] . ': ' . $data['form_' . $fieldSum['FIELD_TYPE'] . '_' . $fieldSum['ID']];

                    $r = $crmEvent->Add([
                        'ENTITY_TYPE' => 'DEAL',
                        'ENTITY_ID' => $_POST['DEAL_ID'],
                        'EVENT_TYPE' => CCrmEvent::TYPE_CHANGE,
                        'USER_ID' => $currentUserID,
                        'EVENT_NAME' => 'Добавление записи(Приход/Расход)',
                        'EVENT_TEXT_1' => $eventMess,
                        'DATE_CREATE' => $eventDate,
                    ]);

                    CFormResult::Update($RESULT_ID, $data, 'Y');

                    //$arResult["FORM_NOTE"] = GetMessage("FORM_DATA_SAVED1").$RESULT_ID.GetMessage("FORM_DATA_SAVED2");
                    $arResult["FORM_RESULT"] = 'addok';

                    // send email notifications
                    CFormCRM::onResultAdded($_POST["WEB_FORM_ID"], $RESULT_ID);
                    CFormResult::SetEvent($RESULT_ID);
                    CFormResult::Mail($RESULT_ID);

                    // choose type of user redirect and do it

                    echo json_encode(['result' => 'ok']);

                } else {
                    echo json_encode(['result' => 'fail', 'errors' => $GLOBALS["strError"]]);
                }
            }
        }
    } else {
        echo json_encode(['result' => 'fail', 'errors' => $formErrors]);
    }
} elseif (isset($_GET['WEB_FORM_ID']) && (isset($_GET['del_id']) || isset($_GET['del_ids'])) && check_bitrix_sessid()) {
    // deleting single form result

    $arPaymentType = [];

    if (CModule::IncludeModule("highloadblock")) {
        $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getList(['filter' => ['NAME' => 'PaymentTypeCommission']])->fetch();
        $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $arrRes = $entity_data_class::getList();
        while ($item = $arrRes->fetch()) {
            $arPaymentType[$item['UF_PAYMENT_IDN']] = $item['UF_PAYMENT_TYPE'];
        }

    }
    $rsUsers = CUser::GetList($by, $order, ['GROUPS_ID' => [1]]);
    $emails = [];
//    $emails[] = 'x_drw@bk.ru';
    while ($user = $rsUsers->Fetch()) {
        //$emails[] = $user['EMAIL'];
    }

    $del_id = intval($_GET["del_id"]);

    if ($del_id > 0) {
        CForm::GetResultAnswerArray($_GET['WEB_FORM_ID'],
            $arrColumns,
            $arrAnswers,
            $arrAnswersVarname,
            array("RESULT_ID" => $del_id));

        CFormResult::Delete($del_id);

        $arrAnswersVarname = current($arrAnswersVarname);
        // Сообщение
        $message = '';
        $message .= '<b>Название:</b> ' . $arrAnswersVarname['DEAL_BILLING_TITLE'][0]['USER_TEXT'] . '<br />';
        $message .= '<b>Сумма:</b> ' . $arrAnswersVarname['DEAL_BILLING_SUM'][0]['USER_TEXT'] . '<br />';
        $message .= '<b>Вид:</b> ' . $arPaymentType[$arrAnswersVarname['DEAL_BILLING_TYPE'][0]['USER_TEXT']] . '<br />';
        $message .= '<b>Дата:</b> ' . $arrAnswersVarname['DEAL_BILLING_DATE'][0]['USER_TEXT'] . '<br />';

        $arParams = [
            'MESSAGE' => $message,
            'DEAL_ID' => $_GET['DEAL_ID'],
            'EMAILS' => implode(',', $emails),
        ];

        CEvent::Send('DEAL_BILLING_DELETE_RESULT', ['s1'], $arParams);

        $eventMess = $arPaymentType[$arrAnswersVarname['DEAL_BILLING_TYPE'][0]['USER_TEXT']] . ': ' . $arrAnswersVarname['DEAL_BILLING_SUM'][0]['USER_TEXT'];

        $crmEvent->Add([
            'ENTITY_TYPE' => 'DEAL',
            'ENTITY_ID' => $_GET['DEAL_ID'],
            'EVENT_TYPE' => CCrmEvent::TYPE_CHANGE,
            'USER_ID' => $currentUserID,
            'EVENT_NAME' => 'Удаление записи(Приход/Расход)',
            'EVENT_TEXT_1' => $message,
            'DATE_CREATE' => $eventDate,
        ]);
    }

    // deleting single form result
    $del_ids = $_GET["del_ids"];

    if (count($del_ids) > 0) {
        $message = '';

        foreach ($del_ids as $del_id) {
            $arrColumns = [];
            $arrAnswers = [];
            $arrAnswersVarname = [];
            $eventMess = '';
            CForm::GetResultAnswerArray($_GET['WEB_FORM_ID'],
                $arrColumns,
                $arrAnswers,
                $arrAnswersVarname,
                array("RESULT_ID" => $del_id));

            //CFormResult::Delete($del_id);

            $arrAnswersVarname = current($arrAnswersVarname);
            // Сообщение
            $eventMess .= '<b>Название:</b> ' . $arrAnswersVarname['DEAL_BILLING_TITLE'][0]['USER_TEXT'] . '<br />';
            $eventMess .= '<b>Сумма:</b> ' . $arrAnswersVarname['DEAL_BILLING_SUM'][0]['USER_TEXT'] . '<br />';
            $eventMess .= '<b>Вид:</b> ' . $arPaymentType[$arrAnswersVarname['DEAL_BILLING_TYPE'][0]['USER_TEXT']] . '<br />';
            $eventMess .= '<b>Дата:</b> ' . $arrAnswersVarname['DEAL_BILLING_DATE'][0]['USER_TEXT'] . '<br /><hr />';

            $crmEvent->Add([
                'ENTITY_TYPE' => 'DEAL',
                'ENTITY_ID' => $_GET['DEAL_ID'],
                'EVENT_TYPE' => CCrmEvent::TYPE_CHANGE,
                'USER_ID' => $currentUserID,
                'EVENT_NAME' => 'Удаление записи(Приход/Расход)',
                'EVENT_TEXT_1' => $eventMess,
                'DATE_CREATE' => $eventDate,
            ]);
            $message .= $eventMess;
        }

        echo $message;

        $arParams = [
            'MESSAGE' => $message,
            'DEAL_ID' => $_GET['DEAL_ID'],
            'EMAILS' => implode(',', $emails),
        ];

        CEvent::Send('DEAL_BILLING_DELETE_RESULT', ['s1'], $arParams);
    }
}

exit();