<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>

<?= sprintf( // form header (<form> tag and hidden inputs)
    "<form name=\"%s\" action=\"%s\" method=\"%s\" enctype=\"multipart/form-data\">",
    'DEAL_DOCS', '/local/components/qb/deal.docs.list/templates/.default/ajax_save.php', "POST"
) ?>
<?= bitrix_sessid_post() ?>
<div class="deal_form_result"></div>
<input type="hidden" name="WEB_FORM_ID" value="<?= $arParams['WEB_FORM_ID'] ?>">
<input type="hidden" name="DEAL_ID" value="<?= $arParams['DEAL_ID'] ?>">
<input type="hidden" name="web_form_apply" value="1">

<table class="form-table my-data-table" width="100%">
    <tbody>
    <?
    foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
        if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') {
            echo $arQuestion["HTML_CODE"];
        } else {
            ?>

            <tr>
                <td>
                    <? if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
                        <span class="error-fld" title="<?= $arResult["FORM_ERRORS"][$FIELD_SID] ?>"></span>
                    <?endif; ?>
                    <?= $arQuestion["CAPTION"] ?><? if ($arQuestion["REQUIRED"] == "Y"):?><?= $arResult["REQUIRED_SIGN"]; ?><?endif; ?>
                    <?= $arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />" . $arQuestion["IMAGE"]["HTML_CODE"] : "" ?>
                </td>
                <td><?= $arQuestion["HTML_CODE"] ?></td>
            </tr>

            <?
        }
    } //endwhile
    ?>
    <tr>
        <td>
            <input type="submit" name="web_form_submit" value="Добавить"/>
        </td>
    </tr>
    </tbody>
</table>
<?= $arResult["FORM_FOOTER"] ?>
