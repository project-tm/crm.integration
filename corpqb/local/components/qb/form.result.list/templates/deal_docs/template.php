<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
//$this->setFrameMode(true);
//$APPLICATION->AddHeadScript("/bitrix/js/calendar/cal-core.js", false);
//$APPLICATION->AddHeadScript("/bitrix/js/calendar/cal-planner.js", false);
$APPLICATION->AddHeadString('<link href="' . SITE_TEMPLATE_PATH . '/css/main-style.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="' . $this->GetFolder() . '/css/style.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="' . SITE_TEMPLATE_PATH . '/css/dataTables.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="/bitrix/js/crm/css/crm.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>');
$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.11/js/jquery.dataTables.min.js"></script>');

?>

<?
if (strlen($arResult["FORM_ERROR"]) > 0) ShowError($arResult["FORM_ERROR"]);
if (strlen($arResult["FORM_NOTE"]) > 0) ShowNote($arResult["FORM_NOTE"]);

$isAdmin = false;
$moreColumnCount = 2;

if (CSite::InGroup(array(1))) {
    $moreColumnCount += 1;
    $isAdmin = true;
}
//
//echo '<pre>';
//print_r($arResult);
//echo '</pre>';
?>
<form method="post" id="editableForm">
    <table cellpadding='5' cellspacing='0' id='ordersTable' class="display">
        <thead>
        <tr>
            <th style="padding-left: 10px"><input type="checkbox" class="check_all"/></th>
            <th width="10px"></th>
            <th>Добавил</th>
            <? if (is_array($arResult["arrColumns"])) {
                foreach ($arResult["arrColumns"] as $arrCol) {
                    ?>
                    <th><?= $arrCol["RESULTS_TABLE_TITLE"] ?></th><?
                }
            } ?>
<!--            <th>Статус</th>-->
            <th></th>
        </tr>
        </thead>
        <tfoot class="order_footer">
        <tr>
            <th></th>
            <th></th>
            <th style="text-align: right"></th>
            <?
            if (is_array($arResult["arrColumns"])) {
                foreach ($arResult["arrColumns"] as $arrCol) {
                    ?>
                    <th style="<?= $arrCol['SID'] == 'PRICE' ? 'text-align: right;' : '' ?>"></th>
                    <?
                }
            }
            ?>
<!--            <th></th>-->
            <th></th>
        </tr>
        </tfoot>
        <? if (count($arResult["arrResults"]) > 0) { ?>
            <tbody>
            <? foreach ($arResult["arrResults"] as $arRes) { ?>
                <tr>
                    <td><? if ($isAdmin || $arRes['STATUS_ID'] == 45) : ?><input type="checkbox" name="check_order[]"
                                                                                 class="chBox"
                                                                                 value="<?= $arRes["ID"] ?>"><? endif; ?>
                    </td>
                    <td style="text-align: right"></td>
                    <td><?= $arRes['USER_FIRST_NAME'] . ' ' . $arRes['USER_SECOND_NAME'] . ' ' . $arRes['USER_LAST_NAME'] ?></td>
                    <? foreach ($arResult["arrColumns"] as $FIELD_ID => $arrC) { ?>
                        <?php
                        $firstElem = current($arResult["arrAnswers"][$arRes["ID"]][$FIELD_ID]);
                        ?>
                        <td class="editable<?//= in_array($firstElem['SID'], $editableTD) ? 'editable' : 'hidden_editable' ?>">
                            <?
                            $entityInfo = [];
                            $arrAnswer = $arResult["arrAnswers"][$arRes["ID"]][$FIELD_ID];
                            if (is_array($arrAnswer)) {
                                foreach ($arrAnswer as $key => $arrA) {
                                    $answerText = "";
                                    if (strlen(trim($arrA["USER_TEXT"])) > 0) {
                                        $answerText = $arrA["USER_TEXT"];
                                    }
                                    if (strlen(trim($arrA["ANSWER_TEXT"])) > 0) {
                                        $answerText = $arrA["ANSWER_TEXT"];
                                    }

                                    $originalAnswerText = $answerText;
//                                    echo '<pre>';
//                                    print_r($arrA);
//                                    echo '</pre>';
                                    echo '<span class="originalAnswerText">' . $answerText . '</span>';

                                    switch ($arrA['FIELD_TYPE']) {
                                        case 'text':
                                            echo '<input type="text" value="' . (isset($entityInfo['TITLE']) ? $entityInfo['TITLE'] : $answerText) . '" name="EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' . $arrA['ANSWER_ID'] . ']"/>';
                                            break;
                                        case
                                        'textarea':
                                            echo '<textarea name="EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' . $arrA['ANSWER_ID'] . ']">' . $answerText . '</textarea>';
                                            break;
                                        case 'radio':
                                            $html = '<select name="EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' . $arrA['SID'] . ']">';
                                            foreach ($arResult["arrColumns"][$arrA['FIELD_ID']]['ANSWERS'] as $item) {
                                                $html .= '<option value="' . $item['ID'] . '" ' . ($arrA['ANSWER_ID'] == $item['ID'] ? "selected" : "") . '>' . $item['MESSAGE'] . '</option>';
                                            }
                                            $html .= '</select>';
                                            echo $html;
                                            break;
                                        case "file":
                                            $res = "";
                                            if ($arFile = CFormResult::GetFileByAnswerID($arRes["ID"],  $arrA['ANSWER_ID']))
                                            {
                                                if (intval($arFile["USER_FILE_ID"])>0)
                                                {
                                                    $res .= "&nbsp;&nbsp;[<a class=\"tablebodylink\" href=\"/bitrix/tools/form_show_file.php?rid=".$arRes['ID']."&hash=".$arFile["USER_FILE_HASH"]."&lang=".LANGUAGE_ID."&action=download\">".GetMessage("FORM_DOWNLOAD")."</a>(" . CFile::FormatSize($arFile["USER_FILE_SIZE"]) . ")]";
                                                    $res .= "<br />";
                                                } //endif;
                                            } //endif;


                                            $res .= '<input name="EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' .  $arrA['ANSWER_ID'] . ']" class="inputfile" size="0" type="file">';

                                            echo $res;

                                            break;
                                        default:
                                            echo '<input type="text" value="' . (isset($entityInfo['TITLE']) ? $entityInfo['TITLE'] : $answerText) . '" name="EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' . $arrA['ANSWER_ID'] . ']"/>';
                                            break;
                                    }
                                    //}
                                } //foreach
                            } // endif (is_array($arrAnswer));
                            ?>
                        </td>
                        <?
                    } //foreach
                    ?>
                    <td width="50px">
                            <a href="/deal/ajax.php?WEB_FORM_ID=<?= $arParams["WEB_FORM_ID"] ?>&RESULT_ID=<?= $arRes['ID'] ?>"
                               title="Редактировать" class="action_btn_link">
                                <span class="feed-destination-edit"></span>
                            </a>
                            <a href="/services/requests/form_list.php?WEB_FORM_ID=<?= $arParams["WEB_FORM_ID"] ?>&del_id=<?= $arRes['ID'] ?>"
                               title="Удалить" class="action_btn_link">
                                <span class="feed-destination-delete"></span>
                            </a>
                    </td>
                </tr>
                <?
            } //foreach
            ?>
            </tbody>
            <?
        } ?>
    </table>

    <div class="control_panel">
        <div class="btn btn-del">Удалить</div>
    </div>
</form>

<div class="dw_modal" style="display: none;">

</div>

<?CUtil::InitJSCore(array('window'));?>
<script>
    // Variable to store your files
    var files;

    // Add events
    $('input[type=file]').on('change', prepareUpload);

    // Grab the files and set them to our variable
    function prepareUpload(event)
    {
        files = event.target.files;
    }
    var Dialog;
    $(document).ready(function () {
        $('#ordersTable').on('click', '.feed-destination-edit', function (e) {
            e.preventDefault();
            var aAAA = $(this).closest('a');
            var form = $.get($(aAAA).attr('href'), function (data) {
                Dialog = new BX.CDialog({
                    title: "Редактирование",
                    content: data,
                    icon: 'head-block',

                    resizable: true,
                    draggable: true,
                    height: '300',
                    width: '400',
                    buttons: false
                });
                Dialog.Show();
            });
        });

        $('body').on('click', '#cancel', function () {
           Dialog.Close();
        });

//        $('body').on('click', 'input[name="web_form_submit"]', function () {
//            var url = $('form[name="AJAX_DOCS"]').attr('action');
//            var data = $('form[name="AJAX_DOCS"]').serialize();
//            $.each($('.inputfile')[0].files, function(i, file) {
//                data.append('file-'+i, file);
//            });
//            console.log($('.inputfile')[0].files);
//            jQuery.ajax({
//                url: url,
//                data: data,
//                cache: false,
//                contentType: false,
//                processData: false,
//                type: 'POST',
//                success: function(data){
//                    alert(data);
//                }
//            });
//        });

        $('#editableForm').on('click', '.btn-edit', function (e) {
            var trEditableArr = $('#ordersTable').find('input[type=checkbox]:checked').closest('tr');
            var self = $(this);
            if (self.hasClass('editable')) {
                var data = {};
                data['sessid'] = '<?=bitrix_sessid()?>';
                trEditableArr.each(function (indx, element) {
                    var tdEditable = $(element).find('td.editable');
                    tdEditable.each(function (indx, element) {
                        if ($(element).find('textarea').length) {
                            data[$(element).find('textarea').attr('name')] = $(element).find('textarea').html();
                        } else {
                            data[$(element).find('input, select').attr('name')] = $(element).find('input, select').val();
                        }
                    });
                });

                $.ajax({
                    type: 'POST',
                    data: data,
                    success: function () {
                        trEditableArr.each(function (indx, elementTr) {
                            var tdEditable = $(elementTr).find('td.editable, td.hidden_editable');
                            $(elementTr).addClass('enable');
                            tdEditable.each(function (indx, element) {
                                if ($(element).find('select').length) {
                                    $(element).find('span.originalAnswerText').html($(element).find('select option:selected'));
                                }else if ($(element).find('input[type=file]').length) {
                                    $(element).find('input[type=file]').html($(element).find('input[type=file]'));
                                } else if ($(element).find('.bx-crm-edit-crm-entity-field a.bx-crm-entity-info-link').length) {
                                    $(element).find('span.originalAnswerText').html($(element).find('.bx-crm-edit-crm-entity-field a.bx-crm-entity-info-link').clone());
                                } else if ($(element).find('input').length) {
                                    $(element).find('span.originalAnswerText').html($(element).find('input').val());
                                } else if ($(element).find('textarea').length) {
                                    $(element).find('span.originalAnswerText').html($(element).find('textarea').html());
                                }
                            });
                            $(elementTr).find('input.chBox').prop("checked", false);
                            $(elementTr).removeClass('enable');
                        });
                        self.removeClass('editable');
                        self.text('Редактировать');
                        self.parent().removeClass('edit');
                        $('#editableForm input.check_all').prop("checked", false);
                    }
                });
            } else {
                $('#ordersTable').find('tr').removeClass('enable');
                if (trEditableArr.length) {
                    self.addClass('editable');
                    self.text('Сохранить');
                    self.parent().addClass('edit');
                    trEditableArr.each(function (indx, element) {
                        $(element).addClass('enable');
                    });
                }
            }
        });

        $('#editableForm').on('click', '.btn-change', function (e) {
            var trEditableArr = $('#ordersTable').find('input[type=checkbox]:checked').closest('tr');
            var data = {};
            data['sessid'] = '<?=bitrix_sessid()?>';
            data['CHANGE_STATUS'] = $('#editableForm .control_panel .status_change select option:selected').val();
            if (data['CHANGE_STATUS'] > 0) {
                var ids = [];
                trEditableArr.each(function (indx, element) {
                    ids.push($(element).find('input.chBox').val());
                });

                data['CHANGE_STATUS_IDS'] = ids;
                if (ids.length > 0) {
                    $.ajax({
                        type: 'POST',
                        data: data,
                        success: function () {
                            trEditableArr.each(function (indx, elementTr) {
                                var str = $('#editableForm .control_panel .status_change select option:selected').text();
                                str = str.substring(str.indexOf(' '), str.length)
                                $(elementTr).find('td.status').text(str);
                                $(elementTr).find('input.chBox').prop("checked", false);
                                $('#editableForm input.check_all').prop("checked", false);
                            });
                        }
                    });
                }
            }
        });

        $('#editableForm').on('click', '.btn-cancel', function (e) {
            var self = $(this);
            var trEditableArr = $('#ordersTable').find('input[type=checkbox]:checked').closest('tr');
            self.parent().removeClass('edit');
            self.parent().find('.btn-edit').removeClass('editable').text('Редактировать');
            trEditableArr.each(function (indx, element) {
                $(element).removeClass('enable');
            });
        });

        $('#editableForm').on('click', '.btn-del', function (e) {
            var trEditableArr = $('#ordersTable').find('input[type=checkbox]:checked').closest('tr');
            var data = {};
            var ids = [];
            data['sessid'] = '<?=bitrix_sessid()?>';

            trEditableArr.each(function (indx, element) {
                ids.push($(element).find('input.chBox').val());
            });

            data['del_ids'] = ids;
            $.ajax({
                type: 'GET',
                data: data,
                success: function () {
                    trEditableArr.each(function (indx, elementTr) {
                        elementTr.remove();
                        $('#editableForm input.check_all').prop("checked", false);
                    });
                }
            });
        });

        $('#editableForm').on('click', 'input.check_all', function (e) {
            $('#ordersTable').find('input[type=checkbox]').prop("checked", $(this).prop("checked"));
        });

        var t = $('#ordersTable').DataTable({
            "columnDefs": [
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                },
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 1
                }
//                ,
//                {
//                    "targets": <?//=$moreColumnCount?>//,
//                    "render": function (data, type, full, meta) {
//                        if (type === 'filter') {
//                            return $('#ordersTable').DataTable().cell(meta.row, meta.col).nodes().to$().find('span.originalAnswerText').text();
//                        } else {
//                            return data;
//                        }
//                    }
//                }
                <? if($isAdmin) :?>
                ,
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 5
                }
                <? endif; ?>

            ],
            "info": false,
            "paging": false,
            "order": [[2, 'asc']],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Russian.json"
            },
        });

        t.on('order.dt search.dt', function () {
            t.column(1, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

    });
</script>