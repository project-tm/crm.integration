<? use helpers\FormHelper;
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
//$this->setFrameMode(true);
//$APPLICATION->AddHeadScript("/bitrix/js/calendar/cal-core.js", false);
//$APPLICATION->AddHeadScript("/bitrix/js/calendar/cal-planner.js", false);
$APPLICATION->AddHeadString('<link href="' . SITE_TEMPLATE_PATH . '/css/main-style.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="' . $this->GetFolder() . '/css/style.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="' . SITE_TEMPLATE_PATH . '/css/dataTables.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<link href="/bitrix/js/crm/css/crm.css" type="text/css" rel="stylesheet">');
$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>');
$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.11/js/jquery.dataTables.min.js"></script>');

?>

<?
if (strlen($arResult["FORM_ERROR"]) > 0) ShowError($arResult["FORM_ERROR"]);
if (strlen($arResult["FORM_NOTE"]) > 0) ShowNote($arResult["FORM_NOTE"]);

$isAdmin = false;
if (CSite::InGroup(array(1))) {
    $isAdmin = true;
}

$cashAnswerId = null;
$orderFieldId = null;
$bankAnswerId = null;

if (is_array($arResult["arrColumns"])) {
    foreach ($arResult["arrColumns"] as $arrCol) {
        if ($arrCol['SID'] == 'ORDER_TYPE') {

            $orderType = $arrCol['ANSWERS'];

            if($orderType){
                foreach ($orderType as $item){
                    if($item['FIELD_PARAM'] == 'CASH'){
                        $cashAnswerId = $item['ID'];
                    }
                    if($item['FIELD_PARAM'] == 'BANK'){
                        $bankAnswerId = $item['ID'];
                    }
                }

                $orderFieldId = $arrCol['ID'];
            }
        }
    }
}

$arStatuses = [];

if (isset($arResult['arStatuses_MOVE'])) {
    foreach ($arResult['arStatuses_MOVE'] as $item) {
        $arStatuses[$item['REFERENCE_ID']] = $item['REFERENCE'];
    }
}

$editableTD = [
    'ORDER_PRIORITY',
    'PRICE',
    'ORDER_ASSIGNMENT',
    'ORDER_CONTR',
    'ORDER_QB_LC_ID',
    'ORDER_QB_LC_NAME',
];

$bufScript = null;


/** @var FormHelper $formHelper */
$formHelper = FormHelper::getInstance();
$formStatuses = $formHelper->getFormStatus((int)$arParams["WEB_FORM_ID"]);
$unpaidStatusIDs = [];
foreach($formStatuses as $code => $status){
    if($code != 'statusgreen paid'){
        $unpaidStatusIDs[] = $status;
    } else {
        $paidStatusID = $status['ID'];
    }
}
$newStatusId = $formStatuses['statusgray']['ID'];
$payFormId = $formHelper->getFormParameters('payform')['ID'];

?>
<form method="post" id="editableForm">
    <div class="control_panel">
        <a href="/services/requests/form.php?WEB_FORM_ID=<?= $_GET['WEB_FORM_ID'] ?>" class="btn btn-edit">Новая заявка</a>
    </div>
    <div class="pager">
        <?=$arResult["pager"]?>
    </div>
    <table cellpadding='5' cellspacing='0' id='ordersTable' class="display">
        <thead>
        <tr>
            <th style="padding-left: 10px"><input type="checkbox" class="check_all"/></th>
<!--            <th></th>-->
            <?
            $col = 0;
            if (is_array($arResult["arrColumns"])) {
                foreach ($arResult["arrColumns"] as $arrCol) {
                    if ($isAdmin && $col == 1) :
                        echo '<th class="fio">ФИО</th>';
                    endif;
                    ?>
                    <th class="<?=$arrCol['SID']?>"><?= $arrCol["RESULTS_TABLE_TITLE"] ?></th><?
                    $col++;
                }
            } ?>
            <th>Статус</th>
            <th></th>
        </tr>
        </thead>
        <tfoot class="order_footer">
        <tr>
            <th></th>
<!--            <th style="text-align: right">&Sigma;:</th>-->
            <?
            $col = 0;
            if (is_array($arResult["arrColumns"])) {
                foreach ($arResult["arrColumns"] as $arrCol) {
                    if($arrCol['SID'] == 'ORDER_PRIORITY'){
                        echo '<th style="text-align: right">&Sigma;:</th>';
                    }else{
                        ?>
                        <th style="<?= $arrCol['SID'] == 'PRICE' ? 'text-align: right;' : '' ?>"></th>
                        <?
                    }

                    if ($isAdmin && $col == 1) :
                        echo '<th></th>';
                    endif;
                    $col++;
                }
            }
            ?>
            <th></th>
            <th></th>
        </tr>
        </tfoot>
        <? if (count($arResult["arrResults"]) > 0) { ?>
            <tbody>
            <? foreach ($arResult["arrResults"] as $arRes) { ?>
                <tr>
                    <td><? if ($isAdmin || $arRes['STATUS_ID'] == $newStatusId) : ?><input type="checkbox" name="check_order[]"
                                                                                 class="chBox"
                                                                                 value="<?= $arRes["ID"] ?>"><? endif; ?>
                    </td>
<!--                    <td style="text-align: right"></td>-->
                    <?
                    $col = 0;
                    foreach ($arResult["arrColumns"] as $FIELD_ID => $arrC) { ?>
                        <?php
                        $firstElem = current($arResult["arrAnswers"][$arRes["ID"]][$FIELD_ID]);
                        if ($isAdmin && $col == 1) : ?>
                            <td><?= $arRes['USER_FIRST_NAME'] . ' ' . $arRes['USER_SECOND_NAME'] . ' ' . $arRes['USER_LAST_NAME'] ?></td>
                        <? endif;
                        $col++;
                        ?>
                        <td class="<?= in_array($arrC['SID'], $editableTD) ? 'editable' : 'hidden_editable' ?>">
                            <?

                            $entityInfo = [];
                            $arrAnswer = $arResult["arrAnswers"][$arRes["ID"]][$FIELD_ID];
                            if (is_array($arrAnswer)) {
                                foreach ($arrAnswer as $key => $arrA) {
                                    $answerText = "";
                                    if (strlen(trim($arrA["USER_TEXT"])) > 0) {
                                        $answerText = $arrA["USER_TEXT"];
                                    }
                                    if (strlen(trim($arrA["ANSWER_TEXT"])) > 0) {
                                        $answerText = $arrA["ANSWER_TEXT"];
                                    }

                                    $originalAnswerText = $answerText;

                                    //if ($answerText) {
                                    $style = "";
//                                        if(is_numeric($answerText)){
//                                            $style = "float:right";
//                                        }
                                    if ($arrA["SID"] == "PRICE") {
                                        $answerText = number_format($answerText, 0, ".", " ");
                                    }
                                    if ($arrA["SID"] == "PRICE" || $arrA["SID"] == "ORDER_QB_LC_ID" || $arrA["SID"] == "ORDER_PRIORITY") {
                                        $style = "float:right";
                                    }
                                    if ($arrA["SID"] == "ORDER_CONTR") {
                                        switch (substr($answerText, 0, 2)) {
                                            case 'C_':
                                                $valEntityType = 'contact';
                                                break;
                                            case 'CO':
                                                $valEntityType = 'company';
                                                break;
                                            default:
                                                $valEntityType = '';
                                        }
                                        if ($valEntityType != '') {
                                            $entityID = intval(substr($answerText, intval(strpos($answerText, '_')) + 1));
                                            $entityInfo = CCrmEntitySelectorHelper::PrepareEntityInfo($valEntityType, $entityID);
                                            $answerText = '<a href="' . htmlspecialcharsbx($entityInfo['URL']) . '" target="_blank" class="bx-crm-entity-info-link">' . htmlspecialcharsEx($entityInfo['TITLE']) . '</a>';
                                        }
                                    }

                                    echo '<span style="' . $style . '" class="originalAnswerText">' . $answerText . '</span>';
                                    switch ($arrA['FIELD_TYPE']) {
                                        case 'text':
                                        case 'textarea':
                                            if ($arrA["SID"] == "ORDER_CONTR") {
                                                CCrmComponentHelper::RegisterScriptLink('/bitrix/js/crm/crm.js');
                                                CCrmComponentHelper::RegisterScriptLink('/bitrix/js/crm/interface_form.js');
                                                $params = [
                                                    'CONTEXT' => 'ORDER_LIST_' . $arRes["ID"] . '_' . $arrA['ANSWER_ID'],
                                                    'ENTITY_TYPE' => [
                                                        'contact',
                                                        'company',
                                                    ],
                                                    'INPUT_NAME' => 'EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' . $arrA['ANSWER_ID'] . ']',
                                                    'INPUT_VALUE' => $originalAnswerText,
                                                ];
                                                $context = isset($params['CONTEXT']) ? $params['CONTEXT'] : '';
                                                $entityID = $inputValue = isset($params['INPUT_VALUE']) ? $params['INPUT_VALUE'] : '';
                                                $entityType = isset($params['ENTITY_TYPE']) ? $params['ENTITY_TYPE'] : '';
                                                switch (substr($entityID, 0, 2)) {
                                                    case 'C_':
                                                        $valEntityType = 'contact';
                                                        break;
                                                    case 'CO':
                                                        $valEntityType = 'company';
                                                        break;
                                                    default:
                                                        $valEntityType = '';
                                                }
                                                $entityID = intval(substr($entityID, intval(strpos($entityID, '_')) + 1));
                                                $editorID = "eF_{$params['INPUT_NAME']}";
                                                $containerID = "eF_FС_{$params['INPUT_NAME']}";
                                                $selectorID = "eF_ES_{$params['INPUT_NAME']}";
                                                $changeButtonID = "eF_CB_{$params['INPUT_NAME']}";
                                                $addContactButtonID = "eF_ACB_{$params['INPUT_NAME']}";
                                                $addCompanyButtonID = "eF_ACOB_{$params['INPUT_NAME']}";
                                                $dataInputName = isset($params['INPUT_NAME']) ? $params['INPUT_NAME'] : $params['INPUT_NAME'];
                                                $dataInputID = "eF_DI_{$dataInputName}";
                                                $entityInfo = CCrmEntitySelectorHelper::PrepareEntityInfo($valEntityType, $entityID);

//                                                echo '$selectorID<pre>';
//                                                print_r($selectorID);
//                                                echo '</pre>';

                                                $res = '<div id="' . htmlspecialcharsbx($containerID) . '" class="bx-crm-edit-crm-entity-field">
                                                            <div class="bx-crm-entity-info-wrapper">
                                                            ' . (($entityID > 0) ? '<a href="' . htmlspecialcharsbx($entityInfo['URL']) . '" target="_blank" class="bx-crm-entity-info-link">' . htmlspecialcharsEx($entityInfo['TITLE']) . '</a><span class="crm-element-item-delete"></span>' : '') . '
                                                            </div>
                                                            <input type="hidden" id="' . htmlspecialcharsbx($dataInputID) . '" name="' . htmlspecialcharsbx($dataInputName) . '" value="' . htmlspecialcharsbx($inputValue) . '" data-resid = ' . $arRes["ID"] . ' />
                                                            <span id="' . htmlspecialcharsbx($changeButtonID) . '" class="bx-crm-edit-crm-entity-change">' . htmlspecialcharsbx(GetMessage('intarface_form_select')) . '</span>
                                                        </div>';

                                                $dialogSettings['CONTACT'] = array(
                                                    'addButtonName' => GetMessage('interface_form_add_dialog_btn_add'),
                                                    'cancelButtonName' => GetMessage('interface_form_cancel'),
                                                    'title' => GetMessage('interface_form_add_contact_dlg_title'),
                                                    'lastNameTitle' => GetMessage('interface_form_add_contact_fld_last_name'),
                                                    'nameTitle' => GetMessage('interface_form_add_contact_fld_name'),
                                                    'secondNameTitle' => GetMessage('interface_form_add_contact_fld_second_name'),
                                                    'emailTitle' => GetMessage('interface_form_add_contact_fld_email'),
                                                    'phoneTitle' => GetMessage('interface_form_add_contact_fld_phone'),
                                                    'exportTitle' => GetMessage('interface_form_add_contact_fld_export')
                                                );
                                                $dialogSettings['COMPANY'] = array(
                                                    'addButtonName' => GetMessage('interface_form_add_dialog_btn_add'),
                                                    'cancelButtonName' => GetMessage('interface_form_cancel'),
                                                    'title' => GetMessage('interface_form_add_company_dlg_title'),
                                                    'titleTitle' => GetMessage('interface_form_add_company_fld_title_name'),
                                                    'companyTypeTitle' => GetMessage('interface_form_add_conpany_fld_company_type'),
                                                    'industryTitle' => GetMessage('interface_form_add_company_fld_industry'),
                                                    'emailTitle' => GetMessage('interface_form_add_conpany_fld_email'),
                                                    'phoneTitle' => GetMessage('interface_form_add_company_fld_phone'),
                                                    'companyTypeItems' => CCrmEntitySelectorHelper::PrepareListItems(CCrmStatus::GetStatusList('COMPANY_TYPE')),
                                                    'industryItems' => CCrmEntitySelectorHelper::PrepareListItems(CCrmStatus::GetStatusList('INDUSTRY'))
                                                );
                                                ob_start();
                                                ?>
                                                <script type="text/javascript">
                                                    BX.ready(
                                                        function () {
                                                            var entitySelectorId = CRM.Set(
                                                                BX('<?=CUtil::JSEscape($changeButtonID) ?>'),
                                                                '<?=CUtil::JSEscape($selectorID)?>',
                                                                '',
                                                                <?=CUtil::PhpToJsObject(CCrmEntitySelectorHelper::PreparePopupItems($entityType, true, isset($params['NAME_TEMPLATE']) ? $params['NAME_TEMPLATE'] : ''))?>,
                                                                false,
                                                                false,
                                                                <?=CUtil::PhpToJsObject($entityType)?>,
                                                                <?=CUtil::PhpToJsObject(CCrmEntitySelectorHelper::PrepareCommonMessages())?>,
                                                                true
                                                            );

                                                            BX.CrmEntityEditor.messages =
                                                            {
                                                                'unknownError': '<?=GetMessageJS('interface_form_ajax_unknown_error')?>',
                                                                'prefContactType': '<?=GetMessageJS('interface_form_entity_selector_prefContactType')?>',
                                                                'prefPhone': '<?=GetMessageJS('interface_form_entity_selector_prefPhone')?>',
                                                                'prefEmail': '<?=GetMessageJS('interface_form_entity_selector_prefEmail')?>'
                                                            };

                                                            BX.CrmEntityEditor.create(
                                                                '<?=CUtil::JSEscape($editorID . '_C')?>',
                                                                {
                                                                    'context': '<?=CUtil::JSEscape($context)?>',
                                                                    'typeName': 'CONTACT',
                                                                    'containerId': '<?=CUtil::JSEscape($containerID)?>',
                                                                    'buttonAddId': '<?=CUtil::JSEscape($addContactButtonID)?>',
                                                                    'enableValuePrefix': true,
                                                                    'buttonChangeIgnore': false,
                                                                    'dataInputId': '<?=CUtil::JSEscape($dataInputID)?>',
                                                                    'newDataInputId': '<?=CUtil::JSEscape($newDataInputID)?>',
                                                                    'entitySelectorId': entitySelectorId,
                                                                    'serviceUrl': '<?=CUtil::JSEscape('/bitrix/components/bitrix/crm.contact.edit/ajax.php?siteID=' . SITE_ID . '&' . bitrix_sessid_get())?>',
                                                                    'createUrl': '<?=CUtil::JSEscape(CCrmOwnerType::GetEditUrl(CCrmOwnerType::Contact, 0, false))?>',
                                                                    'actionName': 'SAVE_CONTACT',
                                                                    'dialog': <?=CUtil::PhpToJSObject($dialogSettings['CONTACT'])?>
                                                                }
                                                            );

                                                            BX.CrmEntityEditor.create(
                                                                '<?=CUtil::JSEscape($editorID) . '_CO'?>',
                                                                {
                                                                    'context': '<?=CUtil::JSEscape($context)?>',
                                                                    'typeName': 'COMPANY',
                                                                    'containerId': '<?=CUtil::JSEscape($containerID)?>',
                                                                    'buttonAddId': '<?=CUtil::JSEscape($addCompanyButtonID)?>',
                                                                    'buttonChangeIgnore': true,
                                                                    'enableValuePrefix': true,
                                                                    'dataInputId': '<?=CUtil::JSEscape($dataInputID)?>',
                                                                    'newDataInputId': '<?=CUtil::JSEscape($newDataInputID)?>',
                                                                    'entitySelectorId': entitySelectorId,
                                                                    'serviceUrl': '<?=CUtil::JSEscape('/bitrix/components/bitrix/crm.company.edit/ajax.php?siteID=' . SITE_ID . '&' . bitrix_sessid_get())?>',
                                                                    'createUrl': '<?=CUtil::JSEscape(CCrmOwnerType::GetEditUrl(CCrmOwnerType::Company, 0, false))?>',
                                                                    'actionName': 'SAVE_COMPANY',
                                                                    'dialog': <?=CUtil::PhpToJSObject($dialogSettings['COMPANY'])?>
                                                                }
                                                            );
                                                        }
                                                    );
                                                </script><?
                                                $bufScript .= ob_get_contents();
                                                ob_end_clean();
                                            echo $res;
                                            }elseif ($arrA["SID"] == "ORDER_QB_LC_ID99"){
                                            CCrmComponentHelper::RegisterScriptLink('/bitrix/js/crm/crm.js');
                                            $params = [
                                                'CONTEXT' => 'ORDER_LIST_' . $arRes["ID"] . '_' . $arrA['ANSWER_ID'],
                                                'ENTITY_TYPE' => 'DEAL',
                                                'INPUT_NAME' => 'EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' . $arrA['ANSWER_ID'] . ']',
                                                'INPUT_VALUE' => $originalAnswerText,
                                            ];
                                            $context = isset($params['CONTEXT']) ? $params['CONTEXT'] : '';
                                            $entityType = isset($params['ENTITY_TYPE']) ? $params['ENTITY_TYPE'] : '';
                                            $entityID = isset($params['INPUT_VALUE']) ? $params['INPUT_VALUE'] : '';
                                            $editorID = "eF_{$params['INPUT_NAME']}";
                                            $containerID = "eF_FС_{$params['INPUT_NAME']}";
                                            $selectorID = "eF_ES_{$params['INPUT_NAME']}";
                                            $changeButtonID = "eF_CB_{$params['INPUT_NAME']}";
                                            $dataInputName = isset($params['INPUT_NAME']) ? $params['INPUT_NAME'] : $params['INPUT_NAME'];
                                            $dataInputID = "eF_DI_{$dataInputName}";
                                            $entityInfo = CCrmEntitySelectorHelper::PrepareEntityInfo($entityType, $entityID);

                                            $res = '<div id="' . htmlspecialcharsbx($containerID) . '" class="bx-crm-edit-crm-entity-field">
                                                            <div class="bx-crm-entity-info-wrapper">' . ($entityID > 0 ? '<a href="' . htmlspecialcharsbx($entityInfo['URL']) . '" target="_blank" class="bx-crm-entity-info-link">' . htmlspecialcharsEx($entityInfo['TITLE']) . '</a><span class="crm-element-item-delete"></span>' : '') . '</div>
                                                            <input type="hidden" id="' . htmlspecialcharsbx($dataInputID) . '" name="' . htmlspecialcharsbx($dataInputName) . '" value="' . $entityID . '" />
                                                            <div class="bx-crm-entity-buttons-wrapper">
                                                                <span id="' . htmlspecialcharsbx($changeButtonID) . '" class="bx-crm-edit-crm-entity-change">' . htmlspecialcharsbx(GetMessage('intarface_form_select')) . '</span>
                                                            </div>
                                                        </div>';

                                            $serviceUrl = '';
                                            $createUrl = '';
                                            $actionName = '';
                                            $dialogSettings = array(
                                                'addButtonName' => GetMessage('interface_form_add_dialog_btn_add'),
                                                'cancelButtonName' => GetMessage('interface_form_cancel')
                                            );
                                            $dialogSettings['title'] = GetMessage('interface_form_add_company_dlg_title');
                                            $dialogSettings['titleTitle'] = GetMessage('interface_form_add_company_fld_title_name');
                                            $dialogSettings['dealTypeTitle'] = GetMessage('interface_form_add_conpany_fld_company_type');
                                            $dialogSettings['dealPriceTitle'] = GetMessage('interface_form_add_company_fld_industry');
                                            $dialogSettings['companyTypeItems'] = CCrmEntitySelectorHelper::PrepareListItems(CCrmStatus::GetStatusList('DEAL_TYPE'));

                                            ?>
                                                <script type="text/javascript">
                                                    BX.ready(
                                                        function () {
                                                            var entitySelectorId = CRM.Set(
                                                                BX('<?=CUtil::JSEscape($changeButtonID) ?>'),
                                                                '<?=CUtil::JSEscape($selectorID)?>',
                                                                '',
                                                                <?=CUtil::PhpToJsObject(CCrmEntitySelectorHelper::PreparePopupItems($entityType, false, isset($params['NAME_TEMPLATE']) ? $params['NAME_TEMPLATE'] : \Bitrix\Crm\Format\PersonNameFormatter::getFormat()))?>,
                                                                false,
                                                                false,
                                                                ['<?=CUtil::JSEscape(strtolower($entityType))?>'],
                                                                <?=CUtil::PhpToJsObject(CCrmEntitySelectorHelper::PrepareCommonMessages())?>,
                                                                true
                                                            );

                                                            BX.CrmEntityEditor.messages =
                                                            {
                                                                'unknownError': '<?=GetMessageJS('interface_form_ajax_unknown_error')?>',
                                                                'prefContactType': '<?=GetMessageJS('interface_form_entity_selector_prefContactType')?>',
                                                                'prefPhone': '<?=GetMessageJS('interface_form_entity_selector_prefPhone')?>',
                                                                'prefEmail': '<?=GetMessageJS('interface_form_entity_selector_prefEmail')?>'
                                                            };

                                                            BX.CrmEntityEditor.create(
                                                                '<?=CUtil::JSEscape($editorID)?>',
                                                                {
                                                                    'context': '<?=CUtil::JSEscape($context)?>',
                                                                    'typeName': '<?=CUtil::JSEscape($entityType)?>',
                                                                    'containerId': '<?=CUtil::JSEscape($containerID)?>',
                                                                    'dataInputId': '<?=CUtil::JSEscape($dataInputID)?>',
                                                                    'newDataInputId': '<?=CUtil::JSEscape($newDataInputID)?>',
                                                                    'entitySelectorId': entitySelectorId,
                                                                    'buttonChangeIgnore': false,
                                                                    'serviceUrl': '<?= CUtil::JSEscape($serviceUrl) ?>',
                                                                    'createUrl': '<?= CUtil::JSEscape($createUrl) ?>',
                                                                    'actionName': '<?= CUtil::JSEscape($actionName) ?>',
                                                                    'dialog': <?=CUtil::PhpToJSObject($dialogSettings)?>
                                                                }
                                                            );
                                                        }
                                                    );
                                                </script><?
                                                echo $res;
                                            } else {
                                                echo '<input type="text" value="' . (isset($entityInfo['TITLE']) ? $entityInfo['TITLE'] : $answerText) . '" name="EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' . $arrA['ANSWER_ID'] . ']"/>';
                                            }
                                            break;
//                                        case
//                                        'textarea':
//                                            echo '<textarea name="EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' . $arrA['ANSWER_ID'] . ']">' . $answerText . '</textarea>';
//                                            break;
                                        case 'radio':
//                                            if (in_array($firstElem['SID'], $editableTD)) {
                                                $html = '<select name="EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' . $arrA['SID'] . ']">';
                                                foreach ($arResult["arrColumns"][$arrA['FIELD_ID']]['ANSWERS'] as $item) {
                                                    $html .= '<option value="' . $item['ID'] . '" ' . ($arrA['ANSWER_ID'] == $item['ID'] ? "selected" : "") . '>' . $item['MESSAGE'] . '</option>';
                                                }
                                                $html .= '</select>';
                                                echo $html;
//                                            } else {
//                                                echo '<input type="hidden" name="EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' . $arrA['SID'] . ']" value="' . $arrA['ANSWER_ID'] . '"/>';
//                                            }
                                            break;
                                        case "file":
                                            if (strlen(trim($arrA["MESSAGE"]))>0)
                                            {
                                                $res .= $arrA["MESSAGE"];
                                            }
                                            if ($arFile = CFormResult::GetFileByAnswerID($arRes["ID"], $arrA['ANSWER_ID']))
                                            {
                                                if (intval($arFile["USER_FILE_ID"])>0)
                                                {
                                                    $res .= "<br/>[<a title=\"".str_replace("#FILE_NAME#", $arFile["USER_FILE_NAME"], GetMessage("FORM_DOWNLOAD_FILE"))."\" class=\"tablebodylink\" href=\"/bitrix/tools/form_show_file.php?rid=".$arRes["ID"]."&hash=".$arFile["USER_FILE_HASH"]."&lang=".LANGUAGE_ID."&action=download\">".GetMessage("FORM_DOWNLOAD")."</a>(" . CFile::FormatSize($arFile["USER_FILE_SIZE"]) . ")]<br />";
//                                                    $res .= '<input type="checkbox" value="Y" name="EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' . $arrA['ANSWER_ID'] . '_del]" id="EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' . $arrA['ANSWER_ID'] . '_del]" /><label for="form_file_'.$arAnswer['ID'].'_del">'.GetMessage('FORM_DELETE_FILE').'</label><br />';

                                                } //endif;
                                            } //endif;
//
//
//                                            $res .= CForm::GetFileField(
//                                                $arrA['ANSWER_ID'],
//                                                $arrA["FIELD_WIDTH"],
//                                                "FILE",
//                                                0,
//                                                "",
//                                                $arrA["FIELD_PARAM"]);

                                            echo $res;

                                            break;
                                        default:
                                            echo '<input type="text" value="' . (isset($entityInfo['TITLE']) ? $entityInfo['TITLE'] : $answerText) . '" name="EDIT[' . $arRes["ID"] . '][form_' . $arrA['FIELD_TYPE'] . '_' . $arrA['ANSWER_ID'] . ']"/>';
                                            break;
                                    }
                                    //}
                                } //foreach
                            } // endif (is_array($arrAnswer));
                            ?>
                        </td>
                        <?
                    } //foreach
                    ?>
                    <td class="status"><?= $arRes["STATUS_TITLE"] ?></td>
                    <td width="50px">
                        <? if ($isAdmin || $arRes['STATUS_ID'] == $newStatusId) : ?>
                            <a href="/services/requests/form_edit.php?WEB_FORM_ID=<?= $arParams["WEB_FORM_ID"] ?>&RESULT_ID=<?= $arRes['ID'] ?>"
                               title="Редактировать" class="action_btn_link">
                                <span class="feed-destination-edit"></span>
                            </a>
                            <a href="/services/requests/form_list.php?WEB_FORM_ID=<?= $arParams["WEB_FORM_ID"] ?>&del_id=<?= $arRes['ID'] ?>"
                               title="Удалить" class="action_btn_link">
                                <span class="feed-destination-delete"></span>
                            </a>
                        <? endif; ?>
                    </td>
                </tr>
                <?
            } //foreach
            ?>
            </tbody>
            <?
        } ?>
    </table>

    <div class="control_panel">
        <div class="btn btn-edit" data-url="/services/requests/form_edit.php?<?=$_SERVER['QUERY_STRING'];?>">Редактировать</div>
        <div class="btn btn-cancel">Отменить</div>
        <div class="btn btn-del">Удалить</div>
        <? if(CSite::InGroup(array(1,17))) :?>
        <div class="status_change">
            <select>
                <option></option>
                <? foreach ($arResult['arStatuses_MOVE'] as $item) : ?>
                    <option value="<?= $item['REFERENCE_ID'] ?>"><?= $item['REFERENCE'] ?></option>
                <? endforeach; ?>
            </select>
            <div class="btn btn-change">Изменить статус</div>
        </div>
        <? endif;?>
    </div>


    <div class="pager">
        <?=$arResult["pager"]?>
    </div>
</form>
<script>
    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "numeric-comma-pre": function (a) {
            var num = jQuery(a)[0].innerHTML;
            var x = (num == "-") ? 0 : num.replace(/\s/, "");
            return parseFloat(x);
        },

        "numeric-comma-asc": function (a, b) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },

        "numeric-comma-desc": function (a, b) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    });
    $(document).ready(function () {

        var t = $('#ordersTable').DataTable({
            "columnDefs": [
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                },
                {
                    "searchable": false,
                    "orderable": true,
                    "targets": "ORDER_PRIORITY",
                    "render": function (data, type, full, meta) {
                        if (type === 'filter') {
                            return $('#ordersTable').DataTable().cell(meta.row, meta.col).nodes().to$().find('span.originalAnswerText').text();
                        } else {
                            return data;
                        }
                    }
                },
                {
                    "targets": "ORDER_TYPE",
                    "orderable": false,
                    "render": function (data, type, full, meta) {
                        if (type === 'filter') {
                            return $('#ordersTable').DataTable().cell(meta.row, meta.col).nodes().to$().find('span.originalAnswerText').text();
                        } else {
                            return data;
                        }
                    }
                },
                {
                    "targets": "PRICE",
                    "type": "numeric-comma",
                    "render": function (data, type, full, meta) {
                        if (type === 'filter') {
                            return $('#ordersTable').DataTable().cell(meta.row, meta.col).nodes().to$().find('span.originalAnswerText').text().replace(/\s/, '');
                        } else {
                            return data;
                        }
                    }
                }
                ,
                {
                    "targets": "ORDER_CONTR"
                }
                <? if($isAdmin) :?>
                ,
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": $(this).find("thead th").length - 1
                }
                <? endif; ?>

            ],
            "info": false,
            "paging": false,
            "order": [[1, 'asc'], [2, 'asc']],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Russian.json"
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;
                // Remove the formatting to get integer data for summation

                var intVal = function (i) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,\s]/g, '') * 1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                // Total over this page
                pageTotal = api
                    .column('.PRICE', {page: 'current'})
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal($(b)[0].innerHTML);
                    }, 0);

                // Update footer
                $(api.column('.PRICE').footer()).html(
                    pageTotal.toString().replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ')
                );
            },
            "initComplete": function () {
                var column = this.api().column(this.api().columns()[0].length - 2);
                var label = $('<label style="margin-left: 20px;">Статус: </label>').appendTo("#ordersTable_filter");
                var select = $('<select></select>')
                    .appendTo(label)
                    .on('change', function () {

                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        var url = location.href;
                        url = delPrm(url,'status');
                        url = delPrm(url,'PAGEN_1');

                        location.href = url + '&status=' + val;
                    });
                <?
                $status = isset($_GET['status']) ? ($_GET['status'] == $newStatusId ? implode(' | ',$unpaidStatusIDs) : ($_GET['status'] == $unpaidStatusIDs ? $unpaidStatusIDs : '')) : $newStatusId;
                ?>
                select.append('<option <?=$status == $newStatusId ? 'selected' : '' ?> value="<?=$newStatusId?>">Неоплаченные</option>');
                select.append('<option <?=$status == $paidStatusID ? 'selected' : '' ?> value="<?=$paidStatusID?>">Оплаченные</option>');
                select.append('<option <?=$status == '' ? 'selected' : '' ?> value="all">Все</option>');
            }
        });

//        t.on('order.dt search.dt', function () {
//            t.column(1, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
//                cell.innerHTML = i + 1;
//            });
//        }).draw();

        function delPrm(Url,Prm) {
            var a=Url.split('?');
            var re = new RegExp('(\\?|&)'+Prm+'=[^&]+','g');
            Url=('?'+a[1]).replace(re,'');
            Url=Url.replace(/^&|\?/,'');
            var dlm=(Url=='')? '': '?';
            return a[0]+dlm+Url;
        };

        $('.crm-block-cont-block-contact .crm-block-cont-item').on('click', function () {
            var idStr = $(this).closest(".crm-block-cont-block-contact").attr('id');
            var id = idStr.substring(idStr.indexOf("[") + 1, idStr.indexOf("]"));
            $('select[name="EDIT[' + id + '][form_radio_ORDER_TYPE]"] option[value="<?=$cashAnswerId?>"]').prop('selected', true);
            $('#EDIT[' + id + '][form_file_<?=$orderFieldId?>_del]').prop('checked', true);
        });

        $('.crm-block-cont-block-company .crm-block-cont-item').on('click', function () {
            var idStr = $(this).closest(".crm-block-cont-block-company").attr('id');
            var id = idStr.substring(idStr.indexOf("[") + 1, idStr.indexOf("]"));
            $('select[name="EDIT[' + id + '][form_radio_ORDER_TYPE]"] option[value="<?=$bankAnswerId?>"]').prop('selected', true);
            $('#EDIT[' + id + '][form_file_<?=$orderFieldId?>_del]').prop('checked', false);
        });

//        $('#ordersTable').on('click', '.feed-destination-edit', function (e) {
//            e.preventDefault();
//            var self = $(this);
//            var tr = $(this).closest('tr');
//            var tdEditable = $(tr).find('td.editable, td.hidden_editable');
//            var data = {};
//
//            if ($(tr).hasClass('enable')) {
//                tdEditable.each(function (indx, element) {
//                    if ($(element).find('textarea').length) {
//                        data[$(element).find('textarea').attr('name')] = $(element).find('textarea').text();
//                    } else {
//                        data[$(element).find('input, select').attr('name')] = $(element).find('input, select').val();
//                    }
//                });
//                data['sessid'] = '<?//=bitrix_sessid()?>//';
//
//                $.ajax({
//                    type: 'POST',
//                    data: data,
//                    //url: self.parent().attr('href'),
//                    success: function () {
//                        tdEditable.each(function (indx, element) {
//                            if(!$(element).hasClass('hidden_editable')){
//                                if ($(element).find('select').length) {
//                                    $(element).find('span.originalAnswerText').html($(element).find('select option:selected'));
//                                } else if ($(element).find('.bx-crm-edit-crm-entity-field a.bx-crm-entity-info-link').length) {
//                                    $(element).find('span.originalAnswerText').html($(element).find('.bx-crm-edit-crm-entity-field a.bx-crm-entity-info-link').clone());
//                                } else if ($(element).find('input').length) {
//                                    $(element).find('span.originalAnswerText').html($(element).find('input').val());
//                                } else if ($(element).find('textarea').length) {
//                                    $(element).find('span.originalAnswerText').html($(element).find('textarea').html());
//                                }
//                            }
//                        });
//                        $(tr).removeClass('enable');
//                    }
//                });
//            } else {
//                $('#ordersTable').find('tr').removeClass('enable');
//                $('#ordersTable').find('input.chBox').prop("checked", false);
//                $('#editableForm').find('.btn-edit').removeClass('editable');
//                $(tr).addClass('enable');
//            }
//        });

        $('#editableForm').on('click', '.btn-edit', function (e) {
            var trEditableArr = $('#ordersTable').find('input[type=checkbox]:checked').closest('tr');
            var self = $(this);
            if (self.hasClass('editable')) {
                var data = {};
                data['sessid'] = '<?=bitrix_sessid()?>';
                trEditableArr.each(function (indx, element) {
                    var tdEditable = $(element).find('td.editable, td.hidden_editable');
                    tdEditable.each(function (indx, element) {
                        if ($(element).find('textarea').length) {
                            data[$(element).find('textarea').attr('name')] = $(element).find('textarea').html();
                        } else {
                            data[$(element).find('input, select').attr('name')] = $(element).find('input, select').val();
                        }
                    });
                });

                $.ajax({
                    type: 'POST',
                    data: data,
                    //url: self.attr('data-url'),
                    success: function () {
                        trEditableArr.each(function (indx, elementTr) {
                            var tdEditable = $(elementTr).find('td.editable, td.hidden_editable');
                            $(elementTr).addClass('enable');
                            tdEditable.each(function (indx, element) {
                                if ($(element).find('select').length) {
                                    $(element).find('span.originalAnswerText').html($(element).find('select option:selected').text());
                                } else if ($(element).find('.bx-crm-edit-crm-entity-field a.bx-crm-entity-info-link').length) {
                                    $(element).find('span.originalAnswerText').html($(element).find('.bx-crm-edit-crm-entity-field a.bx-crm-entity-info-link').clone());
                                } else if ($(element).find('input').length) {
                                    $(element).find('span.originalAnswerText').html($(element).find('input').val());
                                } else if ($(element).find('textarea').length) {
                                    $(element).find('span.originalAnswerText').html($(element).find('textarea').html());
                                }
                            });
                            $(elementTr).find('input.chBox').prop("checked", false);
                            $(elementTr).removeClass('enable');
                        });
                        self.removeClass('editable');
                        self.text('Редактировать');
                        self.parent().removeClass('edit');
                        $('#editableForm input.check_all').prop("checked", false);
                    }
                });
            } else {
                $('#ordersTable').find('tr').removeClass('enable');
                if (trEditableArr.length) {
                    self.addClass('editable');
                    self.text('Сохранить');
                    self.parent().addClass('edit');
                    trEditableArr.each(function (indx, element) {
                        $(element).addClass('enable');
                    });
                }
            }
        });

        $('#editableForm').on('click', '.btn-change', function (e) {
            var trEditableArr = $('#ordersTable').find('input[type=checkbox]:checked').closest('tr');
            var data = {};
            data['sessid'] = '<?=bitrix_sessid()?>';
            data['CHANGE_STATUS'] = $('#editableForm .control_panel .status_change select option:selected').val();
            if (data['CHANGE_STATUS'] > 0) {
                var ids = [];
                trEditableArr.each(function (indx, element) {
                    ids.push($(element).find('input.chBox').val());
                });

                data['CHANGE_STATUS_IDS'] = ids;
                if (ids.length > 0) {
                    $.ajax({
                        type: 'POST',
                        data: data,
                        success: function () {
                            trEditableArr.each(function (indx, elementTr) {
                                var str = $('#editableForm .control_panel .status_change select option:selected').text();
                                str = str.substring(str.indexOf(' '), str.length)
                                $(elementTr).find('td.status').text(str);
                                $(elementTr).find('input.chBox').prop("checked", false);
                                $('#editableForm input.check_all').prop("checked", false);
                            });
                        }
                    });
                }
            }
        });

        $('#editableForm').on('click', '.btn-cancel', function (e) {
            var self = $(this);
            var trEditableArr = $('#ordersTable').find('input[type=checkbox]:checked').closest('tr');
            self.parent().removeClass('edit');
            self.parent().find('.btn-edit').removeClass('editable').text('Редактировать');
            trEditableArr.each(function (indx, element) {
                $(element).removeClass('enable');
            });
        });

        $('#editableForm').on('click', '.btn-del', function (e) {
            var trEditableArr = $('#ordersTable').find('input[type=checkbox]:checked').closest('tr');
            var data = {};
            var ids = [];
            data['sessid'] = '<?=bitrix_sessid()?>';

            trEditableArr.each(function (indx, element) {
                ids.push($(element).find('input.chBox').val());
            });

            data['del_ids'] = ids;
            $.ajax({
                type: 'GET',
                data: data,
                success: function () {
                    trEditableArr.each(function (indx, elementTr) {
                        elementTr.remove();
                        $('#editableForm input.check_all').prop("checked", false);
                    });
                }
            });
        });

        $('#editableForm').on('click', 'input.check_all', function (e) {
            $('#ordersTable').find('input[type=checkbox]').prop("checked", $(this).prop("checked"));
        });
    });
</script>
<?= $bufScript ? $bufScript : '' ?>