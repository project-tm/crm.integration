<?php

namespace Local\Site;

class Umi {

    static public function callback() {
        self::sendRoistatAmo(array(
            'title' => 'Обратный звонок с сайта boardshop.ru:' . $_REQUEST['data']['new']['client_name'],
            'name' => $_REQUEST['data']['new']['client_name'],
            'phone' => $_REQUEST['data']['new']['client_phone'],
            'fields' => array(
                563283 => 'boardshop.ru',
                563325 => 'boardshop.ru',
            )
        ));
    }

    static public function orderClick($order) {
        $comment = array(
            'Заказ №' . $order->getId(),
            'Товары:'
        );
        $orderItems = $order->getItems();
        foreach ($orderItems as $value) {
            $comment[] = $value->getName() . ' x ' . $value->getAmount();
        }
        $comment[] = 'Итого: ' . $order->getActualPrice();
        $customerId = $order->getCustomerId();
        if ($customerId) {
            $objects = \umiObjectsCollection::getInstance();
            $customerObject = $objects->getObject($customerId);
            $customer = new \customer($customerObject);

            if ($customer->getValue('fname')) {
                $name[] = $customer->getValue('fname');
            }
            if ($customer->getValue('lname')) {
                $name[] = $customer->getValue('lname');
            }
            if ($customer->getValue('father_name')) {
                $name[] = $customer->getValue('father_name');
            }
            if ($customer->getValue('delivery_addresses')) {
                $comment[] = 'Адрес: ' . $customer->getValue('delivery_addresses');
            }
            self::sendRoistatAmo(array(
                'title' => 'Заказ с сайта boardshop.ru №' . $order->getId() . ': ' . implode(' ', $name),
                'comment' => implode(PHP_EOL, $comment),
                'name' => implode(' ', $name),
                'email' => $customer->getValue('email'),
                'phone' => $customer->getValue('phone'),
                'fields' => array(
                    563283 => 'boardshop.ru',
                    563325 => 'boardshop.ru',
                )
            ));
        } else {
            $name = array();
            if (empty($_REQUEST['data'][$customerId])) {
                return;
            }
            if (!empty($_REQUEST['data'][$customerId]['fname'])) {
                $name[] = $_REQUEST['data'][$customerId]['fname'];
            }
            if (!empty($_REQUEST['data'][$customerId]['lname'])) {
                $name[] = $_REQUEST['data'][$customerId]['lname'];
            }
            if (!empty($_REQUEST['data'][$customerId]['father_name'])) {
                $name[] = $_REQUEST['data'][$customerId]['father_name'];
            }
            if (!empty($_REQUEST['data'][$customerId]['adress_user'])) {
                $comment[] = 'Адрес: ' . $_REQUEST['data'][$customerId]['adress_user'];
            }
            if (!empty($_REQUEST['data'][$customerId]['comment'])) {
                $comment[] = 'Комментарий: ' . $_REQUEST['data'][$customerId]['comment'];
            }
            self::sendRoistatAmo(array(
                'title' => 'Заказ с сайта boardshop.ru №' . $order->getId() . ': ' . implode(' ', $name),
                'comment' => implode(PHP_EOL, $comment),
                'name' => implode(' ', $name),
                'email' => $_REQUEST['data'][$customerId]['email'],
                'phone' => $_REQUEST['data'][$customerId]['phone'],
                'fields' => array(
                    563283 => 'boardshop.ru',
                    563325 => 'boardshop.ru',
                )
            ));
        }
//        preExit();
    }

    static public function orderOneClick($order) {
        $comment = array(
            'Быстрый заказ',
            'Товары:'
        );
        $orderItems = $order->getItems();
        foreach ($orderItems as $value) {
            $comment[] = $value->getName() . ' x ' . $value->getAmount();
        }
        $comment[] = 'Итого: ' . $order->getActualPrice();
        $name = array();
        if (!empty($_REQUEST['data']['new']['fname'])) {
            $name[] = $_REQUEST['data']['new']['fname'];
        }
        if (!empty($_REQUEST['data']['new']['lname'])) {
            $name[] = $_REQUEST['data']['new']['lname'];
        }
        self::sendRoistatAmo(array(
            'title' => 'Быстрый заказ с сайта boardshop.ru:' . implode(' ', $name),
            'comment' => implode(PHP_EOL, $comment),
            'name' => implode(' ', $name),
            'email' => $_REQUEST['data']['new']['email'],
            'phone' => $_REQUEST['data']['new']['phone'],
            'fields' => array(
                563283 => 'boardshop.ru',
                563325 => 'boardshop.ru',
            )
        ));
    }

    static public function sendRoistatAmo($roistatData) {
        $roistatData['roistat'] = isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null;
        $roistatData['key'] = 'NzYyMzo0NDM1MDozODJlMTYxMGRmMDkzYzBmOGNjOTY3Y2EyMjI3MDJjOA==';
        $roistatData['tags'] = 'boardshop.ru';
        $roistatData['fields']['527287'] = $roistatData['roistat'];
        $roistatData['fields']['563327'] = $roistatData['roistat'];
//        pre($roistatData);
//        preExit(file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData)));
        file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));
//            $roistatData = array(
//            'roistat' => isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null,
//            'key' => 'NTkxMjo1NzE2NzphMTY1YmQ0Yzk1Zjg1NWNiN2EyYTc5YzllZjAzYTk1ZA==', // Замените SECRET_KEY на секретный ключ из пункта меню Настройки -> Интеграция со сделками в нижней части экрана и строчке Ключ для интеграций
//            'title' => (isset($_POST["name"])) ? $_POST["name"] : "",
//            'comment' => (isset($_POST["city"])) ? $_POST["city"] : "",
//            'name' => (isset($_POST["name"])) ? $_POST["name"] : "",
//            'email' => (isset($_POST["email"])) ? $_POST["email"] : "",
//            'phone' => (isset($_POST["phone"])) ? $_POST["phone"] : "",
//            'fields' => $fields,
//            "tags" => 'more-wishes.ru',
//        );
//        pre(var_export($roistatData));
//        preExit();
    }

}
