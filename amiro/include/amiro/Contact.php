<?php

namespace Local\Amiro;

class Contact {

    static public function search($arData, $leadId) {
        $response = Utility::curl('v2/json/contacts/list?query=' . urlencode(empty($arData['Телефон']) ? $arData['Имя'] : $arData['Телефон']));
        if ($response and isset($response['response']['contacts'][0]['id'])) {
            $contactId = $response['response']['contacts'][0]['id'];
            pre($contactId);
            $set = array();
            $set['request']['contacts']['update'][0] = $response['response']['contacts'][0];
            $set['request']['contacts']['update'][0]['linked_leads_id'][] = $leadId;

            $response = Utility::curl('v2/json/contacts/set', $set);
//            pre($response);
            if ($response and isset($response['response']['contacts']['update'][0]['id'])) {

            } else {
                throw new \Exception('Невозможно создать контакт');
            }

        } else {
            $fields = Utility::getFields('contacts');

            $contact = array(
                'name' => $arData['Сайт'] . (empty($arData['Имя']) ? '' : ' ' . $arData['Имя']) . ': ' . $arData['Телефон'],
                'custom_fields' => Utility::setFields(array(
//                    163619 => $arData['Сайт'],
//                    163621 => $arData['roistat'],
                        )
                ),
                "linked_leads_id" => $leadId,
                "tags" => $arData['Сайт'],
                'responsible_user_id' => Config::RESPONSIBLE_ID,
            );
            Config::contact($contact, $arData);

            if (!empty($arData['Email'])) {
                $contact['custom_fields'][] = array(
                    'id' => $fields['EMAIL'],
                    'values' => array(
                        array(
                            'value' => $arData['Email'],
                            'enum' => 'OTHER'
                        )
                    )
                );
            }

            if (!empty($arData['Телефон'])) {
                $contact['custom_fields'][] = array(
                    'id' => $fields['PHONE'],
                    'values' => array(
                        array(
                            'value' => $arData['Телефон'],
                            'enum' => 'OTHER'
                        )
                    )
                );
            }

            $set = array();
            $set['request']['contacts']['add'][] = $contact;
//            pre($set);
//            exit;
            $response = Utility::curl('v2/json/contacts/set', $set);
            if ($response and isset($response['response']['contacts']['add'][0]['id'])) {
                $contactId = $response['response']['contacts']['add'][0]['id'];
            } else {
                throw new \Exception('Невозможно создать контакт');
            }
        }
        return $contactId;
    }

}
