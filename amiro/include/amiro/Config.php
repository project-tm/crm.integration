<?php

namespace Local\Amiro;

class Config {

    const SUBDOMAIN = 'SUBDOMAIN';
    const RESPONSIBLE_ID = RESPONSIBLE_ID;
    const USER_LOGIN = 'USER_LOGIN';
    const USER_HASH = 'USER_HASH';

    static public function lead(&$lead, $arData) {
        $lead['name'] = $arData['Сайт'] . (empty($arData['Имя']) ? '' : ' ' . $arData['Имя']) . ': ' . $arData['Телефон'];
        $lead['custom_fields'] = \Local\Amiro\Utility::setFields(array(
                    312173 => $arData['Описание'],
                    312175 => $arData['Сайт'],
                    312177 => $arData['Точка захвата'],
                    312179 => $arData['roistat'],
        ));
    }

    static public function contact(&$contact, $arData) {
        $contact['name'] = $arData['Сайт'] . (empty($arData['Имя']) ? '' : ' ' . $arData['Имя']) . ': ' . $arData['Телефон'];
        $contact['custom_fields'] = \Local\Amiro\Utility::setFields(array(
                    312059 => $arData['Сайт'],
                    312061 => $arData['Точка захвата'],
                    312171 => $arData['roistat'],
        ));
    }

}
