<?php

namespace Local\Amiro;

class Form {

    static public function send($arData) {
        try {
//            pre($arData);
            if (!Auth::init()) {
                return;
            }
            if(empty($arData['Имя'])) {
                $arData['Имя'] = '';
            }
            $arData['roistat'] = isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null;
            $leadId = Lead::add($arData);
            $contactId = Contact::search($arData, $leadId);
//            Task::add($contactId, $arData);
        } catch (\Exception $exc) {
            pre($exc->getTraceAsString());
            pre('Ошибка: ' . $exc->getMessage() . PHP_EOL . 'Код ошибки: ' . $exc->getCode());
        }
    }

}
